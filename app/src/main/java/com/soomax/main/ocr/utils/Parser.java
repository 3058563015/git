package com.soomax.main.ocr.utils;


import com.soomax.main.ocr.exception.FaceException;

/**
 * JSON解析
 * @param <T>
 */
public interface Parser<T> {
    T parse(String json) throws FaceException;

}
