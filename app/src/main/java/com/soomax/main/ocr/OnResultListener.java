/*
 * Copyright (C) 2017 Baidu, Inc. All Rights Reserved.
 */
package com.soomax.main.ocr;


import com.soomax.main.ocr.exception.FaceException;

public interface OnResultListener<T> {
    void onResult(T result);

    void onError(FaceException error);
}
