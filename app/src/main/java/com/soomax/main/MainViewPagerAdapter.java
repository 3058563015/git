package com.soomax.main;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.soomax.main.home.FinderFragment;
import com.soomax.main.home.MainFragment;
import com.soomax.main.my.MineFragment;

public class MainViewPagerAdapter extends FragmentPagerAdapter {

    private int size;
    FragmentManager fm;


    public MainViewPagerAdapter(FragmentManager fm, int size) {
        super(fm);
        this.fm = fm;
        this.size = size;
    }


    @Override
    public Fragment getItem(int position) {

        if (position == 0) {
            Main.currentIndex = position;
            return MainFragment.newInstance(position + "");
        }
        if (position == 1) {
            Main.currentIndex = position;
            return FinderFragment.newInstance(position + "");
        }
        if (position == 2) {
            Main.currentIndex = position;
            return MineFragment.newInstance(position + "");
        }
        if (position == 3) {
            Main.currentIndex = position;
            return MineFragment.newInstance(position + "");
        }
        return MainFragment.newInstance(position + "");

    }

    @Override
    public int getCount() {
        return size;
    }
}
