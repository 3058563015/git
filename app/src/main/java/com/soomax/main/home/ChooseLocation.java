package com.soomax.main.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.kongzue.stacklabelview.StackLabel;
import com.kongzue.stacklabelview.interfaces.OnLabelClickListener;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.LocationPojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.home_chooselocation)
public class ChooseLocation extends BaseActivity {


    @BindView(R.id.tvNowLocation)
    TextView tvNowLocation;
    @BindView(R.id.stackLabelView)
    StackLabel stackLabelView;
    @BindView(R.id.tvBack)
    TextView tvBack;
    @BindView(R.id.rlTop)
    RelativeLayout rlTop;
    LocationPojo locationPojo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_chooselocation);
        ButterKnife.bind(this);
        getLocation();
        tvNowLocation.setText("当前定位-" + Hawk.get("address"));
        stackLabelView.setOnLabelClickListener(new OnLabelClickListener() {
            @Override
            public void onClick(int index, View v, String s) {
                Hawk.put("address", s);
                Hawk.put("lat", locationPojo.getRes().get(index).getLat() + "");
                Hawk.put("lng", locationPojo.getRes().get(index).getLng() + "");

            }
        });


    }


    private void getLocation() {
        final Map<String, String> params = new HashMap<String, String>();
        OkGo.<String>post(API.appgetregionlist).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                if (MyTextUtils.isEmpty(response.body())) return;
                locationPojo = JSON.parseObject(response.body(), LocationPojo.class);
                if (locationPojo.getCode().equals("200")) {
                    ArrayList<String> labels = new ArrayList<>();
                    for (LocationPojo.ResBean temp : locationPojo.getRes()) {
                        labels.add(temp.getName());
                    }
                    stackLabelView.setLabels(labels);
                    for (int i = 0; i < labels.size(); i++) {
                        if (labels.get(i).equals(Hawk.get("address"))) {
                            RelativeLayout relativeLayout = (RelativeLayout) stackLabelView.getChildAt(i);
                            LinearLayout linearLayout = (LinearLayout) relativeLayout.getChildAt(0);
                            linearLayout.performClick();
                        }
                    }
                }
            }
        });

    }

    @OnClick(R.id.tvBack)
    public void onViewClicked() {
        finish();
    }
}