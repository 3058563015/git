package com.soomax.main.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hedgehog.ratingbar.RatingBar;
import com.kongzue.stacklabelview.StackLabel;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.common.BDLocationUtil;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.ResultPojo;
import com.soomax.pojo.VideoDetailPojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.jzvd.JzvdStd;

@Route(path = RoutePath.home_video_detail)
public class VideoDetail extends BaseActivity {
    int pageindex = 1;
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.tvBarTitle)
    TextView tvBarTitle;
    @BindView(R.id.jz_video)
    JzvdStd jzVideo;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @Autowired(name = "id")
    String id;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    CommentAdapter commentAdapter;
    List<VideoDetailPojo.ResBean.CommentlistBean> list = new ArrayList<>();
    VideoDetailPojo videoDetailPojo;
    @BindView(R.id.stackLabelView)
    StackLabel stackLabelView;
    @BindView(R.id.rlTop)
    RelativeLayout rlTop;
    @BindView(R.id.tvEye)
    TextView tvEye;
    @BindView(R.id.tvComment)
    TextView tvComment;
    @BindView(R.id.tvzan)
    TextView tvzan;
    @BindView(R.id.tvshare)
    TextView tvshare;
    @BindView(R.id.ivZan)
    ImageView ivZan;
    @BindView(R.id.ivComment)
    ImageView ivComment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_video_detail);
        ButterKnife.bind(this);
        commentAdapter = new CommentAdapter(R.layout.sd_home_school_detail_fragment04_item, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(VideoDetail.this));
        recyclerView.setAdapter(commentAdapter);
        refreshLayout.setRefreshHeader(new MaterialHeader(VideoDetail.this).setShowBezierWave(false).setSize(1));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
                    @Override
                    public void result(BDLocation location) {
                        pageindex = 1;
                        getVideoDetail();
                    }
                });
            }
        });

        linBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ivZan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zan();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }

    private void zan() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        params.put("islike", videoDetailPojo.getRes().getSportroominfo().getIslike() == 0 ? "1" : "0");
        OkGo.<String>post(API.addappsportroomlike).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (!resultPojo.getCode().equals("200")) {
                    LightToasty.normal(VideoDetail.this, resultPojo.getMsg());
                } else {
//                    Glide.with(VideoDetail.this).load(R.mipmap.sd_zan_blue).into(ivZan);
                    getVideoDetail();
                }
            }
        });
    }

    private void zanlist(String id, String like) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        params.put("islike", like);
        OkGo.<String>post(API.addappsportroomcommentlike).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (!resultPojo.getCode().equals("200")) {
                    LightToasty.normal(VideoDetail.this, resultPojo.getMsg());
                } else {
                    refreshLayout.autoRefresh();
                }
            }
        });
    }

    private void getVideoDetail() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        params.put("pageCount", "20");
        params.put("pageNumber", pageindex + "");
        OkGo.<String>post(API.apiHealthSportvideo).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                refreshLayout.finishRefresh();
                if (MyTextUtils.isEmpty(response.body())) return;
                videoDetailPojo = JSON.parseObject(response.body(), VideoDetailPojo.class);
                initUI(videoDetailPojo);
            }
        });
    }


    void initUI(VideoDetailPojo videoDetailPojo) {

        tvTitle.setText(videoDetailPojo.getRes().getSportroominfo().getTitle());
        stackLabelView.setLabels(videoDetailPojo.getRes().getSportroominfo().getSportname().split(","));


        tvDesc.setText(videoDetailPojo.getRes().getSportroominfo().getDescs());


        jzVideo.setUp(videoDetailPojo.getRes().getSportroominfo().getLinkurl(), "");
        jzVideo.setScreenNormal();
        Glide.with(this)
                .load(videoDetailPojo.getRes().getSportroominfo().getImgpath())
                .into(jzVideo.thumbImageView);

        if (pageindex == 1) list.clear();
        if (videoDetailPojo.getRes().getSportroominfo().getCommentlist().size() == 0) {
            commentAdapter.loadMoreEnd();
        } else {
            commentAdapter.loadMoreComplete();
        }
        list.addAll(videoDetailPojo.getRes().getCommentlist());
        pageindex++;
        commentAdapter.notifyDataSetChanged();

        tvEye.setText(videoDetailPojo.getRes().getSportroominfo().getRecordcount() + "");
        tvComment.setText(videoDetailPojo.getRes().getCommentlistsize() + "");
        tvzan.setText(videoDetailPojo.getRes().getSportroominfo().getLikecount() + "");
        tvshare.setText("0");

        if (videoDetailPojo.getRes().getSportroominfo().getIslike() == 1) {
            Glide.with(VideoDetail.this).load(R.mipmap.sd_zan_blue).into(ivZan);
        } else {
            Glide.with(VideoDetail.this).load(R.mipmap.sd_zan_gray).into(ivZan);
        }


        ivComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Hawk.contains("token") || MyTextUtils.isEmpty(Hawk.get("token"))) {
                    ARouter.getInstance().build(RoutePath.login)
                            .navigation();
                } else {
                    ARouter.getInstance().build(RoutePath.comment)
                            .withString("id", videoDetailPojo.getRes().getSportroominfo().getId())
                            .withString("flag", "video")
                            .navigation();
                }
            }
        });


    }

    public class CommentAdapter extends BaseQuickAdapter<VideoDetailPojo.ResBean.CommentlistBean, BaseViewHolder> {
        public CommentAdapter(int layoutResId, List<VideoDetailPojo.ResBean.CommentlistBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final VideoDetailPojo.ResBean.CommentlistBean item) {
            viewHolder.setText(R.id.tvPhone, item.getNickname())
                    .setText(R.id.tvTime, item.getCreatetime())
                    .setText(R.id.tvzan, item.getIslikecount() + "")
                    .setText(R.id.tvComment, item.getContent() + "");
            RatingBar ratingBar = viewHolder.getView(R.id.ratingBar);
            ratingBar.setStar(Float.parseFloat(item.getScore()));
            ratingBar.setmClickable(false);

            ImageView ivtx = viewHolder.getView(R.id.ivtx);
            Glide.with(VideoDetail.this).load(item.getHeadimgpath()).into(ivtx);

            ImageView ivzans = viewHolder.getView(R.id.ivzan);
            if (item.getIslike() == 1) {
                Glide.with(VideoDetail.this).load(R.mipmap.sd_zan_blue).into(ivzans);
            } else {
                Glide.with(VideoDetail.this).load(R.mipmap.sd_zan_gray).into(ivzans);
            }
            ivzans.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    zanlist(item.getId(), item.getIslike() == 1 ? "0" : "1");
                }
            });


        }
    }

    @Override
    public void onBackPressed() {
        if (jzVideo.backPress()) {
            return;
        }
        super.onBackPressed();
    }


    @Override
    protected void onPause() {
        super.onPause();
        jzVideo.releaseAllVideos();
    }


}