package com.soomax.main.home;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhxdty.soomax.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.orhanobut.logger.Logger;
import com.soomax.pojo.SchoolDetailPojo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SchoolDetailFragment03 extends Fragment {
    Unbinder unbinder;
    static SchoolDetailFragment03 fragment;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    List<SchoolDetailPojo.ResBean.StadiumnoticeBean> list = new ArrayList<>();
    SchoolDetailAdapter schoolDetailAdapter;


    public static SchoolDetailFragment03 newInstance() {
        if (fragment == null) {
            fragment = new SchoolDetailFragment03();
        }
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.sd_home_school_detail_fragment03, null, false);
        unbinder = ButterKnife.bind(this, view);
        schoolDetailAdapter = new SchoolDetailAdapter(R.layout.sd_home_school_detail_fragment03_item, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(schoolDetailAdapter);
        onRefresh(SchoolDetail.schoolDetailPojo);
        return view;
    }


    public class SchoolDetailAdapter extends BaseQuickAdapter<SchoolDetailPojo.ResBean.StadiumnoticeBean, BaseViewHolder> {
        public SchoolDetailAdapter(int layoutResId, List<SchoolDetailPojo.ResBean.StadiumnoticeBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final SchoolDetailPojo.ResBean.StadiumnoticeBean item) {
            viewHolder.setText(R.id.tvGG, item.getContent());
        }
    }


    public void onRefresh(SchoolDetailPojo schoolDetailPojo) {
        if (schoolDetailPojo.getRes().getStadiumnotice() == null) return;
        list = schoolDetailPojo.getRes().getStadiumnotice();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
