package com.soomax.main.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hedgehog.ratingbar.RatingBar;
import com.kongzue.stacklabelview.StackLabel;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.meetsl.scardview.SCardView;
import com.orhanobut.hawk.Hawk;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.simascaffold.banner.GlideImageLoader;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MapNavigationUtil;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.common.BDLocationUtil;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.ResultPojo;
import com.soomax.pojo.StadiumsDetailPojo;
import com.takusemba.multisnaprecyclerview.MultiSnapRecyclerView;
import com.takusemba.multisnaprecyclerview.OnSnapListener;
import com.youth.banner.Banner;
import com.youth.banner.Transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.home_stadiums_detail)
public class StadiumsDetail extends BaseActivity {

    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.banner)
    Banner banner;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.stackLabelView)
    StackLabel stackLabelView;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvyysj)
    TextView tvyysj;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @BindView(R.id.tvzfbjx)
    TextView tvzfbjx;
    @BindView(R.id.tvyhpj)
    TextView tvyhpj;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    List<StadiumsDetailPojo.ResBean.StadiuminfoBean.CommentlistBean> list = new ArrayList<>();
    List<StadiumsDetailPojo.ResBean.StadiuminfoBean.GroundlistBean> list2 = new ArrayList<>();
    CommentAdapter commentAdapter;
    XMAdapter xmAdapter;
    int pageindex = 1;
    StadiumsDetailPojo stadiumsDetailPojo;
    @Autowired(name = "id")
    String id;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.multiSnapRecyclerView)
    MultiSnapRecyclerView multiSnapRecyclerView;
    @BindView(R.id.ivtel)
    ImageView ivtel;
    @BindView(R.id.ivLocation)
    ImageView ivLocation;
    @BindView(R.id.ivComment)
    ImageView ivComment;
    @BindView(R.id.svCardyh)
    SCardView svCardyh;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_stadiums_detail);
        ButterKnife.bind(this);

        commentAdapter = new CommentAdapter(R.layout.sd_home_school_detail_fragment04_item, list);
        xmAdapter = new XMAdapter(R.layout.sd_stadiums_xm_items, list2);
        recyclerView.setLayoutManager(new LinearLayoutManager(StadiumsDetail.this));
        recyclerView.setAdapter(commentAdapter);
        refreshLayout.setRefreshHeader(new MaterialHeader(StadiumsDetail.this).setShowBezierWave(false).setSize(1));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
                    @Override
                    public void result(BDLocation location) {
                        pageindex = 1;
                        getStadiumsDetail();
                    }
                });
            }
        });
        refreshLayout.autoRefresh();


        ivComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Hawk.contains("token") || MyTextUtils.isEmpty(Hawk.get("token"))) {
                    ARouter.getInstance().build(RoutePath.login)
                            .navigation();
                } else {
                    ARouter.getInstance().build(RoutePath.comment)
                            .withString("id", stadiumsDetailPojo.getRes().getStadiuminfo().getId())
                            .withString("flag", "stadiums")
                            .navigation();
                }
            }
        });


    }


    private void getStadiumsDetail() {

        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        params.put("pageCount", "20");
        params.put("pageNumber", pageindex + "");


        OkGo.<String>post(API.apiStadiumsDetail).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                refreshLayout.finishRefresh();
                if (MyTextUtils.isEmpty(response.body())) return;
                stadiumsDetailPojo = JSON.parseObject(response.body(), StadiumsDetailPojo.class);
                initUI(stadiumsDetailPojo);
            }
        });
    }


    void initUI(StadiumsDetailPojo stadiumsDetailPojo) {
        ArrayList<String> images = new ArrayList<>();
        String[] split = stadiumsDetailPojo.getRes().getStadiuminfo().getImgpath().split(",");
        for (String temp : split) {
            images.add(temp);
        }


        banner.setImages(images).setDelayTime(2000)
                .setBannerAnimation(Transformer.Stack)
                .setImageLoader(new GlideImageLoader())
                .start();


        tvTitle.setText(stadiumsDetailPojo.getRes().getStadiuminfo().getName());
        tvPhone.setText(stadiumsDetailPojo.getRes().getStadiuminfo().getConnectcode());
        tvyysj.setText("具体咨询场馆");
        tvLocation.setText(stadiumsDetailPojo.getRes().getStadiuminfo().getAddress());

        String label = stadiumsDetailPojo.getRes().getStadiuminfo().getSportname();
        ArrayList<String> labels = new ArrayList<String>();
        if (label.contains(",")) {
            for (String temp : label.split(",")) {
                labels.add(temp);
            }
        } else {
            labels.add(label);
        }
        stackLabelView.setLabels(labels);


        if (pageindex == 1) list.clear();
        if (stadiumsDetailPojo.getRes().getStadiuminfo().getCommentlist().size() == 0) {
            commentAdapter.loadMoreEnd();
        } else {
            commentAdapter.loadMoreComplete();
        }
        list.addAll(stadiumsDetailPojo.getRes().getStadiuminfo().getCommentlist());
        pageindex++;
        commentAdapter.notifyDataSetChanged();
        ratingBar.setStar(stadiumsDetailPojo.getRes().getStadiuminfo().getScore());
        ratingBar.setmClickable(false);
        tvzfbjx.setText(stadiumsDetailPojo.getRes().getStadiuminfo().getScore() + "分");


        if (stadiumsDetailPojo.getRes().getStadiuminfo().getGroundlist().size() > 0) {
            list2.addAll(stadiumsDetailPojo.getRes().getStadiuminfo().getGroundlist());
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        multiSnapRecyclerView.setLayoutManager(layoutManager);
        multiSnapRecyclerView.setAdapter(xmAdapter);
        multiSnapRecyclerView.setOnSnapListener(new OnSnapListener() {
            @Override
            public void snapped(int position) {

            }
        });


        ivLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapNavigationUtil.goNav(StadiumsDetail.this, getSupportFragmentManager(), stadiumsDetailPojo.getRes().getStadiuminfo().getLat(), stadiumsDetailPojo.getRes().getStadiuminfo().getLat());
            }
        });
        ivtel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                Uri data = Uri.parse("tel:" + stadiumsDetailPojo.getRes().getStadiuminfo().getConnectcode() + "");
                intent.setData(data);
                startActivity(intent);
            }
        });

    }

    @OnClick(R.id.svCardyh)
    public void onViewClicked() {
        lightToast("暂无优惠券");
    }


    public class XMAdapter extends BaseQuickAdapter<StadiumsDetailPojo.ResBean.StadiuminfoBean.GroundlistBean, BaseViewHolder> {
        public XMAdapter(int layoutResId, List<StadiumsDetailPojo.ResBean.StadiuminfoBean.GroundlistBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final StadiumsDetailPojo.ResBean.StadiuminfoBean.GroundlistBean item) {
            viewHolder.setText(R.id.tvName, item.getSportname());

            ImageView ivtx = viewHolder.getView(R.id.ivXM);


            if (item.getSportname().equals("篮球")) {
                Glide.with(StadiumsDetail.this).load(R.mipmap.sd_lq).into(ivtx);
            } else if (item.getSportname().equals("瑜伽")) {
                Glide.with(StadiumsDetail.this).load(R.mipmap.sd_yj).into(ivtx);
            } else if (item.getSportname().equals("游泳")) {
                Glide.with(StadiumsDetail.this).load(R.mipmap.sd_yy).into(ivtx);
            } else if (item.getSportname().equals("跑步")) {
                Glide.with(StadiumsDetail.this).load(R.mipmap.sd_pb).into(ivtx);
            } else if (item.getSportname().equals("健身")) {
                Glide.with(StadiumsDetail.this).load(R.mipmap.sd_js).into(ivtx);
            } else if (item.getSportname().equals("滑冰")) {
                Glide.with(StadiumsDetail.this).load(R.mipmap.sd_hb).into(ivtx);
            } else if (item.getSportname().equals("足球")) {
                Glide.with(StadiumsDetail.this).load(R.mipmap.sd_zq).into(ivtx);
            } else if (item.getSportname().equals("羽毛球")) {
                Glide.with(StadiumsDetail.this).load(R.mipmap.sd_ymq).into(ivtx);
            }


        }
    }


    public class CommentAdapter extends BaseQuickAdapter<StadiumsDetailPojo.ResBean.StadiuminfoBean.CommentlistBean, BaseViewHolder> {
        public CommentAdapter(int layoutResId, List<StadiumsDetailPojo.ResBean.StadiuminfoBean.CommentlistBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final StadiumsDetailPojo.ResBean.StadiuminfoBean.CommentlistBean item) {
            viewHolder.setText(R.id.tvPhone, item.getNickname())
                    .setText(R.id.tvTime, item.getCreatetime())
                    .setText(R.id.tvzan, item.getIslikecount() + "")
                    .setText(R.id.tvComment, item.getContent() + "");

            RatingBar ratingBar = viewHolder.getView(R.id.ratingBar);
            ratingBar.setmClickable(false);
            ratingBar.setStar(Float.parseFloat(item.getScore()));

            ImageView ivtx = viewHolder.getView(R.id.ivtx);
            Glide.with(StadiumsDetail.this).load(item.getHeadimgpath()).into(ivtx);

            ImageView ivzans = viewHolder.getView(R.id.ivzan);
            if (item.getIslike() == 1) {
                Glide.with(StadiumsDetail.this).load(R.mipmap.sd_zan_blue).into(ivzans);
            } else {
                Glide.with(StadiumsDetail.this).load(R.mipmap.sd_zan_gray).into(ivzans);
            }
            ivzans.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    zanlist(item.getId(), item.getIslike() == 1 ? "0" : "1");
                }
            });


        }
    }

    private void zanlist(String id, String like) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        params.put("islike", like);
        OkGo.<String>post(API.addappschoolcommentlike).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (!resultPojo.getCode().equals("200")) {
                    LightToasty.normal(StadiumsDetail.this, resultPojo.getMsg());
                } else {
                    refreshLayout.autoRefresh();
                }
            }
        });
    }

    @OnClick({R.id.linBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;

        }
    }
}