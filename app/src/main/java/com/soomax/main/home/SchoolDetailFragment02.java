package com.soomax.main.home;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.soomax.pojo.SchoolDetailPojo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SchoolDetailFragment02 extends Fragment {
    Unbinder unbinder;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    ArrayList<SchoolDetailPojo.ResBean.SportsortlistBean> list = new ArrayList<>();
    SortAdapter sortAdapter;

    static SchoolDetailFragment02 fragment;

    public static SchoolDetailFragment02 newInstance() {
        if (fragment == null) {
            fragment = new SchoolDetailFragment02();
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.sd_home_school_detail_fragment02, null, false);
        unbinder = ButterKnife.bind(this, view);
        sortAdapter = new SortAdapter(R.layout.sd_home_school_detail_fragment02_item, list);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(sortAdapter);
        return view;
    }


    public void onRefresh(SchoolDetailPojo schoolDetailPojo) {

        list.clear();
        list.addAll(schoolDetailPojo.getRes().getSportsortlist());
        sortAdapter.notifyDataSetChanged();
    }

    public class SortAdapter extends BaseQuickAdapter<SchoolDetailPojo.ResBean.SportsortlistBean, BaseViewHolder> {
        public SortAdapter(int layoutResId, ArrayList<SchoolDetailPojo.ResBean.SportsortlistBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final SchoolDetailPojo.ResBean.SportsortlistBean item) {


            ImageView ivno = viewHolder.getView(R.id.ivNo);
            ImageView ivtx = viewHolder.getView(R.id.ivtx);
            TextView tvno = viewHolder.getView(R.id.tvNo);
            tvno.setText(item.getSortnum()+"");
//            tvno.setText("21");

            viewHolder.setText(R.id.tvNickName, item.getNickname())
                    .setText(R.id.tvMin, item.getSporttime());
//                    .setText(R.id.tvNo,item.getSortnum());

            Glide.with(getActivity()).load(item.getHeadimgpath()).into(ivtx);

            ivno.setVisibility(View.GONE);

            switch (viewHolder.getAdapterPosition()) {
                case 0:
                    Glide.with(getActivity()).load(R.mipmap.sd_no1).into(ivno);
                    ivno.setVisibility(View.VISIBLE);
                    tvno.setVisibility(View.GONE);
                    break;
                case 1:
                    Glide.with(getActivity()).load(R.mipmap.sd_no2).into(ivno);
                    ivno.setVisibility(View.VISIBLE);
                    tvno.setVisibility(View.GONE);
                    break;
                case 2:
                    Glide.with(getActivity()).load(R.mipmap.sd_no3).into(ivno);
                    ivno.setVisibility(View.VISIBLE);
                    tvno.setVisibility(View.GONE);
                    break;
            }


        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
