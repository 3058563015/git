package com.soomax.main.home;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.common.BDLocationUtil;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.HealthSportPojo;
import com.soomax.pojo.HealthTeacherPojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HealthFragment02 extends Fragment {
    Unbinder unbinder;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    int pageindex = 1;
    ArrayList<HealthTeacherPojo.ResBean> list = new ArrayList<>();
    HealthTeacherAdapter healthTeacherAdapter;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    public static HealthFragment02 newInstance(String content) {
        Bundle args = new Bundle();
        HealthFragment02 fragment = new HealthFragment02();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.sd_home_health_fragment02, null, false);
        unbinder = ButterKnife.bind(this, view);
        healthTeacherAdapter = new HealthTeacherAdapter(R.layout.sd_home_health_teacher_items, list);
        healthTeacherAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getHealth01();
            }
        }, recyclerView);


        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));



        recyclerView.setAdapter(healthTeacherAdapter);
        refreshLayout.setRefreshHeader(new MaterialHeader(getActivity()).setShowBezierWave(false).setSize(1));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
                    @Override
                    public void result(BDLocation location) {
                        pageindex = 1;
                        getHealth01();
                    }
                });

            }
        });
        getHealth01();
        return view;
    }


    void getHealth01() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("pageNumber", pageindex + "");
        params.put("pageCount", "10");

        OkGo.<String>post(API.apiHealthTeacher).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                refreshLayout.finishRefresh();
                if (MyTextUtils.isEmpty(response.body())) return;
                HealthTeacherPojo healthSportPojo = JSON.parseObject(response.body(), HealthTeacherPojo.class);
                if (healthSportPojo.getCode().equals("200")) {
                    if (pageindex == 1) list.clear();
                    if (healthSportPojo.getRes().size() == 0) {
                        healthTeacherAdapter.loadMoreEnd();
                    } else {
                        healthTeacherAdapter.loadMoreComplete();
                    }
                    list.addAll(healthSportPojo.getRes());
                    pageindex++;
                    healthTeacherAdapter.notifyDataSetChanged();
                }

            }
        });
    }


    public class HealthTeacherAdapter extends BaseQuickAdapter<HealthTeacherPojo.ResBean, BaseViewHolder> {
        public HealthTeacherAdapter(int layoutResId, List<HealthTeacherPojo.ResBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final HealthTeacherPojo.ResBean item) {
            viewHolder.setText(R.id.tvName, item.getName())
                    .setText(R.id.tvDesc, item.getDescs())
                    .setText(R.id.tvXiangmu, item.getSportname());

            ImageView temp = viewHolder.getView(R.id.ivtx);

            RoundedCorners roundedCorners = new RoundedCorners(20);
            RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                    .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                    .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                    .error(R.drawable.sd_default);//图片加载失败后，显示的图
            Glide.with(getActivity()).load(item.getHeadimgpath()).apply(options).into(temp);


            viewHolder.getView(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ARouter.getInstance()
                            .build(RoutePath.home_teacher_detail)
                            .withString("id", item.getId())
                            .navigation();
                }
            });
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
