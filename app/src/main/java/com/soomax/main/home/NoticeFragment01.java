package com.soomax.main.home;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.common.BDLocationUtil;
import com.soomax.constant.API;
import com.soomax.pojo.Notice01Pojo;
import com.soomax.pojo.NoticePojo;
import com.soomax.pojo.ResultPojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NoticeFragment01 extends Fragment {
    Unbinder unbinder;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    int pageindex = 1;

    ArrayList<Notice01Pojo.ResBean.ListBean> list = new ArrayList<>();
    NoticeAdapter noticeAdapter;

    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    public static NoticeFragment01 newInstance(String content) {
        Bundle args = new Bundle();
        NoticeFragment01 fragment = new NoticeFragment01();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.sd_home_health_fragment01, null, false);
        unbinder = ButterKnife.bind(this, view);
        noticeAdapter = new NoticeAdapter(R.layout.sd_mynotice_items01, list);
        noticeAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getNotice01();
            }
        }, recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(noticeAdapter);
        refreshLayout.setRefreshHeader(new MaterialHeader(getActivity()).setShowBezierWave(false).setSize(1));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
                    @Override
                    public void result(BDLocation location) {
                        pageindex = 1;
                        getNotice01();
                    }
                });

            }
        });
        getNotice01();
        return view;
    }


    private void readed(String id) {
        Mynotice mynotice = (Mynotice) getActivity();
        mynotice.showLoading();
        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        params.put("islook", "1");
        OkGo.<String>post(API.apiNoticeRead).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                mynotice.dismissLoading();
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (!resultPojo.getCode().equals("200")) {
                    LightToasty.normal(getActivity(), resultPojo.getMsg());
                } else {
                    refreshLayout.autoRefresh();
                }
            }
        });
    }


    void getNotice01() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("pageNumber", pageindex + "");
        params.put("pageCount", "20");
        params.put("type", "1");

        OkGo.<String>post(API.apiNotice).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                refreshLayout.finishRefresh();
                if (MyTextUtils.isEmpty(response.body())) return;
                Notice01Pojo notice01Pojo = JSON.parseObject(response.body(), Notice01Pojo.class);
                if (notice01Pojo.getCode().equals("200")) {
                    if (pageindex == 1) list.clear();
                    if (notice01Pojo.getRes().getList().size() == 0) {
                        noticeAdapter.loadMoreEnd();
                    } else {
                        noticeAdapter.loadMoreComplete();
                    }
                    list.addAll(notice01Pojo.getRes().getList());
                    pageindex++;
                    noticeAdapter.notifyDataSetChanged();
                }

            }
        });
    }


    public class NoticeAdapter extends BaseQuickAdapter<Notice01Pojo.ResBean.ListBean, BaseViewHolder> {
        public NoticeAdapter(int layoutResId, List<Notice01Pojo.ResBean.ListBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final Notice01Pojo.ResBean.ListBean item) {

            viewHolder.setText(R.id.tvTime, item.getCreatetime())
                    .setText(R.id.tvAddress, item.getTitle())
                    .setText(R.id.tvContent, item.getContent())
                    .setText(R.id.tvStatus, item.getIslook().equals("1") ? "已读" : "未读");

            viewHolder.getView(R.id.linMain).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    readed(item.getId());
                }
            });


        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
