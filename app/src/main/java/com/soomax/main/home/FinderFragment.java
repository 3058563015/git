package com.soomax.main.home;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhxdty.soomax.R;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FinderFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.slidingTabLayout)
    SlidingTabLayout slidingTabLayout;
    @BindView(R.id.vp_main)
    ViewPager mViewPager;
    private FinnderPageAdapter mAdapter;
    private ArrayList<Fragment> mFragments;
    Context context;

    public static FinderFragment newInstance(String content) {
        Bundle args = new Bundle();
        FinderFragment fragment = new FinderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.sd_finder, null, false);
        unbinder = ButterKnife.bind(this, view);
        super.onCreate(savedInstanceState);

        mFragments = new ArrayList<>();
        mFragments.add(new FinderSubFrgment01());
        mFragments.add(new FinderSubFrgment02());
        mFragments.add(new FinderSubFrgment03());
        mFragments.add(new FinderSubFrgment04());
        mViewPager.setAdapter(new FinnderPageAdapter(getChildFragmentManager(), mFragments, new String[]{"最新资讯", "官方新闻", "赛事速递", "体育知识"}));
        slidingTabLayout.setViewPager(mViewPager);

        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
