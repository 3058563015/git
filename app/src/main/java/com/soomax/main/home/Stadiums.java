package com.soomax.main.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hedgehog.ratingbar.RatingBar;
import com.kongzue.stacklabelview.StackLabel;
import com.kongzue.stacklabelview.interfaces.OnLabelClickListener;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.Logger;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.common.BDLocationUtil;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.SportClassPojo;
import com.soomax.pojo.StadiumsPojo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.home_stadiums)
public class Stadiums extends BaseActivity {
    int pageindex = 1;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.linFilter)
    LinearLayout linFilter;
    @BindView(R.id.rlTop)
    RelativeLayout rlTop;
//    @BindView(R.id.refreshLayout)
//    SmartRefreshLayout refreshLayout;

    @BindView(R.id.stackLabelView)
    StackLabel stackLabelView;
    @BindView(R.id.lyXiangmu)
    LinearLayout lyXiangmu;
    SportClassPojo sportClassPojo;
    RVAdapter mAdapter;
    ArrayList<StadiumsPojo.ResBean> list = new ArrayList<>();
    ArrayList<SportClassPojo.ResBean> class_list = new ArrayList<>();
    @BindView(R.id.tvSumit)
    TextView tvSumit;
    String sportclass = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_stadiums);
        ButterKnife.bind(this);
        getSportClass();
//        getStadiums();
        mAdapter = new RVAdapter(R.layout.sd_home_stadiums_items, list);
//        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                getStadiums();
//            }
//        }, rv);
        rv.setLayoutManager(new LinearLayoutManager(Stadiums.this));
        rv.setAdapter(mAdapter);
        View empty = getLayoutInflater().inflate(R.layout.common_nodata, null, false);
        mAdapter.setEmptyView(empty);

//        refreshLayout.setRefreshHeader(new MaterialHeader(Stadiums.this).setShowBezierWave(false).setSize(1));
//        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh(RefreshLayout refreshlayout) {
//                BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
//                    @Override
//                    public void result(BDLocation location) {
//                        pageindex = 1;
//                        getStadiums();
//                    }
//                });
//
//            }
//        });

        stackLabelView.setOnLabelClickListener(new OnLabelClickListener() {
            @Override
            public void onClick(int index, View v, String s) {
                sportclass = sportClassPojo.getRes().get(stackLabelView.getSelectIndexArray()[0]).getId();
            }
        });


//        refreshLayout.autoRefresh();
        getStadiums();

    }


    @OnClick({R.id.linBack, R.id.linFilter, R.id.tvSumit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
            case R.id.tvSumit:
                lyXiangmu.setVisibility(View.GONE);
                rv.setVisibility(View.VISIBLE);
                pageindex = 1;
                getStadiums();
                break;
            case R.id.linFilter:
                if (lyXiangmu.getVisibility() == View.VISIBLE) {
                    lyXiangmu.setVisibility(View.GONE);
                    rv.setVisibility(View.VISIBLE);
                } else {
                    lyXiangmu.setVisibility(View.VISIBLE);
                    rv.setVisibility(View.GONE);
                }
                break;
        }
    }

    public class RVAdapter extends BaseQuickAdapter<StadiumsPojo.ResBean, BaseViewHolder> {
        public RVAdapter(int layoutResId, ArrayList<StadiumsPojo.ResBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final StadiumsPojo.ResBean item) {


            DecimalFormat decimalFormat = new DecimalFormat("0.0");//构造方法的字符格式这里如果小数不足2位,会以0补足.
            String score = decimalFormat.format(item.getScore());
            viewHolder.setText(R.id.tvName, item.getName())
                    .setText(R.id.tvAddress, item.getAddress())
                    .setText(R.id.tvScore, score + "分")
                    .setText(R.id.tvDistance, item.getDistance());

            RatingBar ratingBar = viewHolder.getView(R.id.ratingBar_school);
            ratingBar.setmClickable(false);
            ratingBar.setStar(item.getScore());
            TextView tvFlag = viewHolder.getView(R.id.tvFlag);
            tvFlag.setVisibility(View.INVISIBLE);

            ImageView temp = viewHolder.getView(R.id.ivNews);
            RoundedCorners roundedCorners = new RoundedCorners(20);
            RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                    .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                    .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                    .error(R.drawable.sd_default);//图片加载失败后，显示的图
            Glide.with(Stadiums.this).load(item.getStadiumfacepath()).apply(options).into(temp);

            viewHolder.getView(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ARouter.getInstance()
                            .build(RoutePath.home_stadiums_detail)
                            .withString("id", item.getId())
                            .navigation();
                }
            });
        }


    }

    private void getStadiums() {

        showLoading();
        final Map<String, String> params = new HashMap<String, String>();
        params.put("lng", Hawk.get("lng"));
        params.put("lat", Hawk.get("lat"));
//        params.put("pageNumber", pageindex + "");
//        params.put("pageCount", "20");
        params.put("sportclass", sportclass);

        OkGo.<String>post(API.apiStadiums).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
//                refreshLayout.finishRefresh();
                dismissLoading();
                StadiumsPojo stadiumsPojo = JSON.parseObject(response.body(), StadiumsPojo.class);
                if (stadiumsPojo.getCode().equals("200")) {
//                    if (pageindex == 1)
                    list.clear();
//                    if (stadiumsPojo.getRes().size() == 0) {
//                        mAdapter.loadMoreEnd();
//                    } else {
//                        mAdapter.loadMoreComplete();
//                    }
                    list.addAll(stadiumsPojo.getRes());
//                    pageindex++;
                    mAdapter.notifyDataSetChanged();
                }

            }
        });
    }


    private void getSportClass() {
//        if (Hawk.contains("sportClass")) {
//            stackLabelView.setLabels((ArrayList<String>) Hawk.get("sportClass"));
//            return;
//        }

        final Map<String, String> params = new HashMap<String, String>();
        OkGo.<String>post(API.apiSportClass).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {

                if (MyTextUtils.isEmpty(response.body())) return;
                sportClassPojo = JSON.parseObject(response.body(), SportClassPojo.class);


                int index = 0;
                if (sportClassPojo.getCode().equals("200")) {
                    ArrayList<SportClassPojo.ResBean> sportClassList = (ArrayList<SportClassPojo.ResBean>) sportClassPojo.getRes();
                    ArrayList<String> labels = new ArrayList<String>();


                    for (int i = 0; i < sportClassPojo.getRes().size(); i++) {
                        labels.add(sportClassPojo.getRes().get(i).getName());
                        Hawk.put("sportClass", labels);
                        if (sportclass.equals(sportClassPojo.getRes().get(i).getName())) {
                            index = i;
                        }
                    }
                    stackLabelView.setLabels((ArrayList<String>) Hawk.get("sportClass"));
                    RelativeLayout relativeLayout = (RelativeLayout) stackLabelView.getChildAt(index);
                    LinearLayout linearLayout = (LinearLayout) relativeLayout.getChildAt(0);
                    linearLayout.performClick();
                }
            }
        });


    }
}