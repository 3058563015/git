package com.soomax.main.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.soomax.base.BaseActivity;
import com.soomax.common.BDLocationUtil;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.NoticePojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.home_message)
public class Notice extends BaseActivity {
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.rv)
    RecyclerView rv;

    RVAdapter mAdapter;
    ArrayList<NoticePojo.ResBean> list = new ArrayList<>();
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    int pageindex = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_notice);
        ButterKnife.bind(this);

        mAdapter = new RVAdapter(R.layout.sd_notice_items, list);
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getMessage();
            }
        }, rv);
//        View empty = getLayoutInflater().inflate(R.layout.common_nomessage, null, false);
//        mAdapter.setEmptyView(empty);
        rv.setLayoutManager(new LinearLayoutManager(Notice.this));
        rv.setAdapter(mAdapter);

        refreshLayout.setRefreshHeader(new MaterialHeader(Notice.this).setShowBezierWave(false).setSize(1));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
                    @Override
                    public void result(BDLocation location) {
                        pageindex = 1;
                        getMessage();
                    }
                });

            }
        });

        refreshLayout.autoRefresh();
    }

    @OnClick(R.id.linBack)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
        }
    }

    public class RVAdapter extends BaseQuickAdapter<NoticePojo.ResBean, BaseViewHolder> {
        public RVAdapter(int layoutResId, ArrayList<NoticePojo.ResBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final NoticePojo.ResBean item) {

            viewHolder.setText(R.id.tvContent, item.getContent()).setText(R.id.tvTime, item.getPublishtime());

        }
    }




    private void getMessage() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("pageNumber", pageindex + "");
        params.put("pageCount", "10");
        OkGo.<String>post(API.apiMessage).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                refreshLayout.finishRefresh();
                NoticePojo noticePojo = JSON.parseObject(response.body(), NoticePojo.class);
                if (noticePojo.getCode().equals("200")) {
                    if (pageindex == 1) list.clear();
                    if (noticePojo.getRes().size() == 0) {
                        mAdapter.loadMoreEnd();
                    } else {
                        mAdapter.loadMoreComplete();
                    }
                    list.addAll(noticePojo.getRes());
                    pageindex++;
                    mAdapter.notifyDataSetChanged();
                }

            }
        });
    }
}
