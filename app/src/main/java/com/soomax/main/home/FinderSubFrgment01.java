package com.soomax.main.home;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.common.BDLocationUtil;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.FinderPojo;
import com.soomax.pojo.HealthSportPojo;
import com.soomax.pojo.HomeInfoPojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FinderSubFrgment01 extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    FinderPojo finderPojo;
    int pageindex = 1;
    ArrayList<FinderPojo.ResBean> list = new ArrayList<>();
    FinderAdapter finderAdapter;


    public static FinderSubFrgment01 newInstance(String content) {
        Bundle args = new Bundle();
        FinderSubFrgment01 fragment = new FinderSubFrgment01();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.sd_finder_list, null, false);
        unbinder = ButterKnife.bind(this, view);
        finderAdapter = new FinderAdapter(R.layout.sd_finder_list_item, list);
        finderAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getFinder();
            }
        }, recyclerView);

        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setAdapter(finderAdapter);
        refreshLayout.setRefreshHeader(new MaterialHeader(getActivity()).setShowBezierWave(false).setSize(1));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
                    @Override
                    public void result(BDLocation location) {
                        pageindex = 1;
                        getFinder();
                    }
                });

            }
        });
        getFinder();
        return view;
    }


    void getFinder() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("pageNumber", pageindex + "");
        params.put("pageCount", "10");
        params.put("ctype", "");

        OkGo.<String>post(API.apiFinder).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                refreshLayout.finishRefresh();
                if (MyTextUtils.isEmpty(response.body())) return;

                 finderPojo = JSON.parseObject(response.body(), FinderPojo.class);

                if (finderPojo.getCode().equals("200")) {
                    if (pageindex == 1) list.clear();
                    if (finderPojo.getRes().size() == 0) {
                        finderAdapter.loadMoreEnd();
                    } else {
                        finderAdapter.loadMoreComplete();
                    }
                    list.addAll(finderPojo.getRes());
                    pageindex++;
                    finderAdapter.notifyDataSetChanged();
                }

            }
        });
    }


    public class FinderAdapter extends BaseQuickAdapter<FinderPojo.ResBean, BaseViewHolder> {
        public FinderAdapter(int layoutResId, List<FinderPojo.ResBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final FinderPojo.ResBean item) {
            viewHolder.setText(R.id.tvNews, item.getDescs())
                    .setText(R.id.tvTime, item.getCreatetime());

            ImageView temp = viewHolder.getView(R.id.ivNews);
            RoundedCorners roundedCorners = new RoundedCorners(20);
            RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                    .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                    .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                    .error(R.drawable.sd_default);//图片加载失败后，显示的图


            Glide.with(getActivity()).load(item.getImgpath()).apply(options).into(temp);

            viewHolder.getView(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ARouter.getInstance().build(RoutePath.h5)
                            .withString("suffix", item.getContent())
                            .withString("title",item.getTitle())
                            .navigation();

                }
            });
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
