package com.soomax.main.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hedgehog.ratingbar.RatingBar;
import com.kongzue.stacklabelview.StackLabel;
import com.kongzue.stacklabelview.interfaces.OnLabelClickListener;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.common.BDLocationUtil;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.SchoolPojo;
import com.soomax.pojo.SportClassPojo;
import com.soomax.pojo.StadiumsPojo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.home_school)
public class School extends BaseActivity {

    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.linFilter)
    LinearLayout linFilter;
    @BindView(R.id.rlTop)
    RelativeLayout rlTop;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;


    int pageindex = 1;
    RVAdapter mAdapter;
    ArrayList<SchoolPojo.ResBean> list = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_school);
        ButterKnife.bind(this);
//        getSchool();
        mAdapter = new RVAdapter(R.layout.sd_home_stadiums_items, list);
//        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                getSchool();
//            }
//        }, rv);
//        View empty = getLayoutInflater().inflate(R.layout.common_nomessage, null, false);
//        mAdapter.setEmptyView(empty);
        rv.setLayoutManager(new LinearLayoutManager(School.this));
        rv.setAdapter(mAdapter);
        refreshLayout.setRefreshHeader(new MaterialHeader(School.this).setShowBezierWave(false).setSize(1));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
                    @Override
                    public void result(BDLocation location) {
                        pageindex = 1;
                        getSchool();
                    }
                });

            }
        });
        refreshLayout.autoRefresh();

    }


    @OnClick({R.id.linBack, R.id.linFilter})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
            case R.id.linFilter:
                ARouter.getInstance()
                        .build(RoutePath.home_school_search)
                        .navigation();
                break;
        }
    }

    public class RVAdapter extends BaseQuickAdapter<SchoolPojo.ResBean, BaseViewHolder> {
        public RVAdapter(int layoutResId, ArrayList<SchoolPojo.ResBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final SchoolPojo.ResBean item) {

            DecimalFormat decimalFormat = new DecimalFormat("0.0");//构造方法的字符格式这里如果小数不足2位,会以0补足.
            String score = decimalFormat.format(item.getScore());


            viewHolder.setText(R.id.tvName, item.getName())
                    .setText(R.id.tvAddress, item.getAddress())
                    .setText(R.id.tvScore, score + "分")
                    .setText(R.id.tvDistance, item.getDistance());


            RatingBar ratingBar = viewHolder.getView(R.id.ratingBar_school);
            ratingBar.setmClickable(false);
            ratingBar.setStar(item.getScore());

            ImageView temp = viewHolder.getView(R.id.ivNews);
            RoundedCorners roundedCorners = new RoundedCorners(20);
            RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                    .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                    .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                    .error(R.drawable.sd_default);//图片加载失败后，显示的图
            Glide.with(School.this).load(item.getStadiumfacepath())
                    .apply(options)
                    .into(temp);


            TextView tvFlag = viewHolder.getView(R.id.tvFlag);

            if (viewHolder.getAdapterPosition() == 0) {
                tvFlag.setVisibility(View.VISIBLE);
            } else {
               tvFlag.setVisibility(View.INVISIBLE);
            }
            viewHolder.getView(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ARouter.getInstance()
                            .build(RoutePath.home_school_detail)
                            .withString("id", item.getId())
                            .navigation();
                }
            });
        }


    }

    private void getSchool() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("lng", Hawk.get("lng", ""));
        params.put("lat", Hawk.get("lat", ""));
//        params.put("pageNumber", pageindex + "");
//        params.put("pageCount", "20");

        OkGo.<String>post(API.apiSchool).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                refreshLayout.finishRefresh();
                SchoolPojo schoolPojo = JSON.parseObject(response.body(), SchoolPojo.class);

                if (schoolPojo.getCode().equals("200")) {
                    if (pageindex == 1) list.clear();
                    if (schoolPojo.getRes().size() == 0) {
                        mAdapter.loadMoreEnd();
                    } else {
                        mAdapter.loadMoreComplete();
                    }
                    list.addAll(schoolPojo.getRes());
                    pageindex++;
                    mAdapter.notifyDataSetChanged();
                }

            }
        });
    }


}