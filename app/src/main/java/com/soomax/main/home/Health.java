package com.soomax.main.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.bhxdty.soomax.R;
import com.flyco.tablayout.SlidingTabLayout;
import com.soomax.base.BaseActivity;
import com.soomax.constant.RoutePath;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.home_health)
public class Health extends BaseActivity {
    @BindView(R.id.slidingTabLayout)
    SlidingTabLayout slidingTabLayout;
    @BindView(R.id.vp_main)
    ViewPager mViewPager;
    @BindView(R.id.linBack)
    LinearLayout linBack;
    private ArrayList<Fragment> mFragments;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_health);
        ButterKnife.bind(this);

        mFragments = new ArrayList<>();
        mFragments.add(new HealthFragment01());
        mFragments.add(new HealthFragment02());

        slidingTabLayout.setViewPager(mViewPager, new String[]{"运动课堂", "知名教练",}, this, mFragments);
    }

    @OnClick(R.id.linBack)
    public void onViewClicked() {
        finish();
    }
}
