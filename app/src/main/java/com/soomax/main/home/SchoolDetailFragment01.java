package com.soomax.main.home;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.kongzue.dialog.v2.DialogSettings;
import com.kongzue.dialog.v2.MessageDialog;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.simascaffold.banner.GlideImageLoader;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MapNavigationUtil;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.main.my.Authentication;
import com.soomax.pojo.LoginPojo;
import com.soomax.pojo.ResultPojo;
import com.soomax.pojo.SchoolDetailPojo;
import com.soomax.pojo.SqSchoolPojo;
import com.youth.banner.Transformer;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SchoolDetailFragment01 extends Fragment {
    static SchoolDetailFragment01 fragment;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.tvgjlx)
    TextView tvgjlx;
    @BindView(R.id.tvzbss)
    TextView tvzbss;
    @BindView(R.id.tvkfsj)
    TextView tvkfsj;
    @BindView(R.id.tvssrs)
    TextView tvssrs;
    @BindView(R.id.tvcdrq)
    TextView tvcdrq;
    @BindView(R.id.tvSumit)
    TextView tvSumit;
    Unbinder unbinder;
    @BindView(R.id.ivPhone)
    ImageView ivPhone;
    @BindView(R.id.ivLocation)
    ImageView ivLocation;

    public static SchoolDetailFragment01 newInstance() {
        if (fragment == null) {
            fragment = new SchoolDetailFragment01();
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.sd_home_school_detail_fragment01, null, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void onRefresh(SchoolDetailPojo schoolDetailPojo) {

        SchoolDetailPojo.ResBean.SchoolinfoBean item = schoolDetailPojo.getRes().getSchoolinfo();
        tvPhone.setText(item.getConnectcode());
        tvLocation.setText(item.getAddress());
        tvDesc.setText(item.getStadiumdesc());
        tvgjlx.setText(item.getBusroute());
        tvzbss.setText(item.getPeriphery());
        tvkfsj.setText(item.getWorkdate());
        tvssrs.setText(item.getPeoplenumber() + "");
        tvcdrq.setText(item.getPopularity() + "");


        tvSumit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addappschoolapply(item.getId());
            }
        });


        switch (item.getApplystatus()) {
            case 0:
                tvSumit.setText("已申请");
                tvSumit.setClickable(false);
                tvSumit.setBackground(getResources().getDrawable(R.drawable.btn_click_gray));

                break;
            case 1:
                tvSumit.setText("申请入场");
                tvSumit.setClickable(true);


                break;
            case 2:
                tvSumit.setText("申请中");
                tvSumit.setClickable(false);
                tvSumit.setBackground(getResources().getDrawable(R.drawable.btn_click_gray));

                break;
        }

        if (item.getStatus() != null && item.getStatus().equals("0")) {
            tvSumit.setText("未开放");
            tvSumit.setClickable(false);
            tvSumit.setBackground(getResources().getDrawable(R.drawable.btn_click_gray));
        } else {

        }

        ivPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                Uri data = Uri.parse("tel:" + item.getConnectcode() + "");
                intent.setData(data);
                startActivity(intent);
            }
        });

        ivLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapNavigationUtil.goNav(getActivity(), getActivity().getSupportFragmentManager(),
                        item.getLat(), item.getLng());
            }
        });


        if (!Hawk.contains("token") || MyTextUtils.isEmpty(Hawk.get("token"))) return;

        DialogSettings.dialogContentTextInfo.setGravity(Gravity.LEFT);
        if (schoolDetailPojo.getRes().getUserpromise() == 1) {
            MessageDialog.show(getActivity(), "承诺书", "为保证学校体育设施开放工作的正常开展，维护学校正常的教育教学秩序，在进入学校进行体育锻炼期间，本人承诺做到以下几点：\n  \n一、遵守学校关于体育场地设施开放的各项制度和规定，在学校规定的时段、范围、项目中进行体育锻炼。\n 二、履行入校登记手续，办理健身准入许可必须实事求是为学校提供真实信息。不满12周岁未成年人须在监护人带领下进入校园进行体育锻炼。\n 三、严禁健身人员将车辆（自行车、电动车、摩托车、汽车等）驶入校园；严禁携带宠物进入校园进行体育锻炼；严禁酗酒者进入校园进行体育锻炼。严禁在学校开放场地内进行足球、广场舞、轮滑等不安全运动项目。\n 四、举止文明，不随地吐痰，不随地大小便，不乱扔垃圾，校园内禁止吸烟，自觉维护学校的环境卫生和绿化。\n 五、爱护本校体育场地、设施、器械和花草树木，对故意损坏器材、破坏公物的要照价赔偿。\n 六、在体育健身过程中健身人员要保管好个人财物，并做好自身伤害安全保护，不做危险动作，不滋事斗殴。\n七、本校对入校健身人员的人身安全、财产安全不承担任何经济赔偿及法律责任，本协议自签订之日起生效。", "同意", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    submitPromise();
                }
            });
        }


    }


    private void addappschoolapply(String id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("schoolid", id);
        OkGo.<String>post(API.addappschoolapply).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                if (MyTextUtils.isEmpty(response.body())) return;
                SqSchoolPojo sqSchoolPojo = JSON.parseObject(response.body(), SqSchoolPojo.class);
                LightToasty.normal(getActivity(), sqSchoolPojo.getMsg());
                if (sqSchoolPojo.getCode().equals("200")) {
                    getSchoolDetail(id);
                } else if (sqSchoolPojo.getCode().equals("101")) {
                    LightToasty.warning(getActivity(), sqSchoolPojo.getMsg());
                    ARouter.getInstance().build(RoutePath.my_authentication)
                            .navigation();
                } else if (sqSchoolPojo.getCode().equals("0")) {
                    ARouter.getInstance().build(RoutePath.login)
                            .navigation();
                } else {
                    LightToasty.warning(getActivity(), sqSchoolPojo.getMsg());
                }
            }
        });
    }

    private void submitPromise() {
        final Map<String, String> params = new HashMap<String, String>();
//        params.put("promisestatus", "1");
        OkGo.<String>post(API.appedituserschoolpromise).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (resultPojo.getCode().equals("200")) {
                    tvSumit.setBackground(getResources().getDrawable(R.drawable.btn_click_gray));
                } else {
                    LightToasty.warning(getActivity(), resultPojo.getMsg());
                }


            }
        });
    }

    void getusr() {
        LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", usr.getUsername());
        params.put("userpassword", usr.getUserpassword());
        OkGo.<String>post(API.apiOauthLogin).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                LoginPojo loginPojo = JSON.parseObject(response.body(), LoginPojo.class);
                if (loginPojo.getCode().equals("200")) {

                    LoginPojo.ResBean usr = loginPojo.getRes();
                    Hawk.put("usr", usr);
                    Hawk.put("token", loginPojo.getToken());

                    HttpHeaders headers = new HttpHeaders();
                    headers.put("uid", usr.getId());
                    headers.put("token", loginPojo.getToken());
                    OkGo.getInstance().addCommonHeaders(headers);


                }
            }
        });
    }

    private void getSchoolDetail(String id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);

        OkGo.<String>post(API.apiSchoolDetail).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                if (MyTextUtils.isEmpty(response.body())) return;
                SchoolDetailPojo schoolDetailPojo = JSON.parseObject(response.body(), SchoolDetailPojo.class);
                switch (schoolDetailPojo.getRes().getSchoolinfo().getApplystatus()) {
                    case 0:
                        tvSumit.setText("已申请");
                        tvSumit.setClickable(false);
                        break;
                    case 1:
                        tvSumit.setText("申请入场");
                        tvSumit.setClickable(true);
                        break;
                    case 2:
                        tvSumit.setText("申请中");
                        tvSumit.setClickable(false);
                        break;
                }
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
