package com.soomax.main.home;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.autonavi.amap.mapcore.interfaces.IText;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hedgehog.ratingbar.RatingBar;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.common.BDLocationUtil;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.ResultPojo;
import com.soomax.pojo.SchoolCommentsPojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SchoolDetailFragment04 extends Fragment {
    Unbinder unbinder;
    static SchoolDetailFragment04 fragment;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    List<SchoolCommentsPojo.ResBean.CommentlistBean> list = new ArrayList<>();
    SchoolCommentAdapter schoolCommentAdapter;
    int pageindex = 1;
    @BindView(R.id.tvzfbjx)
    TextView tvzfbjx;
    @BindView(R.id.tvyhpj)
    TextView tvyhpj;
    @BindView(R.id.ivComment)
    ImageView ivComment;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;

    public static SchoolDetailFragment04 newInstance() {
        if (fragment == null) {
            fragment = new SchoolDetailFragment04();
        }
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.sd_home_school_detail_fragment04, null, false);
        unbinder = ButterKnife.bind(this, view);
        list.clear();
        schoolCommentAdapter = new SchoolCommentAdapter(R.layout.sd_home_school_detail_fragment04_item, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(schoolCommentAdapter);
        refreshLayout.setRefreshHeader(new MaterialHeader(getActivity()).setShowBezierWave(false).setSize(1));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
                    @Override
                    public void result(BDLocation location) {
                        pageindex = 1;
                        getComment();
                    }
                });

            }
        });
        getComment();
        return view;
    }

    @OnClick(R.id.ivComment)
    public void onViewClicked() {
        if (!Hawk.contains("token") || MyTextUtils.isEmpty(Hawk.get("token"))) {
            ARouter.getInstance().build(RoutePath.login)
                    .navigation();
        } else {
            ARouter.getInstance().build(RoutePath.comment)
                    .withString("id", SchoolDetail.schoolDetailPojo.getRes().getSchoolinfo().getId())
                    .withString("flag", "school")
                    .navigation();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }


    public class SchoolCommentAdapter extends BaseQuickAdapter<SchoolCommentsPojo.ResBean.CommentlistBean, BaseViewHolder> {
        public SchoolCommentAdapter(int layoutResId, List<SchoolCommentsPojo.ResBean.CommentlistBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final SchoolCommentsPojo.ResBean.CommentlistBean item) {
            viewHolder.setText(R.id.tvPhone, item.getNickname())
                    .setText(R.id.tvTime, item.getCreatetime())
                    .setText(R.id.tvzan, item.getIslikecount() + "")
                    .setText(R.id.tvComment, item.getContent() + "");
            RatingBar ratingBar = viewHolder.getView(R.id.ratingBar);
            ratingBar.setStar(Float.parseFloat(item.getScore()));
            ratingBar.setmClickable(false);
            ImageView ivtx = viewHolder.getView(R.id.ivtx);
            Glide.with(getActivity()).load(item.getHeadimgpath()).into(ivtx);


            ImageView ivzans = viewHolder.getView(R.id.ivzan);
            if (item.getIslike() == 1) {
                Glide.with(getActivity()).load(R.mipmap.sd_zan_blue).into(ivzans);
            } else {
                Glide.with(getActivity()).load(R.mipmap.sd_zan_gray).into(ivzans);
            }
            ivzans.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    zanlist(item.getId(), item.getIslike() == 1 ? "0" : "1");
                }
            });


        }
    }


    private void zanlist(String id, String like) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        params.put("islike", like);
        OkGo.<String>post(API.addappschoolcommentlike).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (!resultPojo.getCode().equals("200")) {
                    LightToasty.normal(getActivity(), resultPojo.getMsg());
                } else {
                    refreshLayout.autoRefresh();
                }
            }
        });
    }


    private void getComment() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("pageNumber", pageindex + "");
        params.put("pageCount", "10");
        params.put("id", SchoolDetail.schoolDetailPojo.getRes().getSchoolinfo().getId());


        OkGo.<String>post(API.apiSchoolComments).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                refreshLayout.finishRefresh();
                SchoolCommentsPojo commentsPojo = JSON.parseObject(response.body(), SchoolCommentsPojo.class);
                if (commentsPojo.getCode().equals("200")) {
                    ratingBar.setStar(Float.parseFloat(commentsPojo.getRes().getSchoolscre()));

                    tvzfbjx.setText(commentsPojo.getRes().getSchoolscre() + "分");
                    tvyhpj.setText("用户评价（" + commentsPojo.getSize() + ")");

                    if (pageindex == 1) list.clear();
                    if (commentsPojo.getRes().getCommentlist().size() == 0) {
                        schoolCommentAdapter.loadMoreEnd();
                    } else {
                        schoolCommentAdapter.loadMoreComplete();
                    }
                    list.addAll(commentsPojo.getRes().getCommentlist());
                    pageindex++;
                    schoolCommentAdapter.notifyDataSetChanged();

                }

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
