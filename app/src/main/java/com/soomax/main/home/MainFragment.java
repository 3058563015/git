package com.soomax.main.home;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hedgehog.ratingbar.RatingBar;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.simascaffold.banner.GlideRoundImageLoader;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.main.Main;
import com.soomax.pojo.HomeInfoPojo;
import com.soomax.pojo.NoticeNumPojo;
import com.soomax.pojo.NoticePojo;
import com.youth.banner.Banner;
import com.youth.banner.Transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class MainFragment extends Fragment {
    @BindView(R.id.banner)
    Banner banner;
    @BindView(R.id.tvSearch)
    TextView tvSearch;
    Unbinder unbinder;

    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.imagecricle)
    ImageView imagecricle;
    @BindView(R.id.recyclerView_tc)
    RecyclerView recyclerViewTc;
    @BindView(R.id.recyclerView_cd)
    RecyclerView recyclerViewCd;
    @BindView(R.id.recyclerView_cs)
    RecyclerView recyclerViewCs;
    @BindView(R.id.rlSearch)
    RelativeLayout rlSearch;
    @BindView(R.id.ivNotice)
    ImageView ivNotice;
    @BindView(R.id.lincg)
    LinearLayout lincg;
    @BindView(R.id.lincd)
    LinearLayout lincd;
    @BindView(R.id.linjk)
    LinearLayout linjk;
    @BindView(R.id.linss)
    LinearLayout linss;
    @BindView(R.id.lintz)
    LinearLayout lintz;

    ArrayList<String> images = new ArrayList<>();
    ArrayList<HomeInfoPojo.ResBean.RecommendlistBean> tuijianList = new ArrayList<>();
    ArrayList<HomeInfoPojo.ResBean.SportteacherlistBean> tcList = new ArrayList<>();
    ArrayList<HomeInfoPojo.ResBean.SchoollistBean> cdList = new ArrayList<>();
    ArrayList<HomeInfoPojo.ResBean.StadiumlistBean> csList = new ArrayList<>();
    HomeInfoPojo homeInfoPojo;
    TuijianAdapter tuijianAdapter;
    TeacherAdapter teacherAdapter;
    CdAdapter cdAdapter;
    CsAdapter csAdapter;
    @BindView(R.id.linlocaiton)
    LinearLayout linlocaiton;
    @BindView(R.id.app_bar_layout)
    LinearLayout appBarLayout;
    @BindView(R.id.ivFlag)
    ImageView ivFlag;
    @BindView(R.id.ivsx)
    ImageView ivsx;
    @BindView(R.id.tvAddress)
    TextView tvAddress;


    public static MainFragment newInstance(String content) {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.sd_home_main, null, false);
        unbinder = ButterKnife.bind(this, view);

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == 0) {
                    imagecricle.setVisibility(View.VISIBLE);
                    rlSearch.setBackground(getResources().getDrawable(R.drawable.sd_edit_bg_white));

                } else {
                    imagecricle.setVisibility(View.GONE);
                    rlSearch.setBackground(getResources().getDrawable(R.drawable.sd_edit_bg));
                }
            }
        });


        tuijianAdapter = new TuijianAdapter(R.layout.sd_home_list_item_news, tuijianList);
        recyclerView.setAdapter(tuijianAdapter);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mLinearLayoutManager.setSmoothScrollbarEnabled(true);
        mLinearLayoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);


        teacherAdapter = new TeacherAdapter(R.layout.sd_home_list_item_teachers, tcList);
        recyclerViewTc.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewTc.setAdapter(teacherAdapter);

        cdAdapter = new CdAdapter(R.layout.sd_home_list_item_changdi, cdList);
        recyclerViewCd.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerViewCd.setAdapter(cdAdapter);

        csAdapter = new CsAdapter(R.layout.sd_home_list_item_changguan, csList);
        recyclerViewCs.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerViewCs.setAdapter(csAdapter);


        refreshLayout.setRefreshHeader(new MaterialHeader(getActivity()).setShowBezierWave(false).setSize(1));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                getMainPageInfo();
            }
        });

        refreshLayout.autoRefresh();
//        getNoticeNum();
        return view;
    }




    private void getNoticeNum() {
        final Map<String, String> params = new HashMap<String, String>();
        OkGo.<String>post(API.apiNoticeNum).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                if (MyTextUtils.isEmpty(response.body())) return;
                NoticeNumPojo noticeNumPojo = JSON.parseObject(response.body(), NoticeNumPojo.class);
                if (noticeNumPojo.getCode().equals("200")) {
                    if (noticeNumPojo.getRes().getNoticenum() > 0) {
                        Glide.with(getActivity()).load(R.mipmap.sd_messge_sm_red).into(ivNotice);
                    }else {
                        Glide.with(getActivity()).load(R.mipmap.sd_message_sm).into(ivNotice);
                    }
                }

            }
        });
    }


    void getMainPageInfo() {
        Main main = (Main) getActivity();

//        main.showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("lng", Hawk.get("lng"));
        params.put("lat", Hawk.get("lat"));
        OkGo.<String>post(API.apiHomeInfo).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
//                main.dismissLoading();
                refreshLayout.finishRefresh();
                if (MyTextUtils.isEmpty(response.body())) return;
                homeInfoPojo = JSON.parseObject(response.body(), HomeInfoPojo.class);
                if (homeInfoPojo.getCode().equals("200")) {
                    //banner列表
                    images.clear();
                    for (HomeInfoPojo.ResBean.BannerlistBean temp : homeInfoPojo.getRes().getBannerlist()) {
                        images.add(temp.getImgpath());
                    }
                    banner.setImages(images).setDelayTime(2000)
                            .setBannerAnimation(Transformer.Stack)
                            .setImageLoader(new GlideRoundImageLoader()).start();
                    //推荐列表
                    tuijianList.clear();
                    tuijianList.addAll((ArrayList<HomeInfoPojo.ResBean.RecommendlistBean>) homeInfoPojo.getRes().getRecommendlist());
                    tuijianAdapter.notifyDataSetChanged();
                    //教练列表
                    tcList.clear();
                    tcList.addAll((ArrayList<HomeInfoPojo.ResBean.SportteacherlistBean>) homeInfoPojo.getRes().getSportteacherlist());
                    teacherAdapter.notifyDataSetChanged();
                    //场地列表
                    cdList.clear();
                    cdList.addAll((ArrayList<HomeInfoPojo.ResBean.SchoollistBean>) homeInfoPojo.getRes().getSchoollist());
                    cdAdapter.notifyDataSetChanged();
                    //场所列表
                    csList.clear();
                    csList.addAll((ArrayList<HomeInfoPojo.ResBean.StadiumlistBean>) homeInfoPojo.getRes().getStadiumlist());
                    csAdapter.notifyDataSetChanged();
                }


            }
        });
    }

    @OnClick({R.id.linlocaiton, R.id.ivNotice, R.id.rlSearch, R.id.lincg, R.id.lincd, R.id.linjk, R.id.linss, R.id.lintz})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivNotice:
                if (!Hawk.contains("token") || MyTextUtils.isEmpty(Hawk.get("token"))) {
                    LightToasty.normal(getActivity(), "请先登录!");
                } else {
                    ARouter.getInstance().build(RoutePath.sd_notice).navigation();
                }
                break;
            case R.id.rlSearch:
                ARouter.getInstance().build(RoutePath.home_stadiums_search).navigation();
                break;
            case R.id.lincg:
                ARouter.getInstance().build(RoutePath.home_stadiums).navigation();
                break;
            case R.id.lincd:
                ARouter.getInstance().build(RoutePath.home_school).navigation();
                break;
            case R.id.linjk:
                ARouter.getInstance().build(RoutePath.home_health).navigation();
                break;
            case R.id.linss:
                ARouter.getInstance().build(RoutePath.home_saishi).navigation();
                break;
            case R.id.lintz:
                ARouter.getInstance().build(RoutePath.home_message).navigation();
                break;
            case R.id.linlocaiton:
                ARouter.getInstance().build(RoutePath.home_chooselocation).navigation();
                break;
        }
    }


    public class TuijianAdapter extends BaseQuickAdapter<HomeInfoPojo.ResBean.RecommendlistBean, BaseViewHolder> {
        public TuijianAdapter(int layoutResId, List<HomeInfoPojo.ResBean.RecommendlistBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final HomeInfoPojo.ResBean.RecommendlistBean item) {
            viewHolder.setText(R.id.tvNews, item.getDescs())
                    .setText(R.id.tvTime, item.getCreatetime());

            ImageView temp = viewHolder.getView(R.id.ivNews);
            RoundedCorners roundedCorners = new RoundedCorners(20);
            RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                    .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                    .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                    .error(R.drawable.sd_default);//图片加载失败后，显示的图


            Glide.with(getActivity()).load(item.getImgpath()).apply(options).into(temp);

            viewHolder.getView(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ARouter.getInstance().build(RoutePath.h5)
                            .withString("suffix", item.getContent())
                            .withString("title", item.getTitle())
                            .navigation();

                }
            });
        }
    }


    public class TeacherAdapter extends BaseQuickAdapter<HomeInfoPojo.ResBean.SportteacherlistBean, BaseViewHolder> {
        public TeacherAdapter(int layoutResId, List<HomeInfoPojo.ResBean.SportteacherlistBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final HomeInfoPojo.ResBean.SportteacherlistBean item) {
            viewHolder.setText(R.id.tvName, item.getName())
                    .setText(R.id.tvXiangmu, item.getSportname());

            ImageView temp = viewHolder.getView(R.id.ivTouxiang);

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                    .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                    .error(R.drawable.sd_default);//图片加载失败后，显示的图


            Glide.with(getActivity()).load(item.getHeadimgpath())
                    .apply(options)
                    .into(temp);

            viewHolder.getView(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ARouter.getInstance()
                            .build(RoutePath.home_teacher_detail)
                            .withString("id", item.getId())
                            .navigation();
                }
            });

        }
    }

    public class CdAdapter extends BaseQuickAdapter<HomeInfoPojo.ResBean.SchoollistBean, BaseViewHolder> {
        public CdAdapter(int layoutResId, List<HomeInfoPojo.ResBean.SchoollistBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final HomeInfoPojo.ResBean.SchoollistBean item) {
            viewHolder.setText(R.id.tvName, item.getName())
                    .setText(R.id.tvTime, item.getAddress())
                    .setText(R.id.tvHot, item.getPopularity() + "")
                    .setText(R.id.tv_distance, item.getDistance() + "");

            ImageView temp = viewHolder.getView(R.id.ivNews);
            RoundedCorners roundedCorners = new RoundedCorners(20);
            RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                    .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                    .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                    .error(R.drawable.sd_default);//图片加载失败后，显示的图
            Glide.with(getActivity()).load(item.getStadiumfacepath()).apply(options).into(temp);

            viewHolder.getView(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ARouter.getInstance()
                            .build(RoutePath.home_school_detail)
                            .withString("id", item.getId())
                            .navigation();
                }
            });
        }
    }

    public class CsAdapter extends BaseQuickAdapter<HomeInfoPojo.ResBean.StadiumlistBean, BaseViewHolder> {
        public CsAdapter(int layoutResId, List<HomeInfoPojo.ResBean.StadiumlistBean> list) {
            super(layoutResId, list);
        }

        @Override
        protected void convert(final BaseViewHolder viewHolder, final HomeInfoPojo.ResBean.StadiumlistBean item) {
            viewHolder.setText(R.id.tvName, item.getName() + "")
                    .setText(R.id.tvTime, item.getAddress() + "")
                    .setText(R.id.tvDistance, item.getDistance() + "");

            ImageView temp = viewHolder.getView(R.id.ivNews);
            RoundedCorners roundedCorners = new RoundedCorners(20);
            RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                    .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                    .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                    .error(R.drawable.sd_default);//图片加载失败后，显示的图
            Glide.with(getActivity()).load(item.getStadiumfacepath()).apply(options).into(temp);
            RatingBar ratingBar = viewHolder.getView(R.id.ratingBar_school);
            ratingBar.setmClickable(false);
            ratingBar.setStar(item.getScore());


            viewHolder.getView(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ARouter.getInstance()
                            .build(RoutePath.home_stadiums_detail)
                            .withString("id", item.getId())
                            .navigation();
                }
            });
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onResume() {
        super.onResume();
        tvAddress.setText(Hawk.get("address"));
        if (!Hawk.contains("token") || MyTextUtils.isEmpty(Hawk.get("token"))) {
        } else {
          getNoticeNum();
        }

    }
}
