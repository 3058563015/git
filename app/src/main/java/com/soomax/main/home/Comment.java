package com.soomax.main.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.hedgehog.ratingbar.RatingBar;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.ResultPojo;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.comment)
public class Comment extends BaseActivity {
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.etComment)
    EditText etComment;
    @BindView(R.id.tvNum)
    TextView tvNum;
    @BindView(R.id.rat_test)
    RatingBar ratTest;
    @BindView(R.id.tvSubmit)
    TextView tvSubmit;

    @Autowired(name = "id")
    String id;

    @Autowired(name = "flag")
    String flag;

    String score;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_comment);
        ButterKnife.bind(this);

        etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int cd = 140 - s.length();
                tvNum.setText(cd + "/140");
            }
        });


        ratTest.setOnRatingChangeListener(
                new RatingBar.OnRatingChangeListener() {
                    @Override
                    public void onRatingChange(float RatingCount) {
//                        lightToast(RatingCount + "");
                        score = RatingCount + "";

                    }
                });


    }


    private void submit() {
        if (MyTextUtils.isEmpty(etComment.getText().toString())) {
            lightToast("评论内容不能为空");
            return;
        }
        if (MyTextUtils.isEmpty(score)) {
            lightToast("星星评价不能为空");
            return;
        }

        final Map<String, String> params = new HashMap<String, String>();
        params.put("content", etComment.getText().toString());
        params.put("id", id);
        params.put("score", score);
        showLoading();

        String url = "";
        if (flag.equals("school")) {
            url = API.addappschoolcommnet;
        } else if (flag.equals("video")) {
            url = API.addappsportroomcommnet;
        } else if (flag.equals("stadiums")) {
            url = API.addappschoolcommnet;
        }
        OkGo.<String>post(url).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                dismissLoading();
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (resultPojo.getCode().equals("200")) {
                    finish();
                } else {
                    lightToast(resultPojo.getMsg());
                }
            }
        });
    }

    @OnClick({R.id.linBack, R.id.tvSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
            case R.id.tvSubmit:
                submit();
                break;
        }
    }
}
