package com.soomax.main.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.flyco.tablayout.SlidingTabLayout;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.simascaffold.banner.GlideImageLoader;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.SchoolDetailPojo;
import com.youth.banner.Banner;
import com.youth.banner.Transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

@Route(path = RoutePath.home_school_detail)
public class SchoolDetail extends BaseActivity {

    @BindView(R.id.slidingTabLayout)
    SlidingTabLayout slidingTabLayout;
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.tvBarTitle)
    TextView tvBarTitle;
    @BindView(R.id.vp_main)
    ViewPager mViewPager;
    @BindView(R.id.banner)
    Banner banner;

    private ArrayList<Fragment> mFragments;
    @Autowired(name = "id")
    String id;
    public static SchoolDetailPojo schoolDetailPojo;
    ArrayList<String> images = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_school_detail);
        ButterKnife.bind(this);
        getSchoolDetail();
        mFragments = new ArrayList<>();
        mFragments.add(SchoolDetailFragment01.newInstance());
        mFragments.add(SchoolDetailFragment02.newInstance());
        mFragments.add(SchoolDetailFragment03.newInstance());
        mFragments.add(SchoolDetailFragment04.newInstance());
        slidingTabLayout.setViewPager(mViewPager, new String[]{"场地介绍", "运动排名", "场地公告", "场地评价"}, this, mFragments);

        linBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void getSchoolDetail() {
        showLoading();
        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);

        OkGo.<String>post(API.apiSchoolDetail).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                dismissLoading();
                if (MyTextUtils.isEmpty(response.body())) return;
                schoolDetailPojo = JSON.parseObject(response.body(), SchoolDetailPojo.class);
                images.clear();
                String[] split = schoolDetailPojo.getRes().getSchoolinfo().getImgpath().split(",");
                for (String temp : split) {
                    images.add(temp);
                }
                banner.setImages(images).setDelayTime(2000)
                        .setBannerAnimation(Transformer.Stack)
                        .setImageLoader(new GlideImageLoader())
                        .start();
                SchoolDetailFragment01.newInstance().onRefresh(schoolDetailPojo);
                SchoolDetailFragment02.newInstance().onRefresh(schoolDetailPojo);
                SchoolDetailFragment03.newInstance().onRefresh(schoolDetailPojo);
                tvBarTitle.setText(schoolDetailPojo.getRes().getSchoolinfo().getName());


            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}