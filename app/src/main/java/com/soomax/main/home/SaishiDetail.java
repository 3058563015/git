package com.soomax.main.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.simascaffold.banner.GlideRoundImageLoader;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.SaishiDetailPojo;
import com.youth.banner.Banner;
import com.youth.banner.Transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.home_saishi_detail)
public class SaishiDetail extends BaseActivity {

    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.tvBarTitle)
    TextView tvBarTitle;
    @BindView(R.id.banner)
    Banner banner;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.tvbmsz)
    TextView tvbmsz;
    @BindView(R.id.tvbssj)
    TextView tvbssj;
    @BindView(R.id.tvbsdd)
    TextView tvbsdd;
    @BindView(R.id.tvbmfy)
    TextView tvbmfy;
    @BindView(R.id.tvbmzg)
    TextView tvbmzg;
    @BindView(R.id.tvryxe)
    TextView tvryxe;
    @BindView(R.id.tvbmzt)
    TextView tvbmzt;
    @BindView(R.id.tvRemark)
    TextView tvRemark;
    @BindView(R.id.tvSumit)
    TextView tvSumit;


    SaishiDetailPojo saishiDetailPojo;
    @Autowired(name = "id")
    String id;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_ss_detail);
        ButterKnife.bind(this);
        getSaishiDetail();
    }


    private void getSaishiDetail() {
        showLoading();
        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);

        OkGo.<String>post(API.apiSaishiDetail).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                dismissLoading();
                if (MyTextUtils.isEmpty(response.body())) return;
                saishiDetailPojo = JSON.parseObject(response.body(), SaishiDetailPojo.class);
                initUI(saishiDetailPojo);
            }
        });
    }


    void initUI(SaishiDetailPojo saishiDetailPojo) {
        ArrayList<String> images = new ArrayList<>();

        if (saishiDetailPojo.getRes().getImglistpath() == null) {

        } else {
            String[] split = saishiDetailPojo.getRes().getImglistpath().split(",");
            for (String temp : split) {
                images.add(temp);
            }
        }


        banner.setImages(images).setDelayTime(2000)
                .setBannerAnimation(Transformer.Stack)
                .setImageLoader(new GlideRoundImageLoader()).start();

        tvBarTitle.setText(saishiDetailPojo.getRes().getTitle());
        tvTitle.setText(saishiDetailPojo.getRes().getTitle());
        tvContent.setText(saishiDetailPojo.getRes().getDescs());

        tvbmsz.setText("报名时间：" + saishiDetailPojo.getRes().getReportbegintime() + "-" + saishiDetailPojo.getRes().getReportendtime());
        tvbssj.setText("比赛时间：" + saishiDetailPojo.getRes().getEndtime());
        tvbsdd.setText("比赛地点：" + saishiDetailPojo.getRes().getAddress());

        tvbmfy.setText("报名费用：" + saishiDetailPojo.getRes().getCost());
        tvbmzg.setText("" + saishiDetailPojo.getRes().getQualidesc());
        tvryxe.setText("人员限额：" + saishiDetailPojo.getRes().getNum());
        tvbmzt.setText("报名状态：" + saishiDetailPojo.getRes().getReportstatus());

        tvRemark.setText(saishiDetailPojo.getRes().getRemark());

        if (saishiDetailPojo.getRes().getReportstatus().contains("报名中")) {
            tvSumit.setText("报名中");
        } else {
            tvSumit.setText("已截止");
            tvSumit.setBackground(getResources().getDrawable(R.drawable.btn_click_gray));
            tvSumit.setClickable(false);

        }

    }


    @OnClick({R.id.linBack, R.id.tvSumit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
            case R.id.tvSumit:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                Uri data = Uri.parse("tel:" + saishiDetailPojo.getRes().getPhone() + "");
                intent.setData(data);
                startActivity(intent);
                break;
        }
    }
}