package com.soomax.main.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kongzue.stacklabelview.StackLabel;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.simascaffold.banner.GlideImageLoader;
import com.simascaffold.utils.MapNavigationUtil;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.TeacherDetailPojo;
import com.youth.banner.Banner;
import com.youth.banner.Transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

@Route(path = RoutePath.home_teacher_detail)
public class TeacherDetail extends BaseActivity {

    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.banner)
    Banner banner;


    int pageindex = 1;
    TeacherDetailPojo teacherDetailPojo;
    @Autowired(name = "id")
    String id;
    @BindView(R.id.ivtx)
    CircleImageView ivtx;
    @BindView(R.id.tvName)
    TextView tvName;
//    @BindView(R.id.tvXiangmu)
//    TextView tvXiangmu;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.tvSumit)
    TextView tvSumit;
    @BindView(R.id.ivLocation)
    ImageView ivLocation;
    @BindView(R.id.tvBarTitle)
    TextView tvBarTitle;
    @BindView(R.id.rlTop)
    RelativeLayout rlTop;
    @BindView(R.id.stackLabelView)
    StackLabel stackLabelView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_teacher_detail);
        ButterKnife.bind(this);
        getTeacherDetail();
    }


    private void getTeacherDetail() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        OkGo.<String>post(API.apiHealthTeacherDetail).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                if (MyTextUtils.isEmpty(response.body())) return;
                teacherDetailPojo = JSON.parseObject(response.body(), TeacherDetailPojo.class);
                initUI(teacherDetailPojo);
            }
        });
    }


    void initUI(TeacherDetailPojo teacherDetailPojo) {
        ArrayList<String> images = new ArrayList<>();
        String[] split = teacherDetailPojo.getRes().getImglistpath().split(",");
        for (String temp : split) {
            images.add(temp);
        }

        banner.setImages(images).setDelayTime(2000)
                .setBannerAnimation(Transformer.Stack)
                .setImageLoader(new GlideImageLoader())
                .start();

        RoundedCorners roundedCorners = new RoundedCorners(20);
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                .error(R.drawable.sd_default);//图片加载失败后，显示的图
        Glide.with(TeacherDetail.this).load(teacherDetailPojo.getRes().getImgpath()).apply(options).into(ivtx);

        tvName.setText(teacherDetailPojo.getRes().getName());


//        tvXiangmu.setText(teacherDetailPojo.getRes().getSportname());
        String label = teacherDetailPojo.getRes().getSportname();
        ArrayList<String> labels = new ArrayList<String>();
        if (label.contains(",")) {
            for (String temp : label.split(",")) {
                labels.add(temp);
            }
        } else {
            labels.add(label);
        }
        stackLabelView.setLabels(labels);


        tvLocation.setText(teacherDetailPojo.getRes().getAddress());
        tvDesc.setText(teacherDetailPojo.getRes().getDescs());

        ivLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapNavigationUtil.goNav(TeacherDetail.this, getSupportFragmentManager(), teacherDetailPojo.getRes().getLat(), teacherDetailPojo.getRes().getLng());
            }
        });


    }


    @OnClick({R.id.linBack, R.id.tvSumit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
            case R.id.tvSumit:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                Uri data = Uri.parse("tel:" + teacherDetailPojo.getRes().getPhone() + "");
                intent.setData(data);
                startActivity(intent);
                break;

        }
    }


}