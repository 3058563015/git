package com.soomax.main.my;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kongzue.dialog.listener.OnMenuItemClickListener;
import com.kongzue.dialog.v2.BottomMenu;
import com.kongzue.stacklabelview.StackLabel;
import com.kongzue.stacklabelview.interfaces.OnLabelClickListener;
import com.lsp.RulerView;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.Response;
import com.meetsl.scardview.SCardView;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.Logger;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.LoginPojo;
import com.soomax.pojo.PersonxmPojo;
import com.soomax.pojo.SubmitPicPojo;
import com.yanzhenjie.album.Action;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumFile;

import org.joda.time.DateTime;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

@Route(path = RoutePath.my_usrinfo)
public class PersonalInfo extends BaseActivity {
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.tvwdtx)
    TextView tvwdtx;
    @BindView(R.id.ivtx)
    CircleImageView ivtx;
    @BindView(R.id.svCard)
    SCardView svCard;
    @BindView(R.id.tvjbxx)
    TextView tvjbxx;
    @BindView(R.id.tvnc)
    TextView tvnc;
    @BindView(R.id.rlnc)
    RelativeLayout rlnc;
    @BindView(R.id.tvxb)
    TextView tvxb;
    @BindView(R.id.rlxb)
    RelativeLayout rlxb;
    @BindView(R.id.tvsg)
    TextView tvsg;
    @BindView(R.id.rlsg)
    RelativeLayout rlsg;
    @BindView(R.id.rltz)
    RelativeLayout rltz;
    @BindView(R.id.tvtz)
    TextView tvtz;
    @BindView(R.id.tvsr)
    TextView tvsr;
    @BindView(R.id.rlsr)
    RelativeLayout rlsr;
    @BindView(R.id.rlqm)
    RelativeLayout rlqm;
    @BindView(R.id.tvxq)
    TextView tvxq;
    @BindView(R.id.rlxq)
    RelativeLayout rlxq;
    @BindView(R.id.tvqy)
    TextView tvqy;
    @BindView(R.id.rlqy)
    RelativeLayout rlqy;
    @BindView(R.id.card)
    SCardView card;
    PersonxmPojo personxmPojo;
    String gender, height, weight, bri, headimgid, fav, loc;
    @BindView(R.id.tvSubmit)
    TextView tvSubmit;
    @BindView(R.id.tvqm)
    TextView tvqm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_setting_person);
        ButterKnife.bind(this);
        getxm();
        getUsr();
    }


    @Override
    protected void onResume() {
        super.onResume();
//
    }

    private void getUsr() {
        final Map<String, String> params = new HashMap<String, String>();
        LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
        params.put("username", usr.getPhone());
        params.put("userpassword", usr.getUserpassword());

        OkGo.<String>post(API.apiOauthLogin).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {

                LoginPojo loginPojo = JSON.parseObject(response.body(), LoginPojo.class);
                if (loginPojo.getCode().equals("200")) {
                    RoundedCorners roundedCorners = new RoundedCorners(20);
                    RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                            .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                            .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                            .error(R.drawable.sd_default);//图片加载失败后，显示的图
                    Glide.with(PersonalInfo.this).load(loginPojo.getRes().getHeadimgpath()).apply(options).into(ivtx);

                    tvnc.setText(loginPojo.getRes().getNickname());
                    tvxb.setText(loginPojo.getRes().getGender());
                    tvsg.setText(loginPojo.getRes().getHeigh());
                    tvtz.setText(loginPojo.getRes().getWeight());
                    tvsr.setText(loginPojo.getRes().getBirthday());
                    tvxq.setText(loginPojo.getRes().getUserhobbyname());
                    tvqy.setText(loginPojo.getRes().getRegionname());
                    tvqm.setText(loginPojo.getRes().getSinglemark());


                    LoginPojo.ResBean usr = loginPojo.getRes();
                    Hawk.put("usr", usr);
                    Hawk.put("token", loginPojo.getToken());

                    HttpHeaders headers = new HttpHeaders();
                    headers.put("uid", usr.getId());
                    headers.put("token", loginPojo.getToken());
                    OkGo.getInstance().addCommonHeaders(headers);

                }

            }
        });
    }


    void submitPic(String filePath) {
        showLoading();
        ArrayList<File> files = new ArrayList<File>();
        files.add(new File(filePath));
        OkGo.<String>post(API.apiSubmitPic + "?status=1&title=test&filesize=30m")
                .tag(this)
                .isMultipart(true)       // 强制使用 multipart/form-data 表单上传（只是演示，不需要的话不要设置。默认就是false）
                .addFileParams("file", files) // 这里支持一个key传多个文件
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        dismissLoading();
                        SubmitPicPojo submitPicPojo = JSON.parseObject(response.body(), SubmitPicPojo.class);
                        if (submitPicPojo.getCode().equals("200")) {
                            headimgid = submitPicPojo.getRes().getId();
                        }
                    }
                });
    }


    private void getxm() {
        final Map<String, String> params = new HashMap<String, String>();
        OkGo.<String>post(API.apipersionxm).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                personxmPojo = JSON.parseObject(response.body(), PersonxmPojo.class);
            }
        });
    }


    @OnClick({R.id.rlnc, R.id.ivtx, R.id.rlxb, R.id.rlsg, R.id.rltz, R.id.rlsr, R.id.rlqm, R.id.rlxq, R.id.rlqy, R.id.tvSubmit, R.id.linBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivtx:
                Album.image(this)
//                        .multipleChoice()
                        .singleChoice()
                        .camera(true)
                        .columnCount(3)
                        .onResult(new Action<ArrayList<AlbumFile>>() {
                            @Override
                            public void onAction(@NonNull ArrayList<AlbumFile> result) {
                                Glide.with(PersonalInfo.this).load(result.get(0).getPath()).into(ivtx);
                                submitPic(result.get(0).getThumbPath());
                            }
                        })
                        .onCancel(new Action<String>() {
                            @Override
                            public void onAction(String result) {
                            }
                        })
                        .start();
                break;


            case R.id.rlnc:
                ARouter.getInstance()
                        .build(RoutePath.my_modify_nickname)
                        .withString("nickname", tvnc.getText().toString())
                        .navigation();
                break;
            case R.id.rlxb:
                BottomMenu.show(PersonalInfo.this, new String[]{"不限", "男", "女"}, new OnMenuItemClickListener() {
                    @Override
                    public void onClick(String text, int index) {
                        tvxb.setText(text);
                        gender = text;
                    }
                });
                break;
            case R.id.rlsg:
                View customView = LayoutInflater.from(PersonalInfo.this).inflate(R.layout.sd_custon_sg, null);
                RulerView rulerView = customView.findViewById(R.id.ruler_sg);

                if (!MyTextUtils.isEmpty(tvsg.getText().toString())) {
                    rulerView.computeScrollTo(Float.parseFloat(tvsg.getText().toString().replace("cm", "")));
                }

                rulerView.setOnChooseResulterListener(new RulerView.OnChooseResulterListener() {
                    @Override
                    public void onEndResult(String result) {
                        height = result.toString() + "cm";
                    }

                    @Override
                    public void onScrollResult(String result) {

                    }
                });
                List<String> list = new ArrayList<>();
                list.add("确定");
                list.add("取消");
                BottomMenu.show(PersonalInfo.this, list, new OnMenuItemClickListener() {
                    @Override
                    public void onClick(String text, int index) {
                        if (index == 0) {
                            tvsg.setText(height);
                        } else if (index == 1) {
                            height = null;
                        }

                    }
                }, false).setCustomView(customView);
                break;
            case R.id.rltz:
                View customView2 = LayoutInflater.from(PersonalInfo.this).inflate(R.layout.sd_custon_tz, null);
                RulerView rulerView2 = customView2.findViewById(R.id.ruler_tz);

                if (!MyTextUtils.isEmpty(tvtz.getText().toString())) {
                    rulerView2.computeScrollTo(Float.parseFloat(tvtz.getText().toString().replace("kg", "")));
                }

                rulerView2.setOnChooseResulterListener(new RulerView.OnChooseResulterListener() {
                    @Override
                    public void onEndResult(String result) {
                        weight = result.toString() + "kg";
                    }

                    @Override
                    public void onScrollResult(String result) {

                    }
                });
                List<String> list2 = new ArrayList<>();
                list2.add("确定");
                list2.add("取消");
                BottomMenu.show(PersonalInfo.this, list2, new OnMenuItemClickListener() {
                    @Override
                    public void onClick(String text, int index) {
                        if (index == 0) {
                            tvtz.setText(weight);
                        } else if (index == 1) {
                            weight = null;
                        }

                    }
                }, false).setCustomView(customView2);
                break;
            case R.id.rlsr:
                View customView3 = LayoutInflater.from(PersonalInfo.this).inflate(R.layout.sd_custon_date, null);
                DatePicker datePicker = customView3.findViewById(R.id.date_pick);
                String[] sr = new String[3];
                if (MyTextUtils.isEmpty(tvsr.getText().toString())) {
                    sr = DateTime.now().toString("yyyy-MM-dd").split("-");
                } else {
                    sr = tvsr.getText().toString().split("-");
                }

                datePicker.init(Integer.parseInt(sr[0]), (Integer.parseInt(sr[1]) - 1), Integer.parseInt(sr[2]), new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        bri = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                    }
                });

                List<String> list3 = new ArrayList<>();
                list3.add("确定");
                list3.add("取消");
                BottomMenu.show(PersonalInfo.this, list3, new OnMenuItemClickListener() {
                    @Override
                    public void onClick(String text, int index) {
                        if (index == 0) {
                            tvsr.setText(bri);
                        } else if (index == 1) {
                            bri = null;
                        }
                    }
                }, false).setCustomView(customView3);
                break;
            case R.id.rlqm:
                ARouter.getInstance()
                        .build(RoutePath.my_modify_qm)
                        .withString("singlemark", tvqm.getText().toString())
                        .navigation();
                break;
            case R.id.rlxq:
                View customView4 = LayoutInflater.from(PersonalInfo.this).inflate(R.layout.sd_custon_tip, null);

                StackLabel stackLabel = customView4.findViewById(R.id.stackLabelView);
                ArrayList<String> labels = new ArrayList<String>();
                for (PersonxmPojo.ResBean.HobbyBean temp : personxmPojo.getRes().getHobby()) {
                    labels.add(temp.getName());
                }
                stackLabel.setLabels(labels);
//                stackLabel.setOnLabelClickListener(new OnLabelClickListener() {
//                    @Override
//                    public void onClick(int index, View v, String s) {
//                        if (fav == null) {
//                            fav = index + "";
//                        } else {
//                            fav = fav + "," + index;
//                        }
//                    }
//                });


                List<String> list4 = new ArrayList<>();
                list4.add("确定");
                list4.add("取消");
                BottomMenu.show(PersonalInfo.this, list4, new OnMenuItemClickListener() {
                    @Override
                    public void onClick(String text, int index) {
                        if (index == 0) {

                            StringBuffer buf = new StringBuffer();
                            StringBuffer bufstr = new StringBuffer();
                            List<Integer> arry = stackLabel.getSelectIndexList();

                            for (int i = 0; i < arry.size(); i++) {
                                buf.append(personxmPojo.getRes().getHobby().get(arry.get(i)).getId()).append(",");
                                bufstr.append(personxmPojo.getRes().getHobby().get(arry.get(i)).getName()).append(",");
                            }

                            if (buf.length() > 0) {
                                buf.replace(buf.length() - 1, buf.length(), "");
                                bufstr.replace(bufstr.length() - 1, bufstr.length(), "");
                            }

                            fav = buf.toString();
//                            Logger.e(fav);
                            tvxq.setText(bufstr.toString());
//                            Logger.e(bufstr.toString());

                        } else if (index == 1) {
                            fav = null;
                        }
                    }
                }, false).setCustomView(customView4);
                break;
            case R.id.rlqy:
                View customView5 = LayoutInflater.from(PersonalInfo.this).inflate(R.layout.sd_custon_tip, null);

                StackLabel stackLabel2 = customView5.findViewById(R.id.stackLabelView);
                ArrayList<String> labels2 = new ArrayList<String>();
                for (PersonxmPojo.ResBean.RegionBean temp : personxmPojo.getRes().getRegion()) {
                    labels2.add(temp.getName());
                }
                stackLabel2.setLabels(labels2);
                stackLabel2.setOnLabelClickListener(new OnLabelClickListener() {
                    @Override
                    public void onClick(int index, View v, String s) {
                        loc = index + "";
                    }
                });
                List<String> list5 = new ArrayList<>();
                list5.add("确定");
                list5.add("取消");
                BottomMenu.show(PersonalInfo.this, list5, new OnMenuItemClickListener() {
                    @Override
                    public void onClick(String text, int index) {
                        if (index == 0) {
                            tvqy.setText(personxmPojo.getRes().getRegion().get(Integer.parseInt(loc)).getName());
                            loc = personxmPojo.getRes().getRegion().get(Integer.parseInt(loc)).getId() + "";
                        } else if (index == 1) {
                            loc = null;
                        }
                    }
                }, false).setCustomView(customView5);
                break;
            case R.id.tvSubmit:
                submitUserinfo();
                break;
            case R.id.linBack:
                finish();
                break;

        }
    }


    void submitUserinfo() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("birthday", bri);
        params.put("gender", gender);
        params.put("headimgid", headimgid);
        params.put("heigh", height);
        params.put("weight", weight);
        params.put("hobby", fav);
        params.put("usercity", loc);


        OkGo.<String>post(API.apiSubmitusr).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                SubmitPicPojo submitPicPojo = JSON.parseObject(response.body(), SubmitPicPojo.class);
                if (submitPicPojo.getCode().equals("200")) {
                    finish();
                }
            }
        });
    }


}
