package com.soomax.main.my;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.simascaffold.utils.LightToasty;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.ResultPojo;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.my_modify_qm)
public class ModifyQM extends BaseActivity {
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.etComment)
    EditText etComment;
    @BindView(R.id.tvNum)
    TextView tvNum;

    @Autowired(name = "singlemark")
    String singlemark;
    @BindView(R.id.tvSubmit)
    TextView tvSubmit;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_modify_qm);
        ButterKnife.bind(this);

        etComment.setText(singlemark);

        etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int cd = 140 - s.length();
                tvNum.setText(cd + "/140");
            }
        });


    }

    @OnClick({R.id.linBack, R.id.tvSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
            case R.id.tvSubmit:
                submitUsrInfo();
                break;

        }
    }

    private void submitUsrInfo() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("singlemark", etComment.getText().toString());
        OkGo.<String>post(API.apiSubmitusr).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (resultPojo.getCode().equals("200")) {
                    finish();
                } else {
                    LightToasty.warning(ModifyQM.this, resultPojo.getMsg());
                }

            }
        });
    }


}
