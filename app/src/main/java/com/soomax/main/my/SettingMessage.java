package com.soomax.main.my;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.LoginPojo;
import com.soomax.pojo.ResultPojo;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ch.ielse.view.SwitchView;

@Route(path = RoutePath.my_setting_messgae)
public class SettingMessage extends BaseActivity {
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.switch_dz)
    SwitchView switchDz;
    @BindView(R.id.switch_xt)
    SwitchView switchXt;
    @BindView(R.id.switch_ts)
    SwitchView switchTs;
    String userexcute, sysnotice, article,id;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_setting_message);
        ButterKnife.bind(this);
        LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");

        id=usr.getRoleid();
        if (usr.getUserexcute().equals("1")) {
            switchDz.setOpened(true);
        } else {
            switchDz.setOpened(false);
        }
        if (usr.getSysnotice().equals("1")) {
            switchXt.setOpened(true);
        } else {
            switchXt.setOpened(false);
        }
        if (usr.getArticle().equals("1")) {
            switchTs.setOpened(true);
        } else {
            switchTs.setOpened(false);
        }


        switchDz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchDz.isOpened()) {
                    userexcute = "1";
                } else {
                    userexcute = "0";
                }
                appedituserrole();
            }
        });

        switchXt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchXt.isOpened()) {
                    sysnotice = "1";
                } else {
                    sysnotice = "0";
                }
                appedituserrole();
            }
        });
        switchTs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchTs.isOpened()) {
                    article = "1";
                } else {
                    article = "0";
                }
                appedituserrole();
            }
        });


    }


    private void appedituserrole() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("userexcute", userexcute);
        params.put("sysnotice", sysnotice);
        params.put("article", article);
        params.put("id", id);
        OkGo.<String>post(API.appedituserrole).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                if (MyTextUtils.isEmpty(response.body())) return;
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (resultPojo.getCode().equals("200")) {
                    finish();
                } else {
                    lightToast(resultPojo.getMsg());
                }

            }
        });
    }


    @OnClick(R.id.linBack)
    public void onViewClicked() {
        finish();
    }
}
