package com.soomax.main.my;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.RegisterPojo;
import com.soomax.pojo.YzmPojo;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.modifypwd)

public class ModifyPwd extends BaseActivity {


    @BindView(R.id.linBack)
    LinearLayout linBack;

    @NotEmpty
    @BindView(R.id.etPhone)
    EditText etPhone;

    @NotEmpty
    @BindView(R.id.etyz)
    EditText etyz;

    @BindView(R.id.tvYzm)
    TextView tvYzm;

    @NotEmpty
    @BindView(R.id.etpw)
    EditText etpw;

    @NotEmpty
    @BindView(R.id.etpw2)
    EditText etpw2;

    @BindView(R.id.tvSumit)
    TextView tvSumit;

    String createTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_modifypwd);
        ButterKnife.bind(this);
    }


    void modofy() {
        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("code", etyz.getText().toString() + "");
        params.put("createtime", createTime);
        params.put("phone", etPhone.getText().toString() + "");
        params.put("type", "101");
        params.put("userpassword", MyTextUtils.getMD5String(etpw.getText() + ""));
        OkGo.<String>post(API.apiRetPWD).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                dismissLoading();
                RegisterPojo registerPojo = JSON.parseObject(response.body(), RegisterPojo.class);
                if (registerPojo.getCode().equals("200")) {
                    LightToasty.normal(getApplicationContext(), registerPojo.getMsg());
                    finish();
                } else {
                    LightToasty.warning(getApplicationContext(), registerPojo.getMsg());
                }
            }
        });
    }

    void getYzm() {
        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("phone", etPhone.getText() + "");
        params.put("type", "101");
        OkGo.<String>post(API.apiSmsCheckCodePublicSend).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                dismissLoading();
                YzmPojo registerPojo = JSON.parseObject(response.body(), YzmPojo.class);
                if (registerPojo.getCode().equals("200")) {
                    LightToasty.normal(getApplicationContext(), registerPojo.getMsg());
                    createTime = registerPojo.getRes().getCreatetime();
                } else {
                    LightToasty.warning(getApplicationContext(), registerPojo.getMsg());
                }
            }
        });
    }


    @Override
    public void onValidationSucceeded() {
        super.onValidationSucceeded();
        String PW_PATTERN = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$";
        if (!etpw.getText().toString().matches(PW_PATTERN)) {
            LightToasty.warning(ModifyPwd.this, "请输入6-12位数字和字母的组合");
            return;
        }
        modofy();
    }


    @OnClick({R.id.linBack, R.id.tvSumit, R.id.tvYzm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
            case R.id.tvYzm:
                getYzm();
                break;
            case R.id.tvSumit:
                validator.validate();
                break;
        }
    }


}
