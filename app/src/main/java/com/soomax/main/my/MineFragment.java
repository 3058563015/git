package com.soomax.main.my;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.LoginPojo;
import com.soomax.pojo.ResultPojo;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MineFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.ivtx)
    ImageView ivtx;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.ivSetting)
    ImageView ivSetting;
    @BindView(R.id.rlTop)
    RelativeLayout rlTop;
    @BindView(R.id.rlSMRZ)
    RelativeLayout rlSMRZ;
    @BindView(R.id.rlHelp)
    RelativeLayout rlHelp;
    @BindView(R.id.rlXieyi)
    RelativeLayout rlXieyi;
    @BindView(R.id.rlYinsi)
    RelativeLayout rlYinsi;
    @BindView(R.id.tvSign)
    TextView tvSign;
    @BindView(R.id.tvFlag)
    TextView tvFlag;


    public static MineFragment newInstance(String content) {
        Bundle args = new Bundle();
        MineFragment fragment = new MineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.sd_mine, null, false);
        unbinder = ButterKnife.bind(this, view);
        super.onCreate(savedInstanceState);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void getUsr() {
        if (!Hawk.contains("token") || MyTextUtils.isEmpty(Hawk.get("token"))) {
            tvSign.setVisibility(View.GONE);
            tvName.setText("登录/注册");
            Glide.with(getActivity()).load(R.mipmap.sd_default_tx).into(ivtx);
            tvFlag.setVisibility(View.GONE);
            return;
        }
        final Map<String, String> params = new HashMap<String, String>();
        LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
        tvSign.setVisibility(View.VISIBLE);
        params.put("username", usr.getPhone());
        params.put("userpassword", usr.getUserpassword());

        OkGo.<String>post(API.apiOauthLogin).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {

                LoginPojo loginPojo = JSON.parseObject(response.body(), LoginPojo.class);
                if (loginPojo.getCode().equals("200")) {
                    RoundedCorners roundedCorners = new RoundedCorners(20);
                    RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                            .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                            .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                            .error(R.drawable.sd_default);//图片加载失败后，显示的图
                    Glide.with(getActivity()).load(loginPojo.getRes().getHeadimgpath()).apply(options).into(ivtx);
                    tvName.setText(loginPojo.getRes().getNickname());
                    if (loginPojo.getRes().getIssigned() == 1) {
                        tvSign.setText("已签到");
                    } else {
                        tvSign.setText("签到");
                    }


                    if (loginPojo.getRes().getIsrealauth() == null) {
                        tvFlag.setVisibility(View.GONE);
                    } else if (loginPojo.getRes().getIsrealauth().equals("1")) {
                        tvFlag.setVisibility(View.VISIBLE);
                    }


                    LoginPojo.ResBean usr = loginPojo.getRes();
                    Hawk.put("usr", usr);
                    Hawk.put("token", loginPojo.getToken());

                    HttpHeaders headers = new HttpHeaders();
                    headers.put("uid", usr.getId());
                    headers.put("token", loginPojo.getToken());
                    OkGo.getInstance().addCommonHeaders(headers);


                }

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getUsr();
    }

    @OnClick({R.id.ivtx, R.id.tvName, R.id.ivSetting, R.id.rlSMRZ, R.id.rlHelp, R.id.rlXieyi, R.id.rlYinsi})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivtx:
            case R.id.tvName:

                if (!Hawk.contains("token") || MyTextUtils.isEmpty(Hawk.get("token"))) {
                    ARouter.getInstance().build(RoutePath.login).navigation();
                } else {
                    ARouter.getInstance().build(RoutePath.my_usrinfo).navigation();
                }

                break;
            case R.id.ivSetting:
                if (!Hawk.contains("token") || MyTextUtils.isEmpty(Hawk.get("token"))) {
                    LightToasty.normal(getActivity(), "请先登录!");
                } else {
                    ARouter.getInstance().build(RoutePath.my_setting).navigation();
                }


                break;
            case R.id.rlSMRZ:
                if (!Hawk.contains("token") || MyTextUtils.isEmpty(Hawk.get("token"))) {
                    LightToasty.normal(getActivity(), "请先登录!");
                } else {
                    ARouter.getInstance().build(RoutePath.my_authentication)
                            .navigation();
                }


                break;
            case R.id.rlHelp:
                ARouter.getInstance().build(RoutePath.my_modify_fk)
                        .navigation();
                break;
            case R.id.rlXieyi:
                ARouter.getInstance().build(RoutePath.h5)
                        .withString("suffix", "http://103.233.6.43:8009/XDfileserver/xieyi.html")
                        .withString("title", "用户协议")
                        .navigation();
                break;
            case R.id.rlYinsi:
                ARouter.getInstance().build(RoutePath.h5)
                        .withString("suffix", "http://103.233.6.43:8009/XDfileserver/tiaokuan.html")
                        .withString("title", "隐私条款")
                        .navigation();

                break;
        }
    }

    @OnClick(R.id.tvSign)
    public void onViewClicked() {
        sign();
    }

    void sign() {
        final Map<String, String> params = new HashMap<String, String>();


        OkGo.<String>post(API.apisign).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (resultPojo.getCode().equals("200")) {
                    tvSign.setText("已签到");
                    tvSign.setClickable(false);
                }
                LightToasty.info(getActivity(), resultPojo.getMsg());

            }
        });
    }


}
