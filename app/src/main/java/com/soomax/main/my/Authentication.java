package com.soomax.main.my;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.baidu.ocr.sdk.OCR;
import com.baidu.ocr.sdk.exception.OCRError;
import com.baidu.ocr.sdk.model.IDCardParams;
import com.baidu.ocr.sdk.model.IDCardResult;
import com.baidu.ocr.ui.camera.CameraActivity;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.main.ocr.APIService;
import com.soomax.main.ocr.FaceDetectExpActivity;
import com.soomax.main.ocr.FileUtil;
import com.soomax.main.ocr.OnResultListener;
import com.soomax.main.ocr.exception.FaceException;
import com.soomax.main.ocr.model.LivenessVsIdcardResult;
import com.soomax.main.ocr.utils.ImageSaveUtil;
import com.soomax.pojo.LoginPojo;
import com.soomax.pojo.SubmitPicPojo;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.my_authentication)
public class Authentication extends BaseActivity {
    @BindView(R.id.rlTop)
    RelativeLayout rlTop;
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.ivzm)
    ImageView ivzm;
    @BindView(R.id.ivfm)
    ImageView ivfm;
    @BindView(R.id.ivtx)
    ImageView ivtx;
    @BindView(R.id.tvSumit)
    TextView tvSumit;
    private static final int REQUEST_CODE_CAMERA = 102;
    private static final int REQUEST_TRACK_FACE = 1;  // 实时采集
    String username, idnumber;

    String path_zm, path_fm;
    String faceimgid;
    String authimgupid;
    String authimgdown;
    String isrealauth;
    String authrealgender;
    String authrealnation;
    String authrealaddress;
    String authrealname;
    String authcode;

    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_authentication);
        ButterKnife.bind(this);
        path_zm = FileUtil.getSaveFile(getApplication()).getAbsolutePath();
        path_fm = FileUtil.getSaveFile2(getApplication()).getAbsolutePath();
        tvSumit.setClickable(false);
        tvSumit.setBackground(getResources().getDrawable(R.drawable.btn_click_gray));
        getusr();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String contentType = data.getStringExtra(CameraActivity.KEY_CONTENT_TYPE);

                if (!MyTextUtils.isEmpty(contentType)) {
                    if (CameraActivity.CONTENT_TYPE_ID_CARD_FRONT.equals(contentType)) {

                        RequestOptions options = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
                        Glide.with(this)
                                .load(path_zm)
                                .apply(options)
                                .into(ivzm);

                        Bitmap bmp = ImageSaveUtil.loadBitmapFromPath(getApplication(), path_zm);
                        ivzm.setImageBitmap(bmp);
                        recIDCard(IDCardParams.ID_CARD_SIDE_FRONT, path_zm);


                    } else if (CameraActivity.CONTENT_TYPE_ID_CARD_BACK.equals(contentType)) {

                        RequestOptions options = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
                        Glide.with(this)
                                .load(path_fm)
                                .apply(options)
                                .into(ivfm);
                        Bitmap bmp = ImageSaveUtil.loadBitmapFromPath(getApplication(), path_fm);
                        ivfm.setImageBitmap(bmp);


                        recIDCard(IDCardParams.ID_CARD_SIDE_BACK, path_fm);

                    }
                }
            }
        } else if (requestCode == REQUEST_TRACK_FACE) {
            if (resultCode == RESULT_OK) {

//                File file = new File(ImageSaveUtil.loadCameraTempPath(getApplication(), FaceDetectExpActivity.BEST_IMG));
//                Glide.with(this).load(file).into(ivtx);

                Bitmap bmp = ImageSaveUtil.loadCameraBitmap(this, FaceDetectExpActivity.BEST_IMG);
                ivtx.setScaleType(ImageView.ScaleType.FIT_XY);
                ivtx.setImageBitmap(bmp);
                tvSumit.setBackground(getResources().getDrawable(R.drawable.btn_click));
                tvSumit.setClickable(true);


            }
        }


    }

    private void recIDCard(String idCardSide, String filePath) {
        IDCardParams param = new IDCardParams();
        param.setImageFile(new File(filePath));
        param.setIdCardSide(idCardSide);
        param.setDetectDirection(true);
        param.setImageQuality(20);

        OCR.getInstance().recognizeIDCard(param, new com.baidu.ocr.sdk.OnResultListener<IDCardResult>() {
            @Override
            public void onResult(IDCardResult result) {
                if (result != null) {
                    if (param.getIdCardSide().equals(IDCardParams.ID_CARD_SIDE_FRONT)) {
                        if (MyTextUtils.isEmpty(result.getName().getWords())) {
                            submitPic(path_fm, 1);
                            return;
                        }
                        username = result.getName().getWords();
                        idnumber = result.getIdNumber().getWords();
                        authrealgender = result.getGender().getWords();
                        authrealnation = result.getEthnic().getWords();
                        authrealaddress = result.getAddress().getWords();
                        authrealname = result.getName().getWords();
                        authcode = result.getIdNumber().getWords();
                        submitPic(path_zm, 0);
                    } else if (param.getIdCardSide().equals(IDCardParams.ID_CARD_SIDE_BACK)) {
                        if (MyTextUtils.isEmpty(result.getSignDate().getWords())) {
                            lightToast("请按要求拍摄或上传正确的证件");
                            return;
                        }
                        submitPic(path_fm, 1);
                    }
                }
            }

            @Override
            public void onError(OCRError error) {
                LightToasty.warning(getApplication(), error.getMessage());
            }
        });
    }

    @OnClick({R.id.linBack, R.id.ivzm, R.id.ivfm, R.id.ivtx, R.id.tvSumit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
            case R.id.ivzm:
                Intent intent = new Intent(Authentication.this, CameraActivity.class);
                intent.putExtra(CameraActivity.KEY_OUTPUT_FILE_PATH, path_zm);
                intent.putExtra(CameraActivity.KEY_CONTENT_TYPE, CameraActivity.CONTENT_TYPE_ID_CARD_FRONT);
                startActivityForResult(intent, REQUEST_CODE_CAMERA);

                break;
            case R.id.ivfm:
                Intent intent2 = new Intent(Authentication.this, CameraActivity.class);
                intent2.putExtra(CameraActivity.KEY_OUTPUT_FILE_PATH, path_fm);
                intent2.putExtra(CameraActivity.KEY_CONTENT_TYPE, CameraActivity.CONTENT_TYPE_ID_CARD_BACK);
                startActivityForResult(intent2, REQUEST_CODE_CAMERA);
                break;
            case R.id.ivtx:
                Intent itTrack = new Intent(Authentication.this, FaceDetectExpActivity.class);
                startActivityForResult(itTrack, REQUEST_TRACK_FACE);
                break;
            case R.id.tvSumit:
                policeVerify(ImageSaveUtil.loadCameraTempPath(getApplication(), FaceDetectExpActivity.BEST_IMG));
                break;
        }
    }


    private void policeVerify(String filePath) {
//        if (TextUtils.isEmpty(filePath) || waitAccesstoken) {
//            return;
//        }
        File file = new File(filePath);
        if (!file.exists()) {
            return;
        }

//        displayTip(resultTipTV, "公安身份核实中...");
        APIService.getInstance().policeVerify(username, idnumber, filePath, new
                        OnResultListener<LivenessVsIdcardResult>() {


                            @Override
                            public void onResult(LivenessVsIdcardResult result) {
                                if (result != null && result.getScore() >= 80) {
//                            delete();
//                            displayTip(resultTipTV, "核身成功");
//                            displayTip(scoreTV, "公安验证分数：" + result.getScore());
                                    lightToast("核身成功");
                                    submitPic(filePath, 2);


                                } else {
//                            displayTip(resultTipTV, "核身失败");
//                            displayTip(scoreTV, "公安验证分数过低：" + result.getScore());
//                            retBtn.setVisibility(View.VISIBLE);
                                    lightToast("公安验证分数过低");
                                }
                            }

                            @Override
                            public void onError(FaceException error) {
//                                delete();
//                                lightToast("3");
//                                submitPic(filePath);

                                LightToasty.warning(getApplicationContext(), "公安身份核实失败:" + error.getMessage() + "重新录入人脸信息");

//                                // 如返回错误码为：216600，则核身失败，提示信息为：身份证号码错误
//                                // 如返回错误码为：216601，则核身失败，提示信息为：身份证号码与姓名不匹配
//                                Toast.makeText(FaceOnlineVerifyActivity.this,
//                                        "公安身份核实失败:" + error.getMessage(), Toast.LENGTH_SHORT)
//                                        .show();
//                                retBtn.setVisibility(View.VISIBLE);

                            }
                        }
        );
    }


    void submitPic(String filePath, int flag) {
        showLoading();
        ArrayList<File> files = new ArrayList<File>();
        files.add(new File(filePath));
        OkGo.<String>post(API.apiSubmitPic + "?status=1&title=test&filesize=30m&filetype=100")
                .tag(this)
                .isMultipart(true)       // 强制使用 multipart/form-data 表单上传（只是演示，不需要的话不要设置。默认就是false）
                .addFileParams("file", files) // 这里支持一个key传多个文件
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        dismissLoading();
                        SubmitPicPojo submitPicPojo = JSON.parseObject(response.body(), SubmitPicPojo.class);
                        if (submitPicPojo.getCode().equals("200")) {
                            switch (flag) {
                                case 0:
                                    authimgupid = submitPicPojo.getRes().getId();
                                    break;
                                case 1:
                                    authimgdown = submitPicPojo.getRes().getId();
                                    break;
                                case 2:
                                    faceimgid = submitPicPojo.getRes().getId();
                                    submit();
                                    break;
                            }
                        } else {
                            lightToast(submitPicPojo.getMsg());
                        }
                    }
                });
    }

    private void submit() {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("faceimgid", faceimgid);
        params.put("isrealauth", "1");
        params.put("authimgupid", authimgupid);
        params.put("authimgdown", authimgdown);
        params.put("authrealgender", authrealgender);
        params.put("authrealnation", authrealnation);
        params.put("authrealaddress", authrealaddress);
        params.put("authrealname", authrealname);
        params.put("authcode", authcode);


        OkGo.<String>post(API.apiSubmitusr).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                SubmitPicPojo submitPicPojo = JSON.parseObject(response.body(), SubmitPicPojo.class);
                LightToasty.info(getApplicationContext(), submitPicPojo.getMsg());
                if (submitPicPojo.getCode().equals("200")) {
//                    login();
                    finish();
                }
            }
        });
    }


    void getusr() {
        LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", usr.getUsername());
        params.put("userpassword", usr.getUserpassword());
        OkGo.<String>post(API.apiOauthLogin).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                LoginPojo loginPojo = JSON.parseObject(response.body(), LoginPojo.class);
                if (loginPojo.getCode().equals("500")) {
                    LightToasty.warning(getApplicationContext(), loginPojo.getMsg());
                } else if (loginPojo.getCode().equals("200")) {
                    LoginPojo.ResBean usr = loginPojo.getRes();
                    Hawk.put("usr", usr);
                    Hawk.put("token", loginPojo.getToken());

                    HttpHeaders headers = new HttpHeaders();
                    headers.put("uid", usr.getId());
                    headers.put("token", loginPojo.getToken());
                    OkGo.getInstance().addCommonHeaders(headers);

                    if (loginPojo.getRes().getAuthrealname() != null) {
                        username = loginPojo.getRes().getAuthrealname();
                        idnumber = loginPojo.getRes().getAuthcode();
                    }
                    Glide.with(Authentication.this).load(loginPojo.getRes().getAuthimguppath()).into(ivzm);
                    Glide.with(Authentication.this).load(loginPojo.getRes().getAuthimgdownpath()).into(ivfm);
                    Glide.with(Authentication.this).load(loginPojo.getRes().getFaceimgpath()).into(ivtx);
                }
            }
        });
    }


}
