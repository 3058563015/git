package com.soomax.main.my;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.orhanobut.logger.Logger;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.ResultPojo;
import com.soomax.pojo.SubmitPicPojo;
import com.yanzhenjie.album.Action;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhouzhuo.zzimagebox.ZzImageBox;

@Route(path = RoutePath.my_modify_fk)
public class Fankui extends BaseActivity {

    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.etComment)
    EditText etComment;

    @BindView(R.id.etMail)
    EditText etMail;
    @BindView(R.id.tvSumit)
    TextView tvSumit;
    @BindView(R.id.zz_image_box)
    ZzImageBox imageBox;

    int i = 0;
    int size;
    ArrayList<String> list_path = new ArrayList<>();
    ArrayList<String> list_id = new ArrayList<>();
    String imagelist;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_fankui);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ButterKnife.bind(this);


        //如果需要加载网络图片，添加此监听。
        imageBox.setOnlineImageLoader(new ZzImageBox.OnlineImageLoader() {
            @Override
            public void onLoadImage(ImageView iv, String url) {
                Glide.with(Fankui.this).load(url).into(iv);
            }
        });
        imageBox.setOnImageClickListener(new ZzImageBox.OnImageClickListener() {
            @Override
            public void onImageClick(int position, String filePath, String tag, int type, ImageView iv) {
//                Toast.makeText(Fankui.this, "你点击了+" + position + "的图片:url=" + filePath + ", tag=" + tag, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeleteClick(int position, String filePath, String tag, int type) {
                imageBox.removeImage(position);
                list_id.remove(position);
            }

            @Override
            public void onAddClick() {
                Album.image(Fankui.this)
                        .multipleChoice()
                        .camera(true)
                        .columnCount(3)
                        .onResult(new Action<ArrayList<AlbumFile>>() {
                            @Override
                            public void onAction(@NonNull ArrayList<AlbumFile> result) {
                                size = result.size();
                                for (AlbumFile temp : result) {
                                    submitPic(temp.getThumbPath());
                                }
                            }
                        })
                        .onCancel(new Action<String>() {
                            @Override
                            public void onAction(String result) {
                            }
                        })
                        .start();


            }
        });
    }

    @OnClick({R.id.linBack, R.id.tvSumit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
            case R.id.tvSumit:
                submit();
                break;
        }
    }


    void submitPic(String filePath) {
        showLoading();
        ArrayList<File> files = new ArrayList<File>();
        files.add(new File(filePath));
        OkGo.<String>post(API.apiSubmitPic + "?status=1&title=test&filesize=30m")
                .tag(this)
                .isMultipart(true)// 强制使用 multipart/form-data 表单上传（只是演示，不需要的话不要设置。默认就是false）
                .addFileParams("file", files) // 这里支持一个key传多个文件
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        dismissLoading();
                        SubmitPicPojo submitPicPojo = JSON.parseObject(response.body(), SubmitPicPojo.class);
                        if (submitPicPojo.getCode().equals("200")) {
                            list_path.add(submitPicPojo.getRes().getFilepath());
                            list_id.add(submitPicPojo.getRes().getId());
                            i++;
                            if (i == size) {
                                for (String path : list_path) {
                                    imageBox.addImageOnline(path);
                                }
                            }
                        }
                    }
                });
    }

    public static boolean orEmail(String email) {
        if (email == null || "".equals(email)) return false;
        String regex = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
        return email.matches(regex);
    }

    private void submit() {
        if (MyTextUtils.isEmpty(etComment.getText().toString())) {
            lightToast("评论内容不能为空");
            return;
        }
        if (MyTextUtils.isEmpty(etMail.getText().toString())) {
            lightToast("邮箱不能为空");
            return;
        }
        if (!orEmail(etMail.getText().toString())) {
            lightToast("请输入正确的邮箱地址");
            return;
        }


        StringBuffer buf = new StringBuffer();
        for (String temp : list_id) {
            buf.append(temp).append(",");
        }
        if (buf.length() > 0) {
            buf.replace(buf.length() - 1, buf.length(), "");
        }

        Logger.e(buf.toString());


        final Map<String, String> params = new HashMap<String, String>();
        params.put("content", etComment.getText().toString());
        params.put("email", etMail.getText().toString());
        if (!MyTextUtils.isEmpty(buf.toString())) {
            params.put("imglist", buf.toString());
        }

        showLoading();
        OkGo.<String>post(API.appadduserfeedback).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                dismissLoading();
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (resultPojo.getCode().equals("200")) {
                    finish();
                } else {
                    lightToast(resultPojo.getMsg());
                }
            }
        });
    }


}
