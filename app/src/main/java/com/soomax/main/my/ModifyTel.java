package com.soomax.main.my;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.Response;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.orhanobut.hawk.Hawk;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.LoginPojo;
import com.soomax.pojo.RegisterPojo;
import com.soomax.pojo.ResultPojo;
import com.soomax.pojo.YzmPojo;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.modifytel)

public class ModifyTel extends BaseActivity {
    @BindView(R.id.linBack)
    LinearLayout linBack;
    String createTime;
    @BindView(R.id.rlTop)
    RelativeLayout rlTop;

    @NotEmpty
    @BindView(R.id.etPhone)
    EditText etPhone;

    @NotEmpty
    @BindView(R.id.etyz)
    EditText etyz;


    @BindView(R.id.tvYzm)
    TextView tvYzm;

    @BindView(R.id.tvSumit)
    TextView tvSumit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_modifytel);
        ButterKnife.bind(this);
        LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
        etPhone.setText("当前手机密码: " + usr.getPhone());

    }


    void step1() {
        LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("code", etyz.getText().toString() + "");
        params.put("createtime", createTime);
        params.put("phone", usr.getPhone() + "");

        OkGo.<String>post(API.appedituserphonestep1).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                dismissLoading();
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (resultPojo.getCode().equals("200")) {
                    etPhone.setText("");
                    etPhone.setEnabled(true);
                    etPhone.setHint("请输入新的手机号");

                    etyz.setText("");

                    tvSumit.setText("确定");

                }


            }
        });
    }


    void step2() {
        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("code", etyz.getText().toString() + "");
        params.put("createtime", createTime);
        params.put("phone", etPhone.getText().toString() + "");

        OkGo.<String>post(API.appedituserphonestep2).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                dismissLoading();
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (resultPojo.getCode().equals("200")) {
                    lightToast(resultPojo.getMsg());
                    LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
                    usr.setUsername(etPhone.getText().toString());
                    Hawk.put("usr", usr);
                    getusr();
                }
            }
        });
    }


    void getusr() {
        LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", usr.getUsername());
        params.put("userpassword", usr.getUserpassword());
        OkGo.<String>post(API.apiOauthLogin).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                LoginPojo loginPojo = JSON.parseObject(response.body(), LoginPojo.class);
                if (loginPojo.getCode().equals("500")) {
                    LightToasty.warning(getApplicationContext(), loginPojo.getMsg());
                } else if (loginPojo.getCode().equals("200")) {
                    LoginPojo.ResBean usr = loginPojo.getRes();
                    Hawk.put("usr", usr);
                    Hawk.put("token", loginPojo.getToken());

                    HttpHeaders headers = new HttpHeaders();
                    headers.put("uid", usr.getId());
                    headers.put("token", loginPojo.getToken());
                    OkGo.getInstance().addCommonHeaders(headers);
                    finish();
                }
            }
        });
    }


    void getYzm() {
        showLoading();
        LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
        Map<String, String> params = new HashMap<String, String>();
        if (tvSumit.getText().equals("下一步")) {
            params.put("phone", usr.getPhone());
        } else {
            params.put("phone", etPhone.getText().toString());
        }
        params.put("type", "101");
        OkGo.<String>post(API.apiSmsCheckCodePublicSend).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                dismissLoading();
                YzmPojo registerPojo = JSON.parseObject(response.body(), YzmPojo.class);
                if (registerPojo.getCode().equals("200")) {
                    LightToasty.normal(getApplicationContext(), registerPojo.getMsg());
                    createTime = registerPojo.getRes().getCreatetime();
                } else {
                    LightToasty.warning(getApplicationContext(), registerPojo.getMsg());
                }
            }
        });
    }


    @Override
    public void onValidationSucceeded() {
        super.onValidationSucceeded();
//        String PW_PATTERN = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$";

        if (tvSumit.getText().equals("下一步")) {
            step1();
        } else {

//            if (!etPhone.getText().toString().matches(PW_PATTERN)) {
//                LightToasty.warning(ModifyTel.this, "请输入6-12位数字和字母的组合");
//                return;
//            }
            step2();
        }
    }


    @OnClick({R.id.linBack, R.id.tvYzm, R.id.tvSumit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linBack:
                finish();
                break;
            case R.id.tvYzm:
                getYzm();
                break;
            case R.id.tvSumit:
                validator.validate();
                break;
        }
    }
}
