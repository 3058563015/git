package com.soomax.main.my;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kongzue.dialog.v2.SelectDialog;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.Response;
import com.meetsl.scardview.SCardView;
import com.orhanobut.hawk.Hawk;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.LoginPojo;
import com.soomax.pojo.ResultPojo;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

@Route(path = RoutePath.my_setting)
public class Setting extends BaseActivity {
    @BindView(R.id.linBack)
    LinearLayout linBack;
    @BindView(R.id.ivtx)
    CircleImageView ivtx;
    @BindView(R.id.rlgrzl)
    RelativeLayout rlgrzl;
    @BindView(R.id.rlqxgl)
    RelativeLayout rlqxgl;
    @BindView(R.id.rlxxtx)
    RelativeLayout rlxxtx;
    @BindView(R.id.rlqchc)
    RelativeLayout rlqchc;
    @BindView(R.id.rlxgmm)
    RelativeLayout rlxgmm;
    @BindView(R.id.rlxgsjh)
    RelativeLayout rlxgsjh;
    @BindView(R.id.card)
    SCardView card;
    @BindView(R.id.tvSumit)
    TextView tvSumit;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_setting);
        ButterKnife.bind(this);

    }


    @Override
    protected void onResume() {
        super.onResume();
        getUsr();
    }

    private void getUsr() {
        final Map<String, String> params = new HashMap<String, String>();
        LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
        params.put("username", usr.getPhone());
        params.put("userpassword", usr.getUserpassword());

        OkGo.<String>post(API.apiOauthLogin).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {

                LoginPojo loginPojo = JSON.parseObject(response.body(), LoginPojo.class);
                if (loginPojo.getCode().equals("200")) {
                    RoundedCorners roundedCorners = new RoundedCorners(20);
                    RequestOptions options = RequestOptions.bitmapTransform(roundedCorners).override(300, 300)
                            .placeholder(R.drawable.sd_default)//图片加载出来前，显示的图片
                            .fallback(R.drawable.sd_default) //url为空的时候,显示的图片
                            .error(R.drawable.sd_default);//图片加载失败后，显示的图
                    Glide.with(Setting.this).load(loginPojo.getRes().getHeadimgpath()).apply(options).into(ivtx);


                    LoginPojo.ResBean usr = loginPojo.getRes();
                    Hawk.put("usr", usr);
                    Hawk.put("token", loginPojo.getToken());

                    HttpHeaders headers = new HttpHeaders();
                    headers.put("uid", usr.getId());
                    headers.put("token", loginPojo.getToken());
                    OkGo.getInstance().addCommonHeaders(headers);

                }

            }
        });
    }

    @OnClick({R.id.rlgrzl, R.id.rlxxtx, R.id.rlqchc, R.id.rlxgmm, R.id.rlxgsjh, R.id.linBack, R.id.tvSumit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlgrzl:
                ARouter.getInstance().build(RoutePath.my_usrinfo).navigation();
                break;

            case R.id.rlxxtx:
                ARouter.getInstance().build(RoutePath.my_setting_messgae).navigation();
                break;
            case R.id.rlqchc:
                break;
            case R.id.rlxgmm:
                ARouter.getInstance().build(RoutePath.modifypwd).navigation();
                break;
            case R.id.rlxgsjh:
                ARouter.getInstance().build(RoutePath.modifytel).navigation();
                break;
            case R.id.linBack:
                finish();
                break;
            case R.id.tvSumit:
                SelectDialog.show(Setting.this, "提示", "是否确定退出", "确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        lgout();
                    }
                }, "取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                break;
        }
    }


    private void lgout() {
        final Map<String, String> params = new HashMap<String, String>();

        OkGo.<String>post(API.applogout).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                if (MyTextUtils.isEmpty(response.body())) return;
                ResultPojo resultPojo = JSON.parseObject(response.body(), ResultPojo.class);
                if (resultPojo.getCode().equals("200")) {

                    Hawk.put("usr", null);
                    Hawk.put("token", "");

                    HttpHeaders headers = new HttpHeaders();
                    headers.put("uid", "");
                    headers.put("token", "");
                    OkGo.getInstance().addCommonHeaders(headers);
                    finish();


                } else {
                    lightToast(resultPojo.getMsg());
                }

            }
        });
    }


}
