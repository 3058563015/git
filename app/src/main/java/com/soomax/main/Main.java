package com.soomax.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.jaeger.library.StatusBarUtil;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.Logger;
import com.simascaffold.utils.LightToasty;
import com.soomax.base.BaseActivity;
import com.simascaffold.component.bottomNav.SpecialTab;
import com.simascaffold.component.bottomNav.SpecialTabRound;
import com.soomax.common.BDLocationUtil;

import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.item.BaseTabItem;


@Route(path = "/sd_login/home")
public class Main extends BaseActivity {
    static int currentIndex;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_home);

        PageNavigationView tab = findViewById(R.id.tab);
        NavigationController navigationController = tab.custom()
                .addItem(newItem(R.mipmap.sd_tab_home_n, R.mipmap.sd_tab_home_p, "首页"))
                .addItem(newItem(R.mipmap.sd_tab_find_n, R.mipmap.sd_tab_find_p, "发现"))
//                .addItem(newItem(R.mipmap.sd_tab_chat_n, R.mipmap.sd_tab_chat_p, "社交"))
                .addItem(newItem(R.mipmap.sd_tab_my_n, R.mipmap.sd_tab_my_p, "我的"))
                .build();


        ViewPager viewPager = findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(new MainViewPagerAdapter(getSupportFragmentManager(), 3));
        navigationController.setupWithViewPager(viewPager);
        StatusBarUtil.setTranslucent(Main.this);
        BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
            @Override
            public void result(BDLocation location) {
                int errorCode = location.getLocType();
//                lightToast(errorCode + "");
                if ((location.getLatitude() + "").equals("4.9E-324")) {
                    Hawk.put("lat", "39.011042");
                    Hawk.put("lng", "117.719731");
                } else {
                    Hawk.put("lat", location.getLatitude() + "");
                    Hawk.put("lng", location.getLongitude() + "");
                }
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    /**
     * 正常tab
     */
    private BaseTabItem newItem(int drawable, int checkedDrawable, String text) {
        SpecialTab mainTab = new SpecialTab(this);
        mainTab.initialize(drawable, checkedDrawable, text);
        mainTab.setTextDefaultColor(0xFF888888);
        mainTab.setTextCheckedColor(getResources().getColor(R.color.colorPrimary));
        return mainTab;
    }

    /**
     * 圆形tab
     */
    private BaseTabItem newRoundItem(int drawable, int checkedDrawable, String text) {
        SpecialTabRound mainTab = new SpecialTabRound(this);
        mainTab.initialize(drawable, checkedDrawable, text);
        mainTab.setTextDefaultColor(0xFF888888);
        mainTab.setTextCheckedColor(0xFF009688);
        return mainTab;
    }


    private long exitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
//                Toast.makeText(getApplicationContext(), "再按一下返回到桌面",
//                        Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                Intent i = new Intent(Intent.ACTION_MAIN);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addCategory(Intent.CATEGORY_HOME);
                startActivity(i);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


}
