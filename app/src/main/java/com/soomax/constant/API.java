package com.soomax.constant;

public class API {
    public static String baseUrl = "http://103.233.6.43:8001/";


    //登录-登录
    public static String apiOauthLogin = baseUrl + "applogin.rest";
    public static String applogout = baseUrl + "applogout.rest";

    //登录-发送短信验证码
    public static String apiSmsCheckCodePublicSend = baseUrl + "appgetvalidatecode.rest";
    //登录-注册
    public static String apiRegister = baseUrl + "appregister.rest";
    //登录-修改密码
    public static String apiRetPWD = baseUrl + "appeditpassword.rest";


    //首页
    public static String apiHomeInfo = baseUrl + "appmainpageinfo.rest";
    //首页选择地址
    public static String appgetregionlist = baseUrl + "appgetregionlist.rest";

    //首页-健康管理—运动课堂
    public static String apiHealthSport = baseUrl + "getappsportroomlist.rest";
    //首页-健康管理—运动课堂-视频详情
    public static String apiHealthSportvideo = baseUrl + "getappsportroominfo.rest";
    //首页-健康管理—运动课堂-整体点赞
    public static String addappsportroomlike = baseUrl + "addappsportroomlike.rest";
    //首页-健康管理—运动课堂-列表点赞
    public static String addappsportroomcommentlike = baseUrl + "addappsportroomcommentlike.rest";
    //首页-健康管理—运动课堂-评价
    public static String addappsportroomcommnet = baseUrl + "addappsportroomcommnet.rest";

    //首页-健康管理—知名教练
    public static String apiHealthTeacher = baseUrl + "getappsportteacherlist.rest";
    //首页-健康管理—知名教练-详情
    public static String apiHealthTeacherDetail = baseUrl + "getappsportteacherinfo.rest";
    //首页-通知
    public static String apiNotice = baseUrl + "appgetnoticelist.rest";
    //首页-通知数量
    public static String apiNoticeNum = baseUrl + "appgetnoticenum.rest";
    //首页-我的通知-已读
    public static String apiNoticeRead = baseUrl + "appeditnotice.rest";


    //首页-场馆预订
    public static String apiStadiums = baseUrl + "appgetstadiumlist.rest";
    //首页-场馆预订-场馆详情
    public static String apiStadiumsDetail = baseUrl + "appgetstadiuminfo.rest";
    //首页-场馆预订-场馆详情-评价
    public static String addappstadiumcommnet = baseUrl + "addappstadiumcommnet.rest";

    //首页-场馆预订-场馆项目列表
    public static String apiSportClass = baseUrl + "getsportclass.rest";


    //首页-官方通知
    public static String apiMessage = baseUrl + "appgetofficialnotice.rest";
    //首页-赛事活动
    public static String apiSaishi = baseUrl + "appgetmatchList.rest";
    //首页-赛事活动-赛事项目列表
    public static String apiSaishiClass = baseUrl + "getappmatchclass.rest";
    //首页-赛事活动-赛事详情
    public static String apiSaishiDetail = baseUrl + "appgetmatchinfo.rest";
    //首页-学校场地
    public static String apiSchool = baseUrl + "getappschoollist.rest";
    //首页-学校场地-详情
    public static String apiSchoolDetail = baseUrl + "getappschoolinfo.rest";
    //首页-学校场地-详情-评价列表
    public static String apiSchoolComments = baseUrl + "getappschoolcommentlist.rest";
    //首页-学校场地-详情-申请
    public static String addappschoolapply = baseUrl + "addappschoolapply.rest";
    //首页-学校场地-详情-评价
    public static String addappschoolcommnet = baseUrl + "addappschoolcommnet.rest";
    //首页-学校场地-详情-小手点赞
    public static String addappschoolcommentlike = baseUrl + "addappschoolcommentlike.rest";
    //首页-学校场地-承诺
    public static String appedituserschoolpromise = baseUrl + "appedituserschoolpromise.rest";


    //发现
    public static String apiFinder = baseUrl + "getappnewslist.rest";
    //发现
    public static String apiSubmitPic = "http://103.233.6.43:8002/" + "uploadfiless.rest";
    //发送图片-发送id
    public static String apiSubmitusr = baseUrl + "appedituserbascinfo.rest";

    //个人页面-项目
    public static String apipersionxm = baseUrl + "appgethobbyandregion.rest";
    //个人页面-签到
    public static String apisign = baseUrl + "appusersign.rest";
    //消息与提醒
    public static String appedituserrole = baseUrl + "appedituserrole.rest";
    //修改手机号第一步
    public static String appedituserphonestep1 = baseUrl + "appedituserphonestep1.rest";
    //修改手机号第二步
    public static String appedituserphonestep2 = baseUrl + "appedituserphonestep2.rest";
    //反馈与帮助
    public static String appadduserfeedback = baseUrl + "appadduserfeedback.rest";
}
