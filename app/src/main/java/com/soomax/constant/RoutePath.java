package com.soomax.constant;

public class RoutePath {

    final public static String home = "/sd_login/home";
    final public static String login = "/sd_login/sd_login";
    final public static String register = "/sd_login/register";
    final public static String reset = "/sd_login/reset";
    final public static String modifypwd = "/sd_login/modifypwd";
    final public static String modifytel = "/sd_login/modifytel";
    final public static String register_sg = "/sd_login/register/sg";
    final public static String home_health = "/home/health";
    final public static String home_stadiums = "/home/stadiums";
    final public static String home_stadiums_detail = "/home/stadiums/detail";
    final public static String home_stadiums_search = "/home/stadiums/search";
    final public static String home_video_detail = "/home/video/detail";
    final public static String home_teacher_detail = "/home/teacher/detail";
    final public static String home_saishi = "/home/saishi";
    final public static String home_saishi_detail = "/home/saishi/detail";
    final public static String home_school = "/home/school";
    final public static String home_school_search = "/home/school/search";
    final public static String home_school_detail = "/home/school/detail";
    final public static String h5 = "/home/h5";
    final public static String comment = "/home/comment";
    final public static String sd_notice = "/home/notice";
    final public static String home_message = "/home/message";
    final public static String home_chooselocation = "/home/chooselocation";

    final public static String my_setting = "/my/setting";
    final public static String my_setting_messgae = "/my/setting/message";
    final public static String my_authentication = "/my/authentication";
    final public static String my_usrinfo = "/my/persioninfo";
    final public static String my_modify_nickname = "/my/modifynickname";
    final public static String my_modify_qm = "/my/modifyqm";
    final public static String my_modify_fk = "/my/fl";

}