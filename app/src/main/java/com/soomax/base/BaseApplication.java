package com.soomax.base;

import android.os.Build;
import android.os.StrictMode;
import android.view.Gravity;

import com.alibaba.android.arouter.launcher.ARouter;
import com.baidu.idl.face.platform.FaceConfig;
import com.baidu.idl.face.platform.FaceEnvironment;
import com.baidu.idl.face.platform.FaceSDKManager;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.ocr.sdk.OCR;
import com.baidu.ocr.sdk.exception.OCRError;
import com.kongzue.dialog.v2.DialogSettings;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cookie.CookieJarImpl;
import com.lzy.okgo.cookie.store.SPCookieStore;
import com.lzy.okgo.interceptor.HttpLoggingInterceptor;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.Logger;
import com.simascaffold.base.AppApplication;
import com.simascaffold.utils.GDLocationUtil;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.common.BDLocationUtil;
import com.soomax.main.ocr.APIService;
import com.soomax.main.ocr.Config;
import com.soomax.main.ocr.OnResultListener;
import com.soomax.main.ocr.exception.FaceException;
import com.soomax.main.ocr.model.AccessToken;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import cn.jpush.android.api.JPushInterface;
import okhttp3.OkHttpClient;

import com.lzy.okgo.model.HttpHeaders;
import com.soomax.pojo.LoginPojo;

public class BaseApplication extends AppApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        //是否开启打印日志
//        KLog.init(true);
//        initCrash();
        SDKInitializer.initialize(getApplicationContext());
        GDLocationUtil.init(this);
        BDLocationUtil.init(this);

//        ARouter.openLog();     // 打印日志
        ARouter.openDebug();   // 开启调试模式(如果在InstantRun模式下运行，必须开启调试模式！线上版本需要关闭,否则有安全风险)
        ARouter.init(BaseApplication.this); // 尽可能早，推荐在Application中初始化

        Hawk.init(getApplicationContext()).build();
        DialogSettings.style = DialogSettings.STYLE_IOS;
        DialogSettings.tip_theme = DialogSettings.THEME_LIGHT;
        DialogSettings.use_blur = false;

        initPush();
        initUpdate();
        initOCRSDK();
        initAccessToken();
        initLib();
        setFaceConfig();
        initOkgo();

    }

    private void initOkgo() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        //配置 log
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor("OkGo");
        //log打印级别，决定了log显示的详细程度
        loggingInterceptor.setPrintLevel(HttpLoggingInterceptor.Level.BODY);
        //log颜色级别，决定了log在控制台显示的颜色
        loggingInterceptor.setColorLevel(Level.INFO);
        builder.addInterceptor(loggingInterceptor);


        //全局的读取超时时间
        builder.readTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);
        //全局的写入超时时间
        builder.writeTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);
        //全局的连接超时时间
        builder.connectTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);

        //cookie保存策略
        builder.cookieJar(new CookieJarImpl(new SPCookieStore(this)));

        HttpHeaders headers = new HttpHeaders();

        if (Hawk.contains("token") && !MyTextUtils.isEmpty(Hawk.get("token"))) {
            LoginPojo.ResBean usr = (LoginPojo.ResBean) Hawk.get("usr");
            headers.put("uid", usr.getId());
            headers.put("token", Hawk.get("token"));
        }

        OkGo.getInstance().addCommonHeaders(headers);

        OkGo.getInstance().init(this)                       //必须调用初始化
                .setOkHttpClient(builder.build())
                //建议设置OkHttpClient，不设置将使用默认的
                .setRetryCount(3);   //全局统一超时重连次数，默认为三次，那么最差的情况会请求4次(一次原始请求，三次重连请求)

        // ，不需要可以设置为0
//                .addCommonParams(params);                       //全局公共参数
    }


    private void initOCRSDK() {
        OCR.getInstance().initAccessToken(new com.baidu.ocr.sdk.OnResultListener<com.baidu.ocr.sdk.model.AccessToken>() {
            @Override
            public void onResult(com.baidu.ocr.sdk.model.AccessToken accessToken) {
                Logger.e(accessToken.getAccessToken());

            }

            @Override
            public void onError(OCRError ocrError) {


            }
        }, getApplicationContext());
    }


    private void initUpdate() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.detectFileUriExposure();
        }
    }


    private void initAccessToken() {
        APIService.getInstance().init(this);
        APIService.getInstance().initAccessTokenWithAkSk(new OnResultListener<AccessToken>() {


            @Override
            public void onResult(AccessToken result) {
//                Log.i("wtf", "AccessToken->" + result.getAccessToken());
//                Logger.e(result.getAccessToken() + "笑脸");
            }

            @Override
            public void onError(FaceException error) {
//                Logger.e("xx", "AccessTokenError:" + error);
//                error.printStackTrace();

            }
        }, Config.apiKey, Config.secretKey);

    }

    /**
     * 初始化SDK
     */
    private void initLib() {
        // 为了android和ios 区分授权，appId=appname_face_android ,其中appname为申请sdk时的应用名
        // 应用上下文
        // 申请License取得的APPID
        // assets目录下License文件名
        FaceSDKManager.getInstance().initialize(this, Config.licenseID, Config.licenseFileName);
         setFaceConfig();
    }

    private void setFaceConfig() {
        FaceConfig config = FaceSDKManager.getInstance().getFaceConfig();
        config.setMinFaceSize(60);//设置最小检测人脸阈值
        config.setCropFaceValue(1200);//设置截取人脸图片大小
        config.setOcclusionValue(0.5f);//设置人脸遮挡阈值
        config.setBrightnessValue(40);//设置亮度阈值
        config.setBlurnessValue(0.4f);//设置图像模糊阈值
        config.setHeadPitchValue(5);//设置头部姿态角度
        config.setHeadRollValue(10);
        config.setHeadYawValue(10);
        config.setNotFaceValue(0.6f);//设置人脸检测精度阈值
        config.setCheckFaceQuality(true);//设置人脸检测进度阈值
        config.setFaceDecodeNumberOfThreads(2);
        FaceSDKManager.getInstance().setFaceConfig(config);
    }


    private void initCrash() {
//        CaocConfig.Builder.create()
//                .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT) //背景模式,开启沉浸式
//                .enabled(true) //是否启动全局异常捕获
//                .showErrorDetails(true) //是否显示错误详细信息
//                .showRestartButton(true) //是否显示重启按钮
//                .trackActivities(true) //是否跟踪Activity
//                .minTimeBetweenCrashesMs(2000) //崩溃的间隔时间(毫秒)
//                .errorDrawable(R.mipmap.logo) //错误图标
//                .restartActivity(Splash.class) //重新启动后的activity
////                .errorActivity(YourCustomErrorActivity.class) //崩溃后的错误activity
////                .eventListener(new YourCustomEventListener()) //崩溃后的错误监听
//                .apply();
    }


    private void initPush() {
        JPushInterface.setDebugMode(true);//正式版的时候设置false，关闭调试
        JPushInterface.init(this);

        Set<String> set = new HashSet<>();
        set.add("nhj");//名字任意，可多添加几个
        JPushInterface.setTags(this, set, null);//设置标签
    }

}
