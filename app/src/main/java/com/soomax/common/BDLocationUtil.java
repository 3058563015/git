package com.soomax.common;

import android.app.Notification;
import android.content.Context;
import android.os.Build;

import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;

public class BDLocationUtil {


    public static LocationClient mLocationClient = null;
    public static LocationClientOption option;
    public static BDLocation bdLocation = null;
    private Notification notification;

    public static void init(Context context) {
        mLocationClient = new LocationClient(context);
        //声明LocationClient类
        //注册监听函数

        option = new LocationClientOption();

        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);

//        option.setCoorType("gcj02");
//
//        option.setScanSpan(1000);

//        option.setOpenGps(true);

        mLocationClient.setLocOption(option);


    }


    public static void getLocation(MyLocationListener listener) {
        if (bdLocation == null) {
            getCurrentLocation(listener);
        } else {
            listener.result(bdLocation);
        }
    }


    public static void getCurrentLocation(final MyLocationListener listener) {
        if (mLocationClient == null) {
            return;
        }
        mLocationClient.registerLocationListener(new BDAbstractLocationListener() {
            @Override
            public void onReceiveLocation(BDLocation sbdLocation) {
                bdLocation = sbdLocation;
                listener.result(sbdLocation);
                mLocationClient.stop();
            }
        });
        mLocationClient.start();
    }


    public interface MyLocationListener {
        public void result(BDLocation location);
    }


}
