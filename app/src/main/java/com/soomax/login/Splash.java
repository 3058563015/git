package com.soomax.login;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.bhxdty.soomax.R;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.Logger;
import com.soomax.base.BaseActivity;
import com.soomax.common.BDLocationUtil;
import com.soomax.constant.RoutePath;

import butterknife.ButterKnife;

public class Splash extends BaseActivity {


    private static final String uploadUrl = "http://xby.sharecar" +
            ".cn/xby/version/current/queryByAppId?appId=c2a3ce0b-8ef8-453c-bde3" +
            "-02ada95a3b63";

//    private static final String uploadUrl="/public/v1/applicationRelease/read/latest?applicationId=";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.sd_login_splash);
        ButterKnife.bind(this);
//        BDLocationUtil.getLocation(new BDLocationUtil.MyLocationListener() {
//            @Override
//            public void result(BDLocation location) {
//                Hawk.put("lat", location.getLatitude() + "");
//                Hawk.put("lng", location.getLongitude() + "");
//
//                Logger.e(JSON.toJSONString(location));
////                Hawk.put("lat", "39.011042");
////                Hawk.put("lng", "117.719731");
//            }
//        });
        Hawk.put("lat", "39.011042");
        Hawk.put("lng", "117.719731");
        Hawk.put("address", "滨海新区");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //执行在主线程
                //启动主页面
                ARouter.getInstance().build(RoutePath.home).navigation();
                //关闭当前页面
                finish();
            }
        }, 2000);


    }

    @Override
    protected void onResume() {
        super.onResume();

//        ARouter.getInstance().build(RoutePath.home).navigation();

//        new UpdateManager.UpdateManagerBuilder()
//                .setUrl(uploadUrl)
//                .setContext(Splash.this)
//                .setLinster(new GoLinster() {
//                    @Override
//                    public void go() {
//                        ARouter.getInstance().build(RoutePath.home).navigation();
////                        if (Hawk.contains("token")) {
////                            ARouter.getInstance().build(RoutePath.home).navigation();
////                        } else {
////                            ARouter.getInstance().build(RoutePath.sd_login).navigation();
////                        }
//
//
//                    }
//                })
//                .build()
//                .checkUpdate();
    }


}
