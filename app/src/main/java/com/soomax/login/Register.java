package com.soomax.login;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.blankj.utilcode.util.ActivityUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.Response;
import com.orhanobut.hawk.Hawk;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.main.Main;
import com.soomax.pojo.LoginPojo;
import com.soomax.pojo.RegisterPojo;
import com.soomax.pojo.YzmPojo;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = RoutePath.register)

public class Register extends BaseActivity {


    @BindView(R.id.tvback)
    TextView tvback;
    @BindView(R.id.etPhone)
    EditText etPhone;
    @BindView(R.id.etyz)
    EditText etyz;
    @BindView(R.id.tvYzm)
    TextView tvYzm;
    @BindView(R.id.etpw)
    EditText etpw;
    @BindView(R.id.tvSumit)
    TextView tvSumit;
    @BindView(R.id.xieyi01)
    TextView xieyi01;
    @BindView(R.id.xieyi02)
    TextView xieyi02;

    String createTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_register);
        ButterKnife.bind(this);
    }


    void register() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("code", etyz.getText().toString() + "");
        params.put("createtime", createTime);
        params.put("phone", etPhone.getText().toString() + "");
        params.put("userpassword", MyTextUtils.getMD5String(etpw.getText() + ""));


        OkGo.<String>post(API.apiRegister).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {

                RegisterPojo registerPojo = JSON.parseObject(response.body(), RegisterPojo.class);
                if (registerPojo.getCode().equals("200")) {
                    login(registerPojo.getRes().getPhone(), registerPojo.getRes().getUserpassword());
                } else {
                    LightToasty.warning(getApplicationContext(), registerPojo.getMsg());
                }


            }
        });
    }

    void getYzm() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("phone", etPhone.getText() + "");
        OkGo.<String>post(API.apiSmsCheckCodePublicSend).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                YzmPojo registerPojo = JSON.parseObject(response.body(), YzmPojo.class);
                if (registerPojo.getCode().equals("200")) {
                    LightToasty.normal(getApplicationContext(), registerPojo.getMsg());
                    createTime = registerPojo.getRes().getCreatetime();
                } else {
                    LightToasty.warning(getApplicationContext(), registerPojo.getMsg());
                }
            }
        });
    }

    void login(String name, String pwd) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", name);
        params.put("userpassword", pwd);
        OkGo.<String>post(API.apiOauthLogin).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                LoginPojo loginPojo = JSON.parseObject(response.body(), LoginPojo.class);
                if (loginPojo.getCode().equals("500")) {
                    LightToasty.warning(getApplicationContext(), loginPojo.getMsg());
                } else if (loginPojo.getCode().equals("200")) {
                    LoginPojo.ResBean usr = loginPojo.getRes();
                    Hawk.put("usr", usr);
                    Hawk.put("token", loginPojo.getToken());

                    HttpHeaders headers = new HttpHeaders();
                    headers.put("uid", usr.getId());
                    headers.put("token", loginPojo.getToken());
                    OkGo.getInstance().addCommonHeaders(headers);

                    ActivityUtils.finishToActivity(Main.class, false, true);
                }
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        super.onValidationSucceeded();
    }


    @OnClick({R.id.tvback, R.id.tvSumit, R.id.xieyi01, R.id.xieyi02, R.id.tvYzm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvback:
                finish();
                break;
            case R.id.tvYzm:
                getYzm();
                break;
            case R.id.tvSumit:
                register();
                break;
            case R.id.xieyi01:
                ARouter.getInstance().build(RoutePath.h5)
                        .withString("suffix", "http://103.233.6.43:8009/XDfileserver/xieyi.html")
                        .withString("title", "用户协议")
                        .navigation();
                break;
            case R.id.xieyi02:
                ARouter.getInstance().build(RoutePath.h5)
                        .withString("suffix", "http://103.233.6.43:8009/XDfileserver/tiaokuan.html")
                        .withString("title", "隐私条款")
                        .navigation();
                break;
        }
    }
}
