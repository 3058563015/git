package com.soomax.login;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.bhxdty.soomax.R;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.Response;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.orhanobut.hawk.Hawk;
import com.simascaffold.utils.CommonUtil;
import com.simascaffold.utils.LightToasty;
import com.simascaffold.utils.MyTextUtils;
import com.soomax.base.BaseActivity;
import com.soomax.constant.API;
import com.soomax.constant.RoutePath;
import com.soomax.pojo.LoginPojo;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = "/sd_login/sd_login")

public class Login extends BaseActivity {

    @NotEmpty
    @BindView(R.id.etPhone)
    EditText etPhone;
    @NotEmpty
    @BindView(R.id.etYzm)
    EditText etYzm;
    @BindView(R.id.tvSumit)
    TextView tvSumit;
    @BindView(R.id.tvRegister)
    TextView tvRegister;
    @BindView(R.id.tv_forget)
    TextView tvForget;
    @BindView(R.id.tvback)
    TextView tvback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_login);
        ButterKnife.bind(this);
//        15176526258 a123456
    }


    void login() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", etPhone.getText() + "");
        params.put("userpassword", MyTextUtils.getMD5String(etYzm.getText() + ""));
        OkGo.<String>post(API.apiOauthLogin).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                LoginPojo loginPojo = JSON.parseObject(response.body(), LoginPojo.class);
                if (loginPojo.getCode().equals("500")) {
                    LightToasty.warning(getApplicationContext(), loginPojo.getMsg());
                } else if (loginPojo.getCode().equals("200")) {
                    LoginPojo.ResBean usr = loginPojo.getRes();
                    Hawk.put("usr", usr);
                    Hawk.put("token", loginPojo.getToken());

                    HttpHeaders headers = new HttpHeaders();
                    headers.put("uid", usr.getId());
                    headers.put("token", loginPojo.getToken());
                    OkGo.getInstance().addCommonHeaders(headers);
                    LightToasty.normal(getApplicationContext(),loginPojo.getMsg());
                    finish();
                }
            }
        });
    }

    void getYzm(String verifyToken, String verifyData) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("telephone", etPhone.getText() + "");
        params.put("verifyToken", verifyToken);
        params.put("verifyData", verifyData);
        OkGo.<String>post(API.apiSmsCheckCodePublicSend).tag(this).params(params).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                HashMap<String, String> map = CommonUtil.json2map(response.body());
                if (response.body().contains("httpCode")) {
                    lightToast("验证码发送成功");
                }

            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        super.onValidationSucceeded();
        login();
    }


    @OnClick({R.id.tvSumit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSumit:
                validator.validate();
                break;
        }
    }


    @OnClick({R.id.tvRegister, R.id.tv_forget, R.id.tvback})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvRegister:
                ARouter.getInstance().build(RoutePath.register).navigation();
                break;
            case R.id.tv_forget:
                ARouter.getInstance().build(RoutePath.reset).navigation();
                break;
            case R.id.tvback:
                finish();
                break;
        }
    }
}
