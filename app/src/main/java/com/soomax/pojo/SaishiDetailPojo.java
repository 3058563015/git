package com.soomax.pojo;

public class SaishiDetailPojo {


    /**
     * msg : 操作成功
     * res : {"num":"不限","reportbegintime":"2019-10-24","remark":"1. 成功报名本次比赛后因个人原因不能按时参加比赛的，按自动弃权处理，报名费不予退还。主办方不提供退赛通道。\n2. 报名通道关闭后，如需转让名额，请在11月4日下午17:00之前由转让方与我方联系。私自转让名额的，一经发现，主办方有权取消该选手参赛资格与成绩。\n3.为杜绝替跑、代跑的情况，请参加比赛的家长，携带儿童有效证件（例如：身份证，护照）以备查验！也希望所有参赛家长遵守比赛规则，给小朋友们营造一个公平的竞赛氛围。","starttime":"2019-11-24 08:00:00","title":"2019武清区儿童平衡车大赛","content":"","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","qualidesc":"2岁至5周岁少儿","managername":"老师","imglistpath":"http://103.233.6.43:8009/XDfileserver/201910272001252715224ea4bfe5323ad8bceb9f690d8de3.jpg,http://103.233.6.43:8009/XDfileserver/20191027200131495a6add9a59c0ffa3bd9a2461dfdae7f3.jpg,http://103.233.6.43:8009/XDfileserver/20191027200128618cde14fb65c634311381251c5da21cc5.jpg","id":"89be9d00-2e6c-4e85-a776-2500334618da","lat":"39.42057","createtime":"2019-10-27 20:08:26 919","address":"天津市武清区新创路","cost":"详情电话咨询","lng":"117.014368","imgid":"78855393-fc6a-4eb8-a49d-db1b5631311b","endtime":"2019-11-24 08:00:00","reportstatus":"报名中","reportendtime":"2019-10-30","imglist":"493004ec-43a3-4451-bcd4-4c4193b902fe,b2d9e1f9-1426-4d96-be67-db6ebb99affa,4ffdc50f-cee9-4591-abd9-8332efddb14c","descs":"主办单位：天津市武清区体育局\n协办单位：中体场馆运营管理(天津）有限公司\n承办单位：乐菲骑行（天津）体育文化传播有限公司\n报名时间：10月24日-10月30日\n活动时间：11月24日（星期日）\n          上午：2岁组/3岁组比赛\n          下午：4岁组/5岁组/OPEN组比赛\n（此时间仅供参考，具体时刻以报名结束后公布的《赛程与分组》为准，请您报名后时刻关注官方公众号和赛事群信息）\n赛事地点：天津市武清区体育中心A座","classid":"8","phone":"13820562636","reportstatuscode":1,"isdelete":"0","status":"1"}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * num : 不限
         * reportbegintime : 2019-10-24
         * remark : 1. 成功报名本次比赛后因个人原因不能按时参加比赛的，按自动弃权处理，报名费不予退还。主办方不提供退赛通道。
         2. 报名通道关闭后，如需转让名额，请在11月4日下午17:00之前由转让方与我方联系。私自转让名额的，一经发现，主办方有权取消该选手参赛资格与成绩。
         3.为杜绝替跑、代跑的情况，请参加比赛的家长，携带儿童有效证件（例如：身份证，护照）以备查验！也希望所有参赛家长遵守比赛规则，给小朋友们营造一个公平的竞赛氛围。
         * starttime : 2019-11-24 08:00:00
         * title : 2019武清区儿童平衡车大赛
         * content :
         * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9e
         * qualidesc : 2岁至5周岁少儿
         * managername : 老师
         * imglistpath : http://103.233.6.43:8009/XDfileserver/201910272001252715224ea4bfe5323ad8bceb9f690d8de3.jpg,http://103.233.6.43:8009/XDfileserver/20191027200131495a6add9a59c0ffa3bd9a2461dfdae7f3.jpg,http://103.233.6.43:8009/XDfileserver/20191027200128618cde14fb65c634311381251c5da21cc5.jpg
         * id : 89be9d00-2e6c-4e85-a776-2500334618da
         * lat : 39.42057
         * createtime : 2019-10-27 20:08:26 919
         * address : 天津市武清区新创路
         * cost : 详情电话咨询
         * lng : 117.014368
         * imgid : 78855393-fc6a-4eb8-a49d-db1b5631311b
         * endtime : 2019-11-24 08:00:00
         * reportstatus : 报名中
         * reportendtime : 2019-10-30
         * imglist : 493004ec-43a3-4451-bcd4-4c4193b902fe,b2d9e1f9-1426-4d96-be67-db6ebb99affa,4ffdc50f-cee9-4591-abd9-8332efddb14c
         * descs : 主办单位：天津市武清区体育局
         协办单位：中体场馆运营管理(天津）有限公司
         承办单位：乐菲骑行（天津）体育文化传播有限公司
         报名时间：10月24日-10月30日
         活动时间：11月24日（星期日）
         上午：2岁组/3岁组比赛
         下午：4岁组/5岁组/OPEN组比赛
         （此时间仅供参考，具体时刻以报名结束后公布的《赛程与分组》为准，请您报名后时刻关注官方公众号和赛事群信息）
         赛事地点：天津市武清区体育中心A座
         * classid : 8
         * phone : 13820562636
         * reportstatuscode : 1
         * isdelete : 0
         * status : 1
         */

        private String num;
        private String reportbegintime;
        private String remark;
        private String starttime;
        private String title;
        private String content;
        private String uid;
        private String qualidesc;
        private String managername;
        private String imglistpath;
        private String id;
        private String lat;
        private String createtime;
        private String address;
        private String cost;
        private String lng;
        private String imgid;
        private String endtime;
        private String reportstatus;
        private String reportendtime;
        private String imglist;
        private String descs;
        private String classid;
        private String phone;
        private int reportstatuscode;
        private String isdelete;
        private String status;

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getReportbegintime() {
            return reportbegintime;
        }

        public void setReportbegintime(String reportbegintime) {
            this.reportbegintime = reportbegintime;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getQualidesc() {
            return qualidesc;
        }

        public void setQualidesc(String qualidesc) {
            this.qualidesc = qualidesc;
        }

        public String getManagername() {
            return managername;
        }

        public void setManagername(String managername) {
            this.managername = managername;
        }

        public String getImglistpath() {
            return imglistpath;
        }

        public void setImglistpath(String imglistpath) {
            this.imglistpath = imglistpath;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getImgid() {
            return imgid;
        }

        public void setImgid(String imgid) {
            this.imgid = imgid;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getReportstatus() {
            return reportstatus;
        }

        public void setReportstatus(String reportstatus) {
            this.reportstatus = reportstatus;
        }

        public String getReportendtime() {
            return reportendtime;
        }

        public void setReportendtime(String reportendtime) {
            this.reportendtime = reportendtime;
        }

        public String getImglist() {
            return imglist;
        }

        public void setImglist(String imglist) {
            this.imglist = imglist;
        }

        public String getDescs() {
            return descs;
        }

        public void setDescs(String descs) {
            this.descs = descs;
        }

        public String getClassid() {
            return classid;
        }

        public void setClassid(String classid) {
            this.classid = classid;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public int getReportstatuscode() {
            return reportstatuscode;
        }

        public void setReportstatuscode(int reportstatuscode) {
            this.reportstatuscode = reportstatuscode;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
