package com.soomax.pojo;

public class SubmitPicPojo {


    /**
     * msg : 500
     * res : {"createtime":"2019-11-01 12:36:10 372","filepath":"http://103.233.6.43:8009/XDfileserver/20191101123610369373440465.jpg","filelocalpath":"E:/xd-active-file/20191101123610369373440465.jpg","filesize":"30m","id":"7ec5adbb-4abc-4b5e-b99b-a81276da9c96","isdelete":0,"title":"test","status":"1"}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * createtime : 2019-11-01 12:36:10 372
         * filepath : http://103.233.6.43:8009/XDfileserver/20191101123610369373440465.jpg
         * filelocalpath : E:/xd-active-file/20191101123610369373440465.jpg
         * filesize : 30m
         * id : 7ec5adbb-4abc-4b5e-b99b-a81276da9c96
         * isdelete : 0
         * title : test
         * status : 1
         */

        private String createtime;
        private String filepath;
        private String filelocalpath;
        private String filesize;
        private String id;
        private int isdelete;
        private String title;
        private String status;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getFilepath() {
            return filepath;
        }

        public void setFilepath(String filepath) {
            this.filepath = filepath;
        }

        public String getFilelocalpath() {
            return filelocalpath;
        }

        public void setFilelocalpath(String filelocalpath) {
            this.filelocalpath = filelocalpath;
        }

        public String getFilesize() {
            return filesize;
        }

        public void setFilesize(String filesize) {
            this.filesize = filesize;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(int isdelete) {
            this.isdelete = isdelete;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
