package com.soomax.pojo;

import java.util.List;

public class SchoolDetailPojo {


    /**
     * msg : 操作成功
     * res : {"usersportsorts":[{"headimgid":"2cf86b06-1164-4b44-8ea9-64b46777d117","test":1111,"sporttime":"1分钟","nickname":"这是昵称阿轲","sortnum":10,"headimgpath":"http://103.233.6.43:8009/XDfileserver/20191107160151326214856783.png","username":"15725040722"}],"stadiumnotice":[{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","createtime":"2019-11-05 15:44:20 048","noticedesc":"学校场地公告","publishtime":" 2019-11-05 03:44:11","id":"e5f90ed3-3e6c-48e6-ac04-84549858d51a","isdelete":"0","title":"学校场地公告","stadiumid":"75ef2c8e-d453-4651-8413-43a4b54e5fda","content":"1.严格按照规定的开放时间进入规定场地活动；严禁进入开放场所以外的其他区域（包括教学区域，寄宿区域等）；不满12周岁未成年人须在监护人带领下进入校园进行体育锻炼。\n2.倡导文明健身，遵守社会公德，保持环境整洁；爱护公共设施，如有损坏应照价赔偿。\n3.十大严禁：严禁翻越围墙、栏杆进入场内；严禁携带宠物进入场内；严禁穿高跟鞋进入场内；严禁在开放场所吸烟；严禁场内乱扔果皮纸屑等杂物；严禁私自移动场内各项设施；严禁在场内私自布置各项设施；严禁健身人员将车辆（自行车、电动车、摩托车、汽车等）驶入校园；严禁酗酒者进入校园进行体育锻炼；严禁在学校开放场地内进行足球、广场舞、轮滑等不安全运动项目。\n4.请根据自己的身体状况选择科学的锻炼方式，高血压、心脏病、心血管疾病及其他疾病的患者应避免剧烈运动，以免受伤，否则后果自行承担。\n5.在体育健身过程中健身人员要保管好个人财物，并做好自身伤害安全保护，不做危险动作，不滋事斗殴。\n6.本校对入校健身人员的人身安全、财产安全不承担任何经济赔偿及法律责任。\n7.具体场地开放时间以学校公告为主。","status":"1"}],"userpromise":0,"sportsortlist":[{"sporttime":"1分钟","nickname":"185***042","sortnum":1},{"sporttime":"1分钟","nickname":"183***025","sortnum":2},{"sporttime":"1分钟","nickname":"137***764","sortnum":3},{"sporttime":"1分钟","nickname":"的的的的的的额额额额额额额额额额","sortnum":4},{"headimgid":"f421d945-dc99-4b23-9159-56903de03c59","sporttime":"1分钟","nickname":"滨海健康","sortnum":5,"headimgpath":"http://103.233.6.43:8009/XDfileserver/20191102074946116300079497.png"},{"sporttime":"1分钟","nickname":"138***931","sortnum":6},{"sporttime":"1分钟","nickname":"139***479","sortnum":7},{"headimgid":"50562aff-6768-4c81-b0a8-064ec29e56e0","sporttime":"1分钟","nickname":"兜里有颗糖","sortnum":8,"headimgpath":"http://103.233.6.43:8009/XDfileserver/2019102721145318020191027091452.png"},{"headimgid":"4e92b9a9-cfff-4d2d-9de8-234b7c5bd4c3","sporttime":"1分钟","nickname":"17688888888","sortnum":9,"headimgpath":"http://103.233.6.43:8009/XDfileserver/20191030114400602255296555.png"},{"headimgid":"2cf86b06-1164-4b44-8ea9-64b46777d117","sporttime":"1分钟","nickname":"这是昵称阿轲","sortnum":10,"headimgpath":"http://103.233.6.43:8009/XDfileserver/20191107160151326214856783.png"},{"sporttime":"1分钟","nickname":"151***948","sortnum":11},{"headimgid":"124e8a3f-f84a-4d99-a7a4-f06bff63d2fe","sporttime":"1分钟","nickname":"159***016","sortnum":12,"headimgpath":"http://103.233.6.43:8009/XDfileserver/20191101085558426210860976.png"},{"sporttime":"1分钟","nickname":"151***094","sortnum":13},{"headimgid":"0ea88c96-5abe-4d67-8b0d-1f2f901f8c87","sporttime":"1分钟","nickname":"克里斯蒂","sortnum":14,"headimgpath":"http://103.233.6.43:8009/XDfileserver/2019102408335568320191024083353.png"}],"schoolinfo":{"vacationDateMorningStart":"","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","popularity":257,"managername":"wxy","id":"75ef2c8e-d453-4651-8413-43a4b54e5fda","workDateAfternoonStart":"","periphery":"泰成公园、中国银行、超市、美食、停车场.","lat":"38.962043","connectcode":"022-65828995","createtime":"2019-10-28 11:33:07 128","address":"天津市滨海新区银河二路316号","lng":"117.710243","imgid":"026c91ed-0348-455a-980e-985de0e44b64,42a1456e-028a-4790-b251-28ef22c914d5,b5edff72-cd8f-4441-a18e-48cae592a4ab","workdatemorningend":"","stadiumdesc":"位于天津市滨海新区银河二路316号，滨海新区塘沽草场街小学中部新城校区。","workdate":"","imgpath":"http://103.233.6.43:8009/XDfileserver/20191028113132083tangguzhongbu.jpg,http://103.233.6.43:8009/XDfileserver/20191028113148280tangguzhongbu1.jpg,http://103.233.6.43:8009/XDfileserver/2019102811320944583560217a5f0f3b990be02684bd7529.jpg","workDateAfternoonEnd":"","busroute":"822路、973路","vacationDateAfternoonEnd":"","peoplenumber":0,"vacationDateMorningEnd":"","vacationdate":"","name":"塘沽中部新城学校","applystatus":1,"vacationDateAfternoonStart":"","isdelete":"0","stadiumfaceid":"6ebb5271-2a4b-4d94-bf14-6afec252cbc5","workdatemorningstart":"","status":"1"}}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * usersportsorts : [{"headimgid":"2cf86b06-1164-4b44-8ea9-64b46777d117","test":1111,"sporttime":"1分钟","nickname":"这是昵称阿轲","sortnum":10,"headimgpath":"http://103.233.6.43:8009/XDfileserver/20191107160151326214856783.png","username":"15725040722"}]
         * stadiumnotice : [{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","createtime":"2019-11-05 15:44:20 048","noticedesc":"学校场地公告","publishtime":" 2019-11-05 03:44:11","id":"e5f90ed3-3e6c-48e6-ac04-84549858d51a","isdelete":"0","title":"学校场地公告","stadiumid":"75ef2c8e-d453-4651-8413-43a4b54e5fda","content":"1.严格按照规定的开放时间进入规定场地活动；严禁进入开放场所以外的其他区域（包括教学区域，寄宿区域等）；不满12周岁未成年人须在监护人带领下进入校园进行体育锻炼。\n2.倡导文明健身，遵守社会公德，保持环境整洁；爱护公共设施，如有损坏应照价赔偿。\n3.十大严禁：严禁翻越围墙、栏杆进入场内；严禁携带宠物进入场内；严禁穿高跟鞋进入场内；严禁在开放场所吸烟；严禁场内乱扔果皮纸屑等杂物；严禁私自移动场内各项设施；严禁在场内私自布置各项设施；严禁健身人员将车辆（自行车、电动车、摩托车、汽车等）驶入校园；严禁酗酒者进入校园进行体育锻炼；严禁在学校开放场地内进行足球、广场舞、轮滑等不安全运动项目。\n4.请根据自己的身体状况选择科学的锻炼方式，高血压、心脏病、心血管疾病及其他疾病的患者应避免剧烈运动，以免受伤，否则后果自行承担。\n5.在体育健身过程中健身人员要保管好个人财物，并做好自身伤害安全保护，不做危险动作，不滋事斗殴。\n6.本校对入校健身人员的人身安全、财产安全不承担任何经济赔偿及法律责任。\n7.具体场地开放时间以学校公告为主。","status":"1"}]
         * userpromise : 0
         * sportsortlist : [{"sporttime":"1分钟","nickname":"185***042","sortnum":1},{"sporttime":"1分钟","nickname":"183***025","sortnum":2},{"sporttime":"1分钟","nickname":"137***764","sortnum":3},{"sporttime":"1分钟","nickname":"的的的的的的额额额额额额额额额额","sortnum":4},{"headimgid":"f421d945-dc99-4b23-9159-56903de03c59","sporttime":"1分钟","nickname":"滨海健康","sortnum":5,"headimgpath":"http://103.233.6.43:8009/XDfileserver/20191102074946116300079497.png"},{"sporttime":"1分钟","nickname":"138***931","sortnum":6},{"sporttime":"1分钟","nickname":"139***479","sortnum":7},{"headimgid":"50562aff-6768-4c81-b0a8-064ec29e56e0","sporttime":"1分钟","nickname":"兜里有颗糖","sortnum":8,"headimgpath":"http://103.233.6.43:8009/XDfileserver/2019102721145318020191027091452.png"},{"headimgid":"4e92b9a9-cfff-4d2d-9de8-234b7c5bd4c3","sporttime":"1分钟","nickname":"17688888888","sortnum":9,"headimgpath":"http://103.233.6.43:8009/XDfileserver/20191030114400602255296555.png"},{"headimgid":"2cf86b06-1164-4b44-8ea9-64b46777d117","sporttime":"1分钟","nickname":"这是昵称阿轲","sortnum":10,"headimgpath":"http://103.233.6.43:8009/XDfileserver/20191107160151326214856783.png"},{"sporttime":"1分钟","nickname":"151***948","sortnum":11},{"headimgid":"124e8a3f-f84a-4d99-a7a4-f06bff63d2fe","sporttime":"1分钟","nickname":"159***016","sortnum":12,"headimgpath":"http://103.233.6.43:8009/XDfileserver/20191101085558426210860976.png"},{"sporttime":"1分钟","nickname":"151***094","sortnum":13},{"headimgid":"0ea88c96-5abe-4d67-8b0d-1f2f901f8c87","sporttime":"1分钟","nickname":"克里斯蒂","sortnum":14,"headimgpath":"http://103.233.6.43:8009/XDfileserver/2019102408335568320191024083353.png"}]
         * schoolinfo : {"vacationDateMorningStart":"","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","popularity":257,"managername":"wxy","id":"75ef2c8e-d453-4651-8413-43a4b54e5fda","workDateAfternoonStart":"","periphery":"泰成公园、中国银行、超市、美食、停车场.","lat":"38.962043","connectcode":"022-65828995","createtime":"2019-10-28 11:33:07 128","address":"天津市滨海新区银河二路316号","lng":"117.710243","imgid":"026c91ed-0348-455a-980e-985de0e44b64,42a1456e-028a-4790-b251-28ef22c914d5,b5edff72-cd8f-4441-a18e-48cae592a4ab","workdatemorningend":"","stadiumdesc":"位于天津市滨海新区银河二路316号，滨海新区塘沽草场街小学中部新城校区。","workdate":"","imgpath":"http://103.233.6.43:8009/XDfileserver/20191028113132083tangguzhongbu.jpg,http://103.233.6.43:8009/XDfileserver/20191028113148280tangguzhongbu1.jpg,http://103.233.6.43:8009/XDfileserver/2019102811320944583560217a5f0f3b990be02684bd7529.jpg","workDateAfternoonEnd":"","busroute":"822路、973路","vacationDateAfternoonEnd":"","peoplenumber":0,"vacationDateMorningEnd":"","vacationdate":"","name":"塘沽中部新城学校","applystatus":1,"vacationDateAfternoonStart":"","isdelete":"0","stadiumfaceid":"6ebb5271-2a4b-4d94-bf14-6afec252cbc5","workdatemorningstart":"","status":"1"}
         */

        private int userpromise;
        private SchoolinfoBean schoolinfo;
        private List<UsersportsortsBean> usersportsorts;
        private List<StadiumnoticeBean> stadiumnotice;
        private List<SportsortlistBean> sportsortlist;

        public int getUserpromise() {
            return userpromise;
        }

        public void setUserpromise(int userpromise) {
            this.userpromise = userpromise;
        }

        public SchoolinfoBean getSchoolinfo() {
            return schoolinfo;
        }

        public void setSchoolinfo(SchoolinfoBean schoolinfo) {
            this.schoolinfo = schoolinfo;
        }

        public List<UsersportsortsBean> getUsersportsorts() {
            return usersportsorts;
        }

        public void setUsersportsorts(List<UsersportsortsBean> usersportsorts) {
            this.usersportsorts = usersportsorts;
        }

        public List<StadiumnoticeBean> getStadiumnotice() {
            return stadiumnotice;
        }

        public void setStadiumnotice(List<StadiumnoticeBean> stadiumnotice) {
            this.stadiumnotice = stadiumnotice;
        }

        public List<SportsortlistBean> getSportsortlist() {
            return sportsortlist;
        }

        public void setSportsortlist(List<SportsortlistBean> sportsortlist) {
            this.sportsortlist = sportsortlist;
        }

        public static class SchoolinfoBean {
            /**
             * vacationDateMorningStart :
             * typecode : 1
             * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9e
             * popularity : 257
             * managername : wxy
             * id : 75ef2c8e-d453-4651-8413-43a4b54e5fda
             * workDateAfternoonStart :
             * periphery : 泰成公园、中国银行、超市、美食、停车场.
             * lat : 38.962043
             * connectcode : 022-65828995
             * createtime : 2019-10-28 11:33:07 128
             * address : 天津市滨海新区银河二路316号
             * lng : 117.710243
             * imgid : 026c91ed-0348-455a-980e-985de0e44b64,42a1456e-028a-4790-b251-28ef22c914d5,b5edff72-cd8f-4441-a18e-48cae592a4ab
             * workdatemorningend :
             * stadiumdesc : 位于天津市滨海新区银河二路316号，滨海新区塘沽草场街小学中部新城校区。
             * workdate :
             * imgpath : http://103.233.6.43:8009/XDfileserver/20191028113132083tangguzhongbu.jpg,http://103.233.6.43:8009/XDfileserver/20191028113148280tangguzhongbu1.jpg,http://103.233.6.43:8009/XDfileserver/2019102811320944583560217a5f0f3b990be02684bd7529.jpg
             * workDateAfternoonEnd :
             * busroute : 822路、973路
             * vacationDateAfternoonEnd :
             * peoplenumber : 0
             * vacationDateMorningEnd :
             * vacationdate :
             * name : 塘沽中部新城学校
             * applystatus : 1
             * vacationDateAfternoonStart :
             * isdelete : 0
             * stadiumfaceid : 6ebb5271-2a4b-4d94-bf14-6afec252cbc5
             * workdatemorningstart :
             * status : 1
             */

            private String vacationDateMorningStart;
            private String typecode;
            private String uid;
            private int popularity;
            private String managername;
            private String id;
            private String workDateAfternoonStart;
            private String periphery;
            private String lat;
            private String connectcode;
            private String createtime;
            private String address;
            private String lng;
            private String imgid;
            private String workdatemorningend;
            private String stadiumdesc;
            private String workdate;
            private String imgpath;
            private String workDateAfternoonEnd;
            private String busroute;
            private String vacationDateAfternoonEnd;
            private int peoplenumber;
            private String vacationDateMorningEnd;
            private String vacationdate;
            private String name;
            private int applystatus;
            private String vacationDateAfternoonStart;
            private String isdelete;
            private String stadiumfaceid;
            private String workdatemorningstart;
            private String status;

            public String getVacationDateMorningStart() {
                return vacationDateMorningStart;
            }

            public void setVacationDateMorningStart(String vacationDateMorningStart) {
                this.vacationDateMorningStart = vacationDateMorningStart;
            }

            public String getTypecode() {
                return typecode;
            }

            public void setTypecode(String typecode) {
                this.typecode = typecode;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public int getPopularity() {
                return popularity;
            }

            public void setPopularity(int popularity) {
                this.popularity = popularity;
            }

            public String getManagername() {
                return managername;
            }

            public void setManagername(String managername) {
                this.managername = managername;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getWorkDateAfternoonStart() {
                return workDateAfternoonStart;
            }

            public void setWorkDateAfternoonStart(String workDateAfternoonStart) {
                this.workDateAfternoonStart = workDateAfternoonStart;
            }

            public String getPeriphery() {
                return periphery;
            }

            public void setPeriphery(String periphery) {
                this.periphery = periphery;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getConnectcode() {
                return connectcode;
            }

            public void setConnectcode(String connectcode) {
                this.connectcode = connectcode;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }

            public String getImgid() {
                return imgid;
            }

            public void setImgid(String imgid) {
                this.imgid = imgid;
            }

            public String getWorkdatemorningend() {
                return workdatemorningend;
            }

            public void setWorkdatemorningend(String workdatemorningend) {
                this.workdatemorningend = workdatemorningend;
            }

            public String getStadiumdesc() {
                return stadiumdesc;
            }

            public void setStadiumdesc(String stadiumdesc) {
                this.stadiumdesc = stadiumdesc;
            }

            public String getWorkdate() {
                return workdate;
            }

            public void setWorkdate(String workdate) {
                this.workdate = workdate;
            }

            public String getImgpath() {
                return imgpath;
            }

            public void setImgpath(String imgpath) {
                this.imgpath = imgpath;
            }

            public String getWorkDateAfternoonEnd() {
                return workDateAfternoonEnd;
            }

            public void setWorkDateAfternoonEnd(String workDateAfternoonEnd) {
                this.workDateAfternoonEnd = workDateAfternoonEnd;
            }

            public String getBusroute() {
                return busroute;
            }

            public void setBusroute(String busroute) {
                this.busroute = busroute;
            }

            public String getVacationDateAfternoonEnd() {
                return vacationDateAfternoonEnd;
            }

            public void setVacationDateAfternoonEnd(String vacationDateAfternoonEnd) {
                this.vacationDateAfternoonEnd = vacationDateAfternoonEnd;
            }

            public int getPeoplenumber() {
                return peoplenumber;
            }

            public void setPeoplenumber(int peoplenumber) {
                this.peoplenumber = peoplenumber;
            }

            public String getVacationDateMorningEnd() {
                return vacationDateMorningEnd;
            }

            public void setVacationDateMorningEnd(String vacationDateMorningEnd) {
                this.vacationDateMorningEnd = vacationDateMorningEnd;
            }

            public String getVacationdate() {
                return vacationdate;
            }

            public void setVacationdate(String vacationdate) {
                this.vacationdate = vacationdate;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getApplystatus() {
                return applystatus;
            }

            public void setApplystatus(int applystatus) {
                this.applystatus = applystatus;
            }

            public String getVacationDateAfternoonStart() {
                return vacationDateAfternoonStart;
            }

            public void setVacationDateAfternoonStart(String vacationDateAfternoonStart) {
                this.vacationDateAfternoonStart = vacationDateAfternoonStart;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getStadiumfaceid() {
                return stadiumfaceid;
            }

            public void setStadiumfaceid(String stadiumfaceid) {
                this.stadiumfaceid = stadiumfaceid;
            }

            public String getWorkdatemorningstart() {
                return workdatemorningstart;
            }

            public void setWorkdatemorningstart(String workdatemorningstart) {
                this.workdatemorningstart = workdatemorningstart;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

        public static class UsersportsortsBean {
            /**
             * headimgid : 2cf86b06-1164-4b44-8ea9-64b46777d117
             * test : 1111
             * sporttime : 1分钟
             * nickname : 这是昵称阿轲
             * sortnum : 10
             * headimgpath : http://103.233.6.43:8009/XDfileserver/20191107160151326214856783.png
             * username : 15725040722
             */

            private String headimgid;
            private int test;
            private String sporttime;
            private String nickname;
            private int sortnum;
            private String headimgpath;
            private String username;

            public String getHeadimgid() {
                return headimgid;
            }

            public void setHeadimgid(String headimgid) {
                this.headimgid = headimgid;
            }

            public int getTest() {
                return test;
            }

            public void setTest(int test) {
                this.test = test;
            }

            public String getSporttime() {
                return sporttime;
            }

            public void setSporttime(String sporttime) {
                this.sporttime = sporttime;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public int getSortnum() {
                return sortnum;
            }

            public void setSortnum(int sortnum) {
                this.sortnum = sortnum;
            }

            public String getHeadimgpath() {
                return headimgpath;
            }

            public void setHeadimgpath(String headimgpath) {
                this.headimgpath = headimgpath;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
        }

        public static class StadiumnoticeBean {
            /**
             * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9F
             * createtime : 2019-11-05 15:44:20 048
             * noticedesc : 学校场地公告
             * publishtime :  2019-11-05 03:44:11
             * id : e5f90ed3-3e6c-48e6-ac04-84549858d51a
             * isdelete : 0
             * title : 学校场地公告
             * stadiumid : 75ef2c8e-d453-4651-8413-43a4b54e5fda
             * content : 1.严格按照规定的开放时间进入规定场地活动；严禁进入开放场所以外的其他区域（包括教学区域，寄宿区域等）；不满12周岁未成年人须在监护人带领下进入校园进行体育锻炼。
             2.倡导文明健身，遵守社会公德，保持环境整洁；爱护公共设施，如有损坏应照价赔偿。
             3.十大严禁：严禁翻越围墙、栏杆进入场内；严禁携带宠物进入场内；严禁穿高跟鞋进入场内；严禁在开放场所吸烟；严禁场内乱扔果皮纸屑等杂物；严禁私自移动场内各项设施；严禁在场内私自布置各项设施；严禁健身人员将车辆（自行车、电动车、摩托车、汽车等）驶入校园；严禁酗酒者进入校园进行体育锻炼；严禁在学校开放场地内进行足球、广场舞、轮滑等不安全运动项目。
             4.请根据自己的身体状况选择科学的锻炼方式，高血压、心脏病、心血管疾病及其他疾病的患者应避免剧烈运动，以免受伤，否则后果自行承担。
             5.在体育健身过程中健身人员要保管好个人财物，并做好自身伤害安全保护，不做危险动作，不滋事斗殴。
             6.本校对入校健身人员的人身安全、财产安全不承担任何经济赔偿及法律责任。
             7.具体场地开放时间以学校公告为主。
             * status : 1
             */

            private String uid;
            private String createtime;
            private String noticedesc;
            private String publishtime;
            private String id;
            private String isdelete;
            private String title;
            private String stadiumid;
            private String content;
            private String status;

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getNoticedesc() {
                return noticedesc;
            }

            public void setNoticedesc(String noticedesc) {
                this.noticedesc = noticedesc;
            }

            public String getPublishtime() {
                return publishtime;
            }

            public void setPublishtime(String publishtime) {
                this.publishtime = publishtime;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getStadiumid() {
                return stadiumid;
            }

            public void setStadiumid(String stadiumid) {
                this.stadiumid = stadiumid;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

        public static class SportsortlistBean {
            /**
             * sporttime : 1分钟
             * nickname : 185***042
             * sortnum : 1
             * headimgid : f421d945-dc99-4b23-9159-56903de03c59
             * headimgpath : http://103.233.6.43:8009/XDfileserver/20191102074946116300079497.png
             */

            private String sporttime;
            private String nickname;
            private int sortnum;
            private String headimgid;
            private String headimgpath;

            public String getSporttime() {
                return sporttime;
            }

            public void setSporttime(String sporttime) {
                this.sporttime = sporttime;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public int getSortnum() {
                return sortnum;
            }

            public void setSortnum(int sortnum) {
                this.sortnum = sortnum;
            }

            public String getHeadimgid() {
                return headimgid;
            }

            public void setHeadimgid(String headimgid) {
                this.headimgid = headimgid;
            }

            public String getHeadimgpath() {
                return headimgpath;
            }

            public void setHeadimgpath(String headimgpath) {
                this.headimgpath = headimgpath;
            }
        }
    }
}
