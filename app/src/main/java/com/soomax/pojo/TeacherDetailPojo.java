package com.soomax.pojo;

public class TeacherDetailPojo {


    /**
     * msg : 操作成功
     * res : {"headimgid":"5bf6cf06-5904-4214-b3aa-fc7419fc5e97","createtime":"2019-10-27 19:16:41 506","address":"天津市滨海新区","lng":"117.687368","imgpath":"http://103.233.6.43:8009/XDfileserver/20191027191420836txm.png","sportname":"舞蹈","imglist":"5cfeeb5b-f90b-41c5-ab4a-3b791cfaf7a0,7886639d-42b1-40ba-8fe4-f14a61c2295f,ebe84539-8bb0-426e-8dd0-2666c7908f25","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"12","descs":"从小对舞蹈感兴趣，擅长肚皮舞。教学风格独特，因材施教，深受学员喜欢。\n2017年，受邀参加华东首届东方舞艺术节；\n2018年，受邀参加丝路东方全球东方舞大赛；\n2018年，受邀参加第二届王维国际东方舞艺术节，荣获国际权威带队机构奖；团队包揽少儿团体组亚军，业余团体组亚军，个人明日之星，全国十佳等奖项；\n2019年，受邀参加繁美之星东方舞艺术大赛；\n2019年，受邀受邀参加华北第四届国际东方舞大赛，担任专业大赛评委；荣获权威带队机构奖；团队包揽少儿团体组季军，幼儿融合组季军，业余民俗组全国十强、金奖，半专业pop song组全国十强，半专业融合组冠军等奖项。\n","phone":"16622135566","name":"谭晓明","imglistpath":"http://103.233.6.43:8009/XDfileserver/20191027191503512yishu1.png,http://103.233.6.43:8009/XDfileserver/20191027191505341yishu2.png,http://103.233.6.43:8009/XDfileserver/20191027191508208yishu3.png","id":"b73c79d0-a583-4cf4-98bc-08cc4a3dd82e","isdelete":"0","lat":"39.012813","status":"1"}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * headimgid : 5bf6cf06-5904-4214-b3aa-fc7419fc5e97
         * createtime : 2019-10-27 19:16:41 506
         * address : 天津市滨海新区
         * lng : 117.687368
         * imgpath : http://103.233.6.43:8009/XDfileserver/20191027191420836txm.png
         * sportname : 舞蹈
         * imglist : 5cfeeb5b-f90b-41c5-ab4a-3b791cfaf7a0,7886639d-42b1-40ba-8fe4-f14a61c2295f,ebe84539-8bb0-426e-8dd0-2666c7908f25
         * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9h
         * sportid : 12
         * descs : 从小对舞蹈感兴趣，擅长肚皮舞。教学风格独特，因材施教，深受学员喜欢。
         2017年，受邀参加华东首届东方舞艺术节；
         2018年，受邀参加丝路东方全球东方舞大赛；
         2018年，受邀参加第二届王维国际东方舞艺术节，荣获国际权威带队机构奖；团队包揽少儿团体组亚军，业余团体组亚军，个人明日之星，全国十佳等奖项；
         2019年，受邀参加繁美之星东方舞艺术大赛；
         2019年，受邀受邀参加华北第四届国际东方舞大赛，担任专业大赛评委；荣获权威带队机构奖；团队包揽少儿团体组季军，幼儿融合组季军，业余民俗组全国十强、金奖，半专业pop song组全国十强，半专业融合组冠军等奖项。
         * phone : 16622135566
         * name : 谭晓明
         * imglistpath : http://103.233.6.43:8009/XDfileserver/20191027191503512yishu1.png,http://103.233.6.43:8009/XDfileserver/20191027191505341yishu2.png,http://103.233.6.43:8009/XDfileserver/20191027191508208yishu3.png
         * id : b73c79d0-a583-4cf4-98bc-08cc4a3dd82e
         * isdelete : 0
         * lat : 39.012813
         * status : 1
         */

        private String headimgid;
        private String createtime;
        private String address;
        private String lng;
        private String imgpath;
        private String sportname;
        private String imglist;
        private String uid;
        private String sportid;
        private String descs;
        private String phone;
        private String name;
        private String imglistpath;
        private String id;
        private String isdelete;
        private String lat;
        private String status;

        public String getHeadimgid() {
            return headimgid;
        }

        public void setHeadimgid(String headimgid) {
            this.headimgid = headimgid;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getImgpath() {
            return imgpath;
        }

        public void setImgpath(String imgpath) {
            this.imgpath = imgpath;
        }

        public String getSportname() {
            return sportname;
        }

        public void setSportname(String sportname) {
            this.sportname = sportname;
        }

        public String getImglist() {
            return imglist;
        }

        public void setImglist(String imglist) {
            this.imglist = imglist;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getSportid() {
            return sportid;
        }

        public void setSportid(String sportid) {
            this.sportid = sportid;
        }

        public String getDescs() {
            return descs;
        }

        public void setDescs(String descs) {
            this.descs = descs;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImglistpath() {
            return imglistpath;
        }

        public void setImglistpath(String imglistpath) {
            this.imglistpath = imglistpath;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
