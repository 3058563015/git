package com.soomax.pojo;

public class RegisterPojo {


    /**
     * msg : 操作成功
     * res : {"createtime":"2019-10-31 16:15:06 566","code":"9012","phone":"15176526258","nickname":"151***258","xdimcode":"XDAPP-15176526258","id":"ce123e00-f294-4f45-bc25-118439a6ead1","isdelete":"0","userrolecode":"1","xdimpassword":"dc483e80a7a0bd9ef71d8cf973673924","userpassword":"dc483e80a7a0bd9ef71d8cf973673924","username":"15176526258","status":"1"}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * createtime : 2019-10-31 16:15:06 566
         * code : 9012
         * phone : 15176526258
         * nickname : 151***258
         * xdimcode : XDAPP-15176526258
         * id : ce123e00-f294-4f45-bc25-118439a6ead1
         * isdelete : 0
         * userrolecode : 1
         * xdimpassword : dc483e80a7a0bd9ef71d8cf973673924
         * userpassword : dc483e80a7a0bd9ef71d8cf973673924
         * username : 15176526258
         * status : 1
         */

        private String createtime;
        private String code;
        private String phone;
        private String nickname;
        private String xdimcode;
        private String id;
        private String isdelete;
        private String userrolecode;
        private String xdimpassword;
        private String userpassword;
        private String username;
        private String status;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getXdimcode() {
            return xdimcode;
        }

        public void setXdimcode(String xdimcode) {
            this.xdimcode = xdimcode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getUserrolecode() {
            return userrolecode;
        }

        public void setUserrolecode(String userrolecode) {
            this.userrolecode = userrolecode;
        }

        public String getXdimpassword() {
            return xdimpassword;
        }

        public void setXdimpassword(String xdimpassword) {
            this.xdimpassword = xdimpassword;
        }

        public String getUserpassword() {
            return userpassword;
        }

        public void setUserpassword(String userpassword) {
            this.userpassword = userpassword;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
