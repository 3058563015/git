package com.soomax.pojo;

import java.util.List;

public class Notice02Pojo {

    /**
     * msg : 操作成功
     * res : {"size":5,"list":[{"uid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","createtime":"2019-10-26 08:59","touid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","ctype":"2","excute":"点赞","nickname":"这是昵称阿轲","id":"aed301f5-1b2d-473c-a4c5-ef04ce644e18","isdelete":"0","islook":"1","content":"对你的评论进行了","status":"1"},{"uid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","createtime":"2019-10-30 06:57","touid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","ctype":"2","excute":"点赞","nickname":"这是昵称阿轲","id":"d50dfbc0-e0f0-463a-96bc-b6cb0d2cb795","isdelete":"0","islook":"1","content":"对你的评论进行了","status":"1"},{"uid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","createtime":"2019-10-31 08:15","touid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","ctype":"2","excute":"点赞","nickname":"这是昵称阿轲","id":"e51adaa7-1823-48d6-8a47-42a40efeaeab","isdelete":"0","islook":"1","content":"对你的评论进行了","status":"1"},{"uid":"b9db79da-a019-4c8e-88a7-c3e0951e4de4","createtime":"2019-11-06 11:57","touid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","ctype":"2","excute":"点赞","nickname":"zhoufeng","id":"e44328b2-22cd-48e5-9af2-fcc3dad9abea","isdelete":"0","islook":"0","content":"对你的评论进行了","status":"1"},{"uid":"7d7d8d0a-05f9-4e51-8910-8fdf2ddb3a06","createtime":"2019-11-06 12:16","touid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","ctype":"2","excute":"点赞","nickname":"17666666666","id":"72eb3c66-f8e7-4776-b97a-ea5562147966","isdelete":"0","islook":"0","content":"对你的评论进行了","status":"1"}]}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * size : 5
         * list : [{"uid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","createtime":"2019-10-26 08:59","touid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","ctype":"2","excute":"点赞","nickname":"这是昵称阿轲","id":"aed301f5-1b2d-473c-a4c5-ef04ce644e18","isdelete":"0","islook":"1","content":"对你的评论进行了","status":"1"},{"uid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","createtime":"2019-10-30 06:57","touid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","ctype":"2","excute":"点赞","nickname":"这是昵称阿轲","id":"d50dfbc0-e0f0-463a-96bc-b6cb0d2cb795","isdelete":"0","islook":"1","content":"对你的评论进行了","status":"1"},{"uid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","createtime":"2019-10-31 08:15","touid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","ctype":"2","excute":"点赞","nickname":"这是昵称阿轲","id":"e51adaa7-1823-48d6-8a47-42a40efeaeab","isdelete":"0","islook":"1","content":"对你的评论进行了","status":"1"},{"uid":"b9db79da-a019-4c8e-88a7-c3e0951e4de4","createtime":"2019-11-06 11:57","touid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","ctype":"2","excute":"点赞","nickname":"zhoufeng","id":"e44328b2-22cd-48e5-9af2-fcc3dad9abea","isdelete":"0","islook":"0","content":"对你的评论进行了","status":"1"},{"uid":"7d7d8d0a-05f9-4e51-8910-8fdf2ddb3a06","createtime":"2019-11-06 12:16","touid":"48269ee4-b9e4-41aa-af20-504f3b3d1e82","ctype":"2","excute":"点赞","nickname":"17666666666","id":"72eb3c66-f8e7-4776-b97a-ea5562147966","isdelete":"0","islook":"0","content":"对你的评论进行了","status":"1"}]
         */

        private int size;
        private List<ListBean> list;

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * uid : 48269ee4-b9e4-41aa-af20-504f3b3d1e82
             * createtime : 2019-10-26 08:59
             * touid : 48269ee4-b9e4-41aa-af20-504f3b3d1e82
             * ctype : 2
             * excute : 点赞
             * nickname : 这是昵称阿轲
             * id : aed301f5-1b2d-473c-a4c5-ef04ce644e18
             * isdelete : 0
             * islook : 1
             * content : 对你的评论进行了
             * status : 1
             */

            private String uid;
            private String createtime;
            private String touid;
            private String ctype;
            private String excute;
            private String nickname;
            private String id;
            private String isdelete;
            private String islook;
            private String content;
            private String status;

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getTouid() {
                return touid;
            }

            public void setTouid(String touid) {
                this.touid = touid;
            }

            public String getCtype() {
                return ctype;
            }

            public void setCtype(String ctype) {
                this.ctype = ctype;
            }

            public String getExcute() {
                return excute;
            }

            public void setExcute(String excute) {
                this.excute = excute;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getIslook() {
                return islook;
            }

            public void setIslook(String islook) {
                this.islook = islook;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }
}
