package com.soomax.pojo;

import java.util.List;

public class SportClassPojo {


    /**
     * msg : 操作成功
     * res : [{"name":"全部"},{"name":"足球","id":"1","isdelete":"0","status":"1"},{"name":"滑冰","id":"10","isdelete":"0","status":"1"},{"name":"篮球","id":"11","isdelete":"0","status":"1"},{"name":"舞蹈","id":"12","isdelete":"0","status":"1"},{"name":"散打","id":"13","isdelete":"0","status":"1"},{"name":"羽毛球","id":"2","isdelete":"0","status":"1"},{"name":"乒乓球","id":"3","isdelete":"0","status":"1"},{"name":"排球","id":"4","isdelete":"0","status":"1"},{"name":"网球","id":"5","isdelete":"0","status":"1"},{"name":"瑜伽","id":"6","isdelete":"0","status":"1"},{"name":"游泳","id":"7","isdelete":"0","status":"1"},{"name":"健身","id":"8","isdelete":"0","status":"1"},{"name":"桌球","id":"9","isdelete":"0","status":"1"}]
     * code : 200
     */

    private String msg;
    private String code;
    private List<ResBean> res;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<ResBean> getRes() {
        return res;
    }

    public void setRes(List<ResBean> res) {
        this.res = res;
    }

    public static class ResBean {
        /**
         * name : 全部
         * id : 1
         * isdelete : 0
         * status : 1
         */

        private String name;
        private String id;
        private String isdelete;
        private String status;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
