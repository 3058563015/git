package com.soomax.pojo;

import java.util.List;

public class VideoDetailPojo {


    /**
     * msg : 操作成功
     * res : {"commentlist":[{"headimgid":"c18f8eb4-e809-413b-ab55-ebbd70ca840f","createtime":"2019-10-28 00:54:31 628","viewtime":"2019-10-28","contentid":"c73dd9a3-3a31-49ae-9899-b5310fece295","content":"厉害厉害","islikecount":0,"uid":"b9db79da-a019-4c8e-88a7-c3e0951e4de4","score":"5","commentcount":0,"ctype":"1","islike":0,"nickname":"zhoufeng","childcommentlist":[],"id":"296c76e5-9960-4c27-b80f-347dab7faff1","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191108170932788330387409.jpg"}],"sportroominfo":{"createtime":"2019-10-27 19:59:42 906","recordcount":128,"imgid":"60a5d630-a795-4468-a591-2a14057b486b","imgpath":"http://103.233.6.43:8009/XDfileserver/201910271958517761.jpg","title":"台球小知识","sportname":"健身,台球,其它","likecount":1,"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","commentlist":[],"descs":"打台球时出杆就歪，直球还打不进？","islike":0,"id":"c73dd9a3-3a31-49ae-9899-b5310fece295","isdelete":"0","linkurl":"http://app.bhxdty.com/20191027/1.mp4","commentlistsize":0,"status":"1"},"commentlistsize":1}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * commentlist : [{"headimgid":"c18f8eb4-e809-413b-ab55-ebbd70ca840f","createtime":"2019-10-28 00:54:31 628","viewtime":"2019-10-28","contentid":"c73dd9a3-3a31-49ae-9899-b5310fece295","content":"厉害厉害","islikecount":0,"uid":"b9db79da-a019-4c8e-88a7-c3e0951e4de4","score":"5","commentcount":0,"ctype":"1","islike":0,"nickname":"zhoufeng","childcommentlist":[],"id":"296c76e5-9960-4c27-b80f-347dab7faff1","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191108170932788330387409.jpg"}]
         * sportroominfo : {"createtime":"2019-10-27 19:59:42 906","recordcount":128,"imgid":"60a5d630-a795-4468-a591-2a14057b486b","imgpath":"http://103.233.6.43:8009/XDfileserver/201910271958517761.jpg","title":"台球小知识","sportname":"健身,台球,其它","likecount":1,"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","commentlist":[],"descs":"打台球时出杆就歪，直球还打不进？","islike":0,"id":"c73dd9a3-3a31-49ae-9899-b5310fece295","isdelete":"0","linkurl":"http://app.bhxdty.com/20191027/1.mp4","commentlistsize":0,"status":"1"}
         * commentlistsize : 1
         */

        private SportroominfoBean sportroominfo;
        private int commentlistsize;
        private List<CommentlistBean> commentlist;

        public SportroominfoBean getSportroominfo() {
            return sportroominfo;
        }

        public void setSportroominfo(SportroominfoBean sportroominfo) {
            this.sportroominfo = sportroominfo;
        }

        public int getCommentlistsize() {
            return commentlistsize;
        }

        public void setCommentlistsize(int commentlistsize) {
            this.commentlistsize = commentlistsize;
        }

        public List<CommentlistBean> getCommentlist() {
            return commentlist;
        }

        public void setCommentlist(List<CommentlistBean> commentlist) {
            this.commentlist = commentlist;
        }

        public static class SportroominfoBean {
            /**
             * createtime : 2019-10-27 19:59:42 906
             * recordcount : 128
             * imgid : 60a5d630-a795-4468-a591-2a14057b486b
             * imgpath : http://103.233.6.43:8009/XDfileserver/201910271958517761.jpg
             * title : 台球小知识
             * sportname : 健身,台球,其它
             * likecount : 1
             * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9F
             * commentlist : []
             * descs : 打台球时出杆就歪，直球还打不进？
             * islike : 0
             * id : c73dd9a3-3a31-49ae-9899-b5310fece295
             * isdelete : 0
             * linkurl : http://app.bhxdty.com/20191027/1.mp4
             * commentlistsize : 0
             * status : 1
             */

            private String createtime;
            private int recordcount;
            private String imgid;
            private String imgpath;
            private String title;
            private String sportname;
            private int likecount;
            private String uid;
            private String descs;
            private int islike;
            private String id;
            private String isdelete;
            private String linkurl;
            private int commentlistsize;
            private String status;
            private List<?> commentlist;

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public int getRecordcount() {
                return recordcount;
            }

            public void setRecordcount(int recordcount) {
                this.recordcount = recordcount;
            }

            public String getImgid() {
                return imgid;
            }

            public void setImgid(String imgid) {
                this.imgid = imgid;
            }

            public String getImgpath() {
                return imgpath;
            }

            public void setImgpath(String imgpath) {
                this.imgpath = imgpath;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSportname() {
                return sportname;
            }

            public void setSportname(String sportname) {
                this.sportname = sportname;
            }

            public int getLikecount() {
                return likecount;
            }

            public void setLikecount(int likecount) {
                this.likecount = likecount;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getDescs() {
                return descs;
            }

            public void setDescs(String descs) {
                this.descs = descs;
            }

            public int getIslike() {
                return islike;
            }

            public void setIslike(int islike) {
                this.islike = islike;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getLinkurl() {
                return linkurl;
            }

            public void setLinkurl(String linkurl) {
                this.linkurl = linkurl;
            }

            public int getCommentlistsize() {
                return commentlistsize;
            }

            public void setCommentlistsize(int commentlistsize) {
                this.commentlistsize = commentlistsize;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public List<?> getCommentlist() {
                return commentlist;
            }

            public void setCommentlist(List<?> commentlist) {
                this.commentlist = commentlist;
            }
        }

        public static class CommentlistBean {
            /**
             * headimgid : c18f8eb4-e809-413b-ab55-ebbd70ca840f
             * createtime : 2019-10-28 00:54:31 628
             * viewtime : 2019-10-28
             * contentid : c73dd9a3-3a31-49ae-9899-b5310fece295
             * content : 厉害厉害
             * islikecount : 0
             * uid : b9db79da-a019-4c8e-88a7-c3e0951e4de4
             * score : 5
             * commentcount : 0
             * ctype : 1
             * islike : 0
             * nickname : zhoufeng
             * childcommentlist : []
             * id : 296c76e5-9960-4c27-b80f-347dab7faff1
             * isdelete : 0
             * headimgpath : http://103.233.6.43:8009/XDfileserver/20191108170932788330387409.jpg
             */

            private String headimgid;
            private String createtime;
            private String viewtime;
            private String contentid;
            private String content;
            private int islikecount;
            private String uid;
            private String score;
            private int commentcount;
            private String ctype;
            private int islike;
            private String nickname;
            private String id;
            private String isdelete;
            private String headimgpath;
            private List<?> childcommentlist;

            public String getHeadimgid() {
                return headimgid;
            }

            public void setHeadimgid(String headimgid) {
                this.headimgid = headimgid;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getViewtime() {
                return viewtime;
            }

            public void setViewtime(String viewtime) {
                this.viewtime = viewtime;
            }

            public String getContentid() {
                return contentid;
            }

            public void setContentid(String contentid) {
                this.contentid = contentid;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public int getIslikecount() {
                return islikecount;
            }

            public void setIslikecount(int islikecount) {
                this.islikecount = islikecount;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getScore() {
                return score;
            }

            public void setScore(String score) {
                this.score = score;
            }

            public int getCommentcount() {
                return commentcount;
            }

            public void setCommentcount(int commentcount) {
                this.commentcount = commentcount;
            }

            public String getCtype() {
                return ctype;
            }

            public void setCtype(String ctype) {
                this.ctype = ctype;
            }

            public int getIslike() {
                return islike;
            }

            public void setIslike(int islike) {
                this.islike = islike;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getHeadimgpath() {
                return headimgpath;
            }

            public void setHeadimgpath(String headimgpath) {
                this.headimgpath = headimgpath;
            }

            public List<?> getChildcommentlist() {
                return childcommentlist;
            }

            public void setChildcommentlist(List<?> childcommentlist) {
                this.childcommentlist = childcommentlist;
            }
        }
    }
}
