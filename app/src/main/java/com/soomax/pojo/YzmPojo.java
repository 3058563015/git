package com.soomax.pojo;

public class YzmPojo {


    /**
     * msg : 操作成功
     * res : {"createtime":"2019-10-31 16:03:51 992","code":2605,"smsid":"0D94814A-16DB-45C6-BC6A-308E301DB324","phone":"15176526258","id":"4903c1be-9789-4567-a1d5-d94116668534","message":"【享动体育】您的验证码为：2605，请不要把验证码泄露于他人！1分钟内有效。","status":1}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * createtime : 2019-10-31 16:03:51 992
         * code : 2605
         * smsid : 0D94814A-16DB-45C6-BC6A-308E301DB324
         * phone : 15176526258
         * id : 4903c1be-9789-4567-a1d5-d94116668534
         * message : 【享动体育】您的验证码为：2605，请不要把验证码泄露于他人！1分钟内有效。
         * status : 1
         */

        private String createtime;
        private int code;
        private String smsid;
        private String phone;
        private String id;
        private String message;
        private int status;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getSmsid() {
            return smsid;
        }

        public void setSmsid(String smsid) {
            this.smsid = smsid;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
