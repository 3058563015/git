package com.soomax.pojo;

import java.util.List;

public class SaishiPojo {


    /**
     * msg : 操作成功
     * res : [{"num":"不限","reportbegintime":"2019-10-24 12:00:00","remark":"1. 成功报名本次比赛后因个人原因不能按时参加比赛的，按自动弃权处理，报名费不予退还。主办方不提供退赛通道。\n2. 报名通道关闭后，如需转让名额，请在11月4日下午17:00之前由转让方与我方联系。私自转让名额的，一经发现，主办方有权取消该选手参赛资格与成绩。\n3.为杜绝替跑、代跑的情况，请参加比赛的家长，携带儿童有效证件（例如：身份证，护照）以备查验！也希望所有参赛家长遵守比赛规则，给小朋友们营造一个公平的竞赛氛围。","starttime":"2019-11-24 08:00:00","title":"2019武清区儿童平衡车大赛","content":"","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","qualidesc":"2岁至5周岁少儿","managername":"老师","id":"89be9d00-2e6c-4e85-a776-2500334618da","lat":"39.42057","createtime":"2019-10-27 20:08:26 919","address":"天津市武清区新创路","cost":"详情电话咨询","lng":"117.014368","imgid":"78855393-fc6a-4eb8-a49d-db1b5631311b","imgpath":"http://103.233.6.43:8009/XDfileserver/20191027200120030060c9d63013f90ee97202c0a93cf055.jpg","endtime":"2019-11-24 08:00:00","reportstatus":"报名中","reportendtime":"2019-10-30 12:00:00","imglist":"493004ec-43a3-4451-bcd4-4c4193b902fe,b2d9e1f9-1426-4d96-be67-db6ebb99affa,4ffdc50f-cee9-4591-abd9-8332efddb14c","descs":"主办单位：天津市武清区体育局\n协办单位：中体场馆运营管理(天津）有限公司\n承办单位：乐菲骑行（天津）体育文化传播有限公司\n报名时间：10月24日-10月30日\n活动时间：11月24日（星期日）\n          上午：2岁组/3岁组比赛\n          下午：4岁组/5岁组/OPEN组比赛\n（此时间仅供参考，具体时刻以报名结束后公布的《赛程与分组》为准，请您报名后时刻关注官方公众号和赛事群信息）\n赛事地点：天津市武清区体育中心A座","classid":"8","phone":"13820562636","reportstatuscode":1,"isdelete":"0","status":"1"},{"num":"不限","reportbegintime":"2019-10-18 12:00:00","remark":"搏击散打，不仅可以让青少年强身健体，更是青少年素质教育的重要组成部分；体育，不仅只有强壮筋骨的\u201c体\u201d字，更有一个\u201c育\u201d字\u2014\u2014一个塑造品格的过程，这个育人过程远比书本上的教诲更具实践性。","starttime":"2019-10-28 12:00:00","title":"\u201c2019天津市散打俱乐部联赛\u201d即将开赛","content":"","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","qualidesc":"据介绍，赛事主办方希望将本次比赛作为完善体育教育和培训体系，深化产教融合、校企合作的创新模式探索。\u201c我们希望通过这一赛事平台，展现广大青少年参赛者的精神面貌和风采。\u201d活动相关负责人说。","managername":"赵老师","id":"fd2bd72f-a36a-40c8-b830-a06eacb4bad1","lat":"39.407805","createtime":"2019-10-27 19:51:12 623","address":"天津市武清区三号路11号","cost":"详情电话咨询","lng":"117.08084","imgid":"a3e0b752-e179-41dd-acfc-be39d31e19c1","imgpath":"http://103.233.6.43:8009/XDfileserver/2019102719480538311.jpg","endtime":"2019-11-03 12:00:00","reportstatus":"已截止","reportendtime":"2019-10-27 12:00:00","imglist":"ded42924-de30-4662-a8e6-7ee840f18100,465fa3c6-8d50-467f-9e06-00882d6b9a69,385f0f47-8f58-40bd-894d-c0880752b19a","descs":"为强调赛事的权威性、普及性、传播性和时尚性，组织方除要求参赛选手严格遵守和执行竞赛规程，规则和相关规定外，采取了一系列安全措施保障赛事安全。多名教练曾担任过WBC亚洲职业拳击&泰拳冠军赛、第十三届全国运动会武说散打项目测试赛暨2017青少年术武散打锦标赛、全国MMA综合格斗、天津市第十四届运动会武术散打比赛等国际、国内大型体育赛事裁判工作，确保比赛的高水平和安全性。除此之外，还将通过微博、微信等形式进行互动展示，让大家近距离接触和了解散打运动。","classid":"13","phone":"022-66282000","reportstatuscode":3,"isdelete":"0","status":"1"},{"num":"不限","reportbegintime":"2019-10-08 12:00:00","remark":"相关要求：\n（一）参赛选手一律不得穿带钉舞鞋上场。\n（二）参赛选手必须佩戴比赛背号，提前20分钟到检录处点名，点名后5分钟未到场检录者。按弃权处理，造成的任何后果由个人承担。\n（三）各代表队的领队负责本队的管理工作，配合赛场安排保证大赛顺利进行。\n（四）参赛选手应遵守赛场纪律，如有选手发生扰乱比赛的事件（包括罢赛、起哄、闹事等），将取消对该选手的本次比赛资格和成绩。\n（五）裁判员应秉公执法，不营私舞弊，如发现违纪现象，组委会将停止其裁判工作。\n（六）各代表队及个人对比赛成绩有异议，必须由该队领队以文字形式提出书面申诉并交纳申诉费200元，如胜诉申诉费退还，如败诉申诉费不予退还。\n","starttime":"2019-10-26 12:00:00","title":"第一届中国·天津体育舞蹈全国公开赛暨天津市滨海新区第六届全民健身体育舞蹈比赛","content":"","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","qualidesc":"凡具有中国人民共和国国籍的公民，符合参赛项目的规定，身体健康，具有一定运动能力的集体个人均可报名参加比赛。\n","managername":"杨晋文","id":"cf281eb0-e462-4f21-a14c-70fe87f14bf7","lat":"39.034257","createtime":"2019-10-27 19:43:52 216","address":"天津市滨海新区塘沽体育馆","cost":"详情电话咨询","lng":"117.653335","imgid":"be35b618-9f44-4fa2-a489-7fb54e827030","imgpath":"http://103.233.6.43:8009/XDfileserver/201910271940501980376f17eab9cfae0896d71d39a566f9.jpg","endtime":"2019-10-27 12:00:00","reportstatus":"已截止","reportendtime":"2019-10-15 12:00:00","imglist":"e29e1baf-82bc-4d1a-87a2-6a869e3b7852,1df307be-4807-45db-bbfa-ad83a76660f8,688f6dac-78de-457e-8461-fa1cdec186b9","descs":"一、主办单位：天津市滨海新区教育体育局\n二、承办单位：天津市滨海新区体育舞蹈运动协会、天津晨辉艺杰体育文化传播有限公司\n三、协办单位：天津体育学院体育文化学院校友会\n四、支持单位 ：中国体育舞蹈联合会、天津市体育舞蹈运动协会\n五、媒体支持单位： 尚舞杂志 人民网 新华网 中国小康网 新华视屏 新浪网 腾讯 网易 今日头条\n","classid":"12","phone":"13820979518","reportstatuscode":3,"isdelete":"0","status":"1"},{"num":"每组7人","reportbegintime":"2019-10-08 12:00:00","remark":"由滨海新区教体局主办，滨海新区足球协会、滨海新区大港体育场、滨海新区塘沽体育场、滨海新区汉沽体育场承办的2019年滨海新区全民健身运动会七人制足球比赛","starttime":"2019-10-19 12:00:00","title":"2019年滨海新区全民健身运动会七人制足球比赛","content":"","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","qualidesc":"每队7人","managername":"赵老师","id":"14a456ce-a096-4f01-9f68-c325246ce16b","lat":"39.026688","createtime":"2019-10-27 19:12:05 408","address":"天津市滨海新区塘沽体育场","cost":"详情电话咨询","lng":"117.657071","imgid":"4de61c99-8020-4388-a40a-ee2414008123","imgpath":"http://103.233.6.43:8009/XDfileserver/20191027190801022qirenzhi.jpg","endtime":"2019-11-24 12:00:00","reportstatus":"已截止","reportendtime":"2019-10-18 12:00:00","imglist":"3802747f-899b-495a-9e52-37e36f0c76d6,10a66f1f-22ea-4b36-bd1e-86c9d8768a00,1ba941d9-9861-45ba-8552-cd073d674cb4","descs":"由滨海新区教体局主办，滨海新区足球协会、滨海新区大港体育场、滨海新区塘沽体育场、滨海新区汉沽体育场承办的2019年滨海新区全民健身运动会七人制足球比赛鸣哨开始。","classid":"1","phone":"022-66282000","reportstatuscode":3,"isdelete":"0","status":"1"}]
     * code : 200
     * size : 4
     */

    private String msg;
    private String code;
    private int size;
    private List<ResBean> res;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<ResBean> getRes() {
        return res;
    }

    public void setRes(List<ResBean> res) {
        this.res = res;
    }

    public static class ResBean {
        /**
         * num : 不限
         * reportbegintime : 2019-10-24 12:00:00
         * remark : 1. 成功报名本次比赛后因个人原因不能按时参加比赛的，按自动弃权处理，报名费不予退还。主办方不提供退赛通道。
         2. 报名通道关闭后，如需转让名额，请在11月4日下午17:00之前由转让方与我方联系。私自转让名额的，一经发现，主办方有权取消该选手参赛资格与成绩。
         3.为杜绝替跑、代跑的情况，请参加比赛的家长，携带儿童有效证件（例如：身份证，护照）以备查验！也希望所有参赛家长遵守比赛规则，给小朋友们营造一个公平的竞赛氛围。
         * starttime : 2019-11-24 08:00:00
         * title : 2019武清区儿童平衡车大赛
         * content :
         * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9e
         * qualidesc : 2岁至5周岁少儿
         * managername : 老师
         * id : 89be9d00-2e6c-4e85-a776-2500334618da
         * lat : 39.42057
         * createtime : 2019-10-27 20:08:26 919
         * address : 天津市武清区新创路
         * cost : 详情电话咨询
         * lng : 117.014368
         * imgid : 78855393-fc6a-4eb8-a49d-db1b5631311b
         * imgpath : http://103.233.6.43:8009/XDfileserver/20191027200120030060c9d63013f90ee97202c0a93cf055.jpg
         * endtime : 2019-11-24 08:00:00
         * reportstatus : 报名中
         * reportendtime : 2019-10-30 12:00:00
         * imglist : 493004ec-43a3-4451-bcd4-4c4193b902fe,b2d9e1f9-1426-4d96-be67-db6ebb99affa,4ffdc50f-cee9-4591-abd9-8332efddb14c
         * descs : 主办单位：天津市武清区体育局
         协办单位：中体场馆运营管理(天津）有限公司
         承办单位：乐菲骑行（天津）体育文化传播有限公司
         报名时间：10月24日-10月30日
         活动时间：11月24日（星期日）
         上午：2岁组/3岁组比赛
         下午：4岁组/5岁组/OPEN组比赛
         （此时间仅供参考，具体时刻以报名结束后公布的《赛程与分组》为准，请您报名后时刻关注官方公众号和赛事群信息）
         赛事地点：天津市武清区体育中心A座
         * classid : 8
         * phone : 13820562636
         * reportstatuscode : 1
         * isdelete : 0
         * status : 1
         */

        private String num;
        private String reportbegintime;
        private String remark;
        private String starttime;
        private String title;
        private String content;
        private String uid;
        private String qualidesc;
        private String managername;
        private String id;
        private String lat;
        private String createtime;
        private String address;
        private String cost;
        private String lng;
        private String imgid;
        private String imgpath;
        private String endtime;
        private String reportstatus;
        private String reportendtime;
        private String imglist;
        private String descs;
        private String classid;
        private String phone;
        private int reportstatuscode;
        private String isdelete;
        private String status;

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getReportbegintime() {
            return reportbegintime;
        }

        public void setReportbegintime(String reportbegintime) {
            this.reportbegintime = reportbegintime;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getQualidesc() {
            return qualidesc;
        }

        public void setQualidesc(String qualidesc) {
            this.qualidesc = qualidesc;
        }

        public String getManagername() {
            return managername;
        }

        public void setManagername(String managername) {
            this.managername = managername;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getImgid() {
            return imgid;
        }

        public void setImgid(String imgid) {
            this.imgid = imgid;
        }

        public String getImgpath() {
            return imgpath;
        }

        public void setImgpath(String imgpath) {
            this.imgpath = imgpath;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getReportstatus() {
            return reportstatus;
        }

        public void setReportstatus(String reportstatus) {
            this.reportstatus = reportstatus;
        }

        public String getReportendtime() {
            return reportendtime;
        }

        public void setReportendtime(String reportendtime) {
            this.reportendtime = reportendtime;
        }

        public String getImglist() {
            return imglist;
        }

        public void setImglist(String imglist) {
            this.imglist = imglist;
        }

        public String getDescs() {
            return descs;
        }

        public void setDescs(String descs) {
            this.descs = descs;
        }

        public String getClassid() {
            return classid;
        }

        public void setClassid(String classid) {
            this.classid = classid;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public int getReportstatuscode() {
            return reportstatuscode;
        }

        public void setReportstatuscode(int reportstatuscode) {
            this.reportstatuscode = reportstatuscode;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
