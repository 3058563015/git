package com.soomax.pojo;

import java.util.List;

public class HealthSportPojo {


    /**
     * msg : 操作成功
     * res : [{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","createtime":"2019-10-26","descs":"体育课堂5体育课堂5体育课堂5","imgid":"32213b26-6ed6-49a7-b96c-07bf3073bf33","imgpath":"http://103.233.6.43:8009/XDfileserver/20191026092728415Img314235964.jpg","id":"8dda6959-fb0b-4803-b714-2e2168180f10","isdelete":"0","linkurl":"https://www.apple.com/105/media/us/iphone-x/2017/01df5b43-28e4-4848-bf20-490c34a926a7/films/feature/iphone-x-feature-tpl-cc-us-20170912_1280x720h.mp4","title":"体育课堂7","sportname":"足球,网球,游泳,排球,篮球","status":"1"},{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","createtime":"2019-10-26","descs":"体育课堂5体育课堂5体育课堂5","imgid":"","id":"182970f7-167e-4d6d-bacf-02abaea2b1aa","isdelete":"0","linkurl":"https://www.apple.com/105/media/us/iphone-x/2017/01df5b43-28e4-4848-bf20-490c34a926a7/films/feature/iphone-x-feature-tpl-cc-us-20170912_1280x720h.mp4","title":"体育课堂6","sportname":"足球,篮球,排球,游泳,网球","status":"1"},{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","createtime":"2019-10-26","descs":"体育课堂5体育课堂5体育课堂5","imgid":"ceb1f8bc-28b6-4c1b-9eaf-2e046e869110","imgpath":"http://103.233.6.43:8009/XDfileserver/20191026092421091fa2c91f391544952a3c2397f596b8384.png","id":"2d86f36a-5db5-452d-8320-2726ff77d2dc","isdelete":"0","linkurl":"https://www.apple.com/105/media/us/iphone-x/2017/01df5b43-28e4-4848-bf20-490c34a926a7/films/feature/iphone-x-feature-tpl-cc-us-20170912_1280x720h.mp4","title":"体育课堂5","sportname":"","status":"1"},{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","createtime":"2019-10-26","descs":"体育课堂4体育课堂4体育课堂4","imgid":"3f648c8b-a063-4b5a-b914-d7fd3e0353d9","imgpath":"http://103.233.6.43:8009/XDfileserver/2019102609230701425279801_1378348357330.jpg","id":"26c6b982-d31c-490b-8f25-ad4262fafab4","isdelete":"0","linkurl":"https://www.apple.com/105/media/us/iphone-x/2017/01df5b43-28e4-4848-bf20-490c34a926a7/films/feature/iphone-x-feature-tpl-cc-us-20170912_1280x720h.mp4","title":"体育课堂4","sportname":"","status":"1"},{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","createtime":"2019-10-26","descs":"体育课堂3333","imgid":"8185e573-f8c2-4644-9cd2-b29b1313dfb7","imgpath":"http://103.233.6.43:8009/XDfileserver/20191026092009649904373dc6d992bcec8bf1d6e564bcac3de060340.jpg","id":"e9809e6b-7c41-419d-ad0f-d0a73ddc0c19","isdelete":"0","linkurl":"https://www.apple.com/105/media/us/iphone-x/2017/01df5b43-28e4-4848-bf20-490c34a926a7/films/feature/iphone-x-feature-tpl-cc-us-20170912_1280x720h.mp4","title":"体育课堂3","sportname":"","status":"1"},{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","createtime":"2019-10-26","descs":"这是一条体育课堂数据2","imgid":"1d1a6fb4-0f52-49c0-82fe-2fd365ec91c2","imgpath":"http://103.233.6.43:8009/XDfileserver/2019102609003521659bbc4288d107.jpg","id":"4d44bd24-a04d-430f-85c4-91c3f7edbf1e","isdelete":"0","linkurl":"https://www.apple.com/105/media/us/iphone-x/2017/01df5b43-28e4-4848-bf20-490c34a926a7/films/feature/iphone-x-feature-tpl-cc-us-20170912_1280x720h.mp4","title":"运动课堂2","sportname":"","status":"1"},{"createtime":"2019-10-25","descs":"这是一条运动课堂数据","imgid":"e053327a-6d57-47cc-a6fe-88e97cc3c9c1","imgpath":"http://103.233.6.43:8009/XDfileserver/2019102523111949553cdd1f7c1f21.jpg","id":"75ea296e-5e25-4435-8580-b09f6ec0ccbe","isdelete":"0","linkurl":"https://www.apple.com/105/media/us/iphone-x/2017/01df5b43-28e4-4848-bf20-490c34a926a7/films/feature/iphone-x-feature-tpl-cc-us-20170912_1280x720h.mp4","title":"运动课堂","sportname":"","status":"1"}]
     * code : 200
     * size : 7
     */

    private String msg;
    private String code;
    private int size;
    private List<ResBean> res;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<ResBean> getRes() {
        return res;
    }

    public void setRes(List<ResBean> res) {
        this.res = res;
    }

    public static class ResBean {
        /**
         * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9F
         * createtime : 2019-10-26
         * descs : 体育课堂5体育课堂5体育课堂5
         * imgid : 32213b26-6ed6-49a7-b96c-07bf3073bf33
         * imgpath : http://103.233.6.43:8009/XDfileserver/20191026092728415Img314235964.jpg
         * id : 8dda6959-fb0b-4803-b714-2e2168180f10
         * isdelete : 0
         * linkurl : https://www.apple.com/105/media/us/iphone-x/2017/01df5b43-28e4-4848-bf20-490c34a926a7/films/feature/iphone-x-feature-tpl-cc-us-20170912_1280x720h.mp4
         * title : 体育课堂7
         * sportname : 足球,网球,游泳,排球,篮球
         * status : 1
         */

        private String uid;
        private String createtime;
        private String descs;
        private String imgid;
        private String imgpath;
        private String id;
        private String isdelete;
        private String linkurl;
        private String title;
        private String sportname;
        private String status;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getDescs() {
            return descs;
        }

        public void setDescs(String descs) {
            this.descs = descs;
        }

        public String getImgid() {
            return imgid;
        }

        public void setImgid(String imgid) {
            this.imgid = imgid;
        }

        public String getImgpath() {
            return imgpath;
        }

        public void setImgpath(String imgpath) {
            this.imgpath = imgpath;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getLinkurl() {
            return linkurl;
        }

        public void setLinkurl(String linkurl) {
            this.linkurl = linkurl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSportname() {
            return sportname;
        }

        public void setSportname(String sportname) {
            this.sportname = sportname;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
