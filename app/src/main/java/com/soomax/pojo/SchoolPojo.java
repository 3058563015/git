package com.soomax.pojo;

import java.util.List;

public class SchoolPojo {


    /**
     * msg : 操作成功
     * res : [{"distance":"60.8km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"5947c30d-d4ec-4525-8110-6f8ce7e29e37","workDateAfternoonStart":"18:00","periphery":"汉沽三幼","lat":"39.25983","connectcode":"022-67193903","createtime":"2019-10-27 17:14:31 963","address":"天津市滨海新区友谊街与建设南路交口东100米路南。","lng":"117.827251","imgid":"56f7b1e2-9960-4c51-b343-10f9c17dc928,c11e33f0-b493-4584-b01d-76f996eed62c,2347617a-0d45-44e3-a5cf-a7a35706c5f2,31ee4813-3ea4-43e2-ad85-082dd42d41fc","workdatemorningend":"","stadiumdesc":"学校领导一班人，积极探索特色办学的思路和途径，注重校外资源的借鉴与校内资源的开发。在低年级开设了珠脑心算实验课题;语文、英语学科实施了小学生能力开发课题实验，两项课题均得到市教科院的关注，并建立我区首例实验基地，此课题具有广泛性和前沿性，构建了符合、尊重儿童个性发展规律，倡导并实施:\"主动参与，探索发现，交流合作\"的学习方式，使学生从小学会交往，学会学习，学会思考，学会创新，成为智商情商和谐发展的个性主体。","workDateAfternoonEnd":"21:30","busroute":"400路、404路。","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"汉沽东海小学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027170357189hangudonghaixiaoxue.jpg","stadiumfaceid":"94cb7e4c-cdaf-48a6-85e1-7889d8a69970","workdatemorningstart":"","status":"1"},{"distance":"60.8km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"66a27071-178f-44ae-964d-db0dced5b15a","workDateAfternoonStart":"18:00","periphery":"九龙里小区","lat":"39.282283","connectcode":" 022-25695172","createtime":"2019-10-27 17:01:39 053","address":"天津市滨海新区汉沽河西九方块三经路","lng":"117.799107","imgid":"6cc8477c-ad4e-4ef3-8b6d-271747ae000e,6a06def3-068c-4672-8961-61d478ee0f30,18ad28a1-5ebc-44cc-8650-5cf283767933","workdatemorningend":"","stadiumdesc":"河西三小是汉沽区一所普通国办小学，占地面积10998平方米，建筑面积2630.5平方米。","workDateAfternoonEnd":"21:30","busroute":"400路、457路、459路。","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"汉沽河西第三小学","vacationDateAfternoonStart":"","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027165849907hanguhexisanxiao.jpg","stadiumfaceid":"0c6ebcec-e522-4106-94af-83fbad8c6aa6","workdatemorningstart":"","status":"1"},{"distance":"62.3km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"7ea4b94f-a4e6-4a71-83ff-b92173eb9116","workDateAfternoonStart":"18:00","periphery":"滨海新区汉沽法治文化园","lat":"39.255021","connectcode":" 022-25696412","createtime":"2019-10-27 17:19:15 289","address":"天津市汉沽区府北路","lng":"117.812092","imgid":"355c658e-2fd7-43e7-8745-f4fe2a983bbc,88d67ab0-8d77-4a96-91b9-9fd15b3c1e32,da3c84e9-3182-4696-b1f9-d7841691f7df","workdatemorningend":"","stadiumdesc":"占地面积24227平方米，建筑面积12400平方米。现有教学班38个，在校生1500余人。","workDateAfternoonEnd":"21:30","busroute":"400路、401路、402路、403路、452路、456路、459路。 ","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"汉沽中心小学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027171719164hanguzhongxinxiaoxue1.jpg","stadiumfaceid":"37380516-7e7c-4d07-8c2c-54792f88a535","workdatemorningstart":"","status":"1"},{"distance":"62.7km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"41c78905-ace5-4f61-963f-c9282bb33e5e","workDateAfternoonStart":"18:00","periphery":"汉沽三幼","lat":"39.243832","connectcode":" 022-67114398","createtime":"2019-10-27 16:57:32 303","address":"天津市滨海新区汉沽太平东街9号","lng":"117.825235","imgid":"d1515107-72f7-4f46-a507-f7f4a434fcb9,3a2553c8-14d2-4e53-9e44-6462c020532e,7e327965-753f-4bea-bed6-ae3c76dfc0e5","workdatemorningend":"","stadiumdesc":"天津市滨海新区汉沽第九中学，位于天津市滨海新区汉沽太平东街9号，前身是创建于50年代末的寨上中学，学校占地面积达16000平方米，其中建筑面积4850平方米。","workDateAfternoonEnd":"21:30","busroute":"402路、453路、460路、939路。 ","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"汉沽第九中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027165524193hangudijiuzhongxue.jpeg","stadiumfaceid":"ca3cc5dc-4e10-4d2f-a67c-7562a32026af","workdatemorningstart":"","status":"1"},{"distance":"64.9km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"d639b33e-565b-4080-8cf6-9c4ac71442e0","workDateAfternoonStart":"18:00","periphery":"葆芳苑小区，公安汉沽支队。","lat":"39.242397","connectcode":"022-25690290","createtime":"2019-10-27 16:53:05 979","address":"天津市滨海新区五纬路60号","lng":"117.797093","imgid":"3cd3b5ab-8380-4422-a17f-b094196ed21f,b580009c-c294-4ac5-b0f1-894c43eb3f16,4a6dbd65-45f9-408e-9b08-57042543b666","workdatemorningend":"","stadiumdesc":"天津市汉沽区第三中学建于1981年，位于汉沽区河西二经路，是一所全日制初级中学。","workDateAfternoonEnd":"21:30","busroute":"403路、458路、461路、455路、400路、通勤快56路。","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"汉沽第三中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/201910271652022470191027165132.jpg","stadiumfaceid":"7440894e-c203-4096-a583-2bfcf51227ac","workdatemorningstart":"","status":"1"},{"distance":"90.7km","vacationDateMorningStart":"","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"5ec1d3d8-3abc-473d-bc34-6099b0709f2a","workDateAfternoonStart":"18:00","periphery":"公厕，塘沽九幼，花鸟鱼虫市场。","lat":"39.047635","connectcode":"022-66309073","createtime":"2019-10-27 17:42:49 818","address":"天津市滨海新区杭州道桂林路2号","lng":"117.653094","imgid":"995a19c4-4c63-4562-970f-8f72abd66547,f3ed43fd-fc7d-424e-9412-f13ca489ff3a,94335357-1ed1-43b8-9e27-791d3b6ebad9","workdatemorningend":"","stadiumdesc":"位于杭州道桂林路2号，拥有9700平方米的塑胶操场","workDateAfternoonEnd":"21:30","busroute":"518路、104路、825路、517路、502路、937路、109路.","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"桂林路小学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027174158115guilinluxiaoxue1.jpg","stadiumfaceid":"6cf3aa83-497c-43b6-94ca-e195106ce1b3","workdatemorningstart":"","status":"1"},{"distance":"91.2km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"1e4dc050-f9d0-4f55-8b5b-f4a59e536b3f","workDateAfternoonStart":"18:00","periphery":"塘沽十四中，塘沽第四幼儿园，中心北路菜市场","lat":"39.046446","connectcode":"022-66305216","createtime":"2019-10-27 18:05:25 300","address":"天津市滨海新区杭州道街道广州道61号","lng":"117.661506","imgid":"b92d0f15-5ba3-4abe-a7c4-9f50edfd1b37,ba4e57d1-504b-48df-b026-3bad1e717683,4fdc99be-87fa-46fe-baef-d8144bc4b20c","workdatemorningend":"","stadiumdesc":"学校在文化立校上利用中国传统\"四君子\"文化的竹文化作为师生文化形象，师生共同学习\"谦虚、向上、有理、有节\"的竹之精神，师生共同颂竹、画竹、赏竹。学校校园文化氛围布置也以此为依托彰显特色;学校在教育教学方面以网络德育、网络社区、数字化校园以及信息技术与学科教学整合的研讨为契合点做大做强特色教育。","workDateAfternoonEnd":"21:30","busroute":"109路、502路、517路、821路、834路、844路.","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"塘沽广州道小学","vacationDateAfternoonStart":"","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027180327096tangguguangzhoudaoxiaoxue1.jpg","stadiumfaceid":"4698ea47-4eea-4755-9fbf-676878ad77c4","workdatemorningstart":"","status":"1"},{"distance":"91.2km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"fec84fe5-073e-4b95-ad00-b3810c0ed706","workDateAfternoonStart":"18:00","periphery":"瑞金堂大药房、中心北路菜市场","lat":"39.04482","connectcode":" 022-25817755","createtime":"2019-10-27 17:56:45 705","address":"天津市塘沽区杭州道街广州道５１号","lng":"117.663959","imgid":"312d3bcb-5a57-46b9-b27e-03cecec53294,fa877b26-fd52-41cb-9fc0-64aa3275b5ab,5d06a9a5-8e24-4f5e-bc21-a6f47650a749","workdatemorningend":"","stadiumdesc":"学校建筑面积达19298平方米，建有教学楼、办公楼、综合实验楼，拥有9152平方米的塑胶操场。学校校园环境优美，人文气息浓郁，教学设施一流。经过二十多年的拼搏与发展，学校逐步形成了自己鲜明的办学特色，积累了丰富的办学经验，历年中考成绩斐然，赢得了社会的广泛赞誉。","workDateAfternoonEnd":"21:30","busroute":"108路、821路、834路、844路、880路、109路、821路","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"塘沽十四中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027175431069tanggushisizhong.jpg","stadiumfaceid":"bf4cf1e0-ccbe-45c0-8a0d-f6c6c712c0aa","workdatemorningstart":"","status":"1"},{"distance":"93.7km","vacationDateMorningStart":"","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"57225ced-16e8-4b7a-b346-5ce1f181d057","workDateAfternoonStart":"18:00","periphery":"中新药业，塘沽街道社区服务中心，天津港公安局。","lat":"39.012617","connectcode":"022-25798124","createtime":"2019-10-27 17:26:56 789","address":"天津市滨海新区港航路","lng":"117.719779","imgid":"1d48c465-cf05-430d-b43b-7a341614a9be,6c302cd9-1734-4364-98ac-45681212f8d5,caa56ac9-a97f-41a5-86e3-a379390ec163","workdatemorningend":"","stadiumdesc":"学校占地面积约20000平方米，建筑面积约8000平方米包括图书馆、办公楼、实验楼、教学楼及一个占地12000平方米的新型塑胶运动场，从规模、硬件设置上达到了现代化学校的标准。在学校管理教育教学体育科研等方面取得市区级荣誉。","workDateAfternoonEnd":"21:30","busroute":"502路、937路、973路、816路。","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"新港中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027172511910xingangzhongxue1.jpg","stadiumfaceid":"e8b1f74d-a9f1-4cd3-87c2-7977a13bc7c5","workdatemorningstart":"","status":"1"},{"distance":"93.7km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"c7c7412a-b350-4f43-ba5f-ddeb2291e897","workDateAfternoonStart":"18:00","periphery":"肯德基，金街步行街，老百姓大药房。","lat":"39.021821","connectcode":"022-25863966","createtime":"2019-10-27 17:30:53 625","address":"天津市滨海新区塘沽街烟台道3号","lng":"117.664799","imgid":",a19e3c3e-28ab-4be0-9eda-fca7404eea58,17169219-f6d4-44cf-9798-92b57abf6d49,6b76aedd-ae2b-4a82-96a7-1473040488e3","workdatemorningend":"","stadiumdesc":"学校占地5.4万平方米，建筑面积3万多平方米。学校现有64教学班，3000余学生，特级及中高级教师50余人。有各种实验室16个，专用劳技教室4个，先进的千兆以太校园网，装备计算机400余台，多媒体电化教室6个，信息技术教室6个，藏书10万余册;学校建有学生电视台、\"晨曦\"文学社等业余社团。2000年被评为市级规范示范校和市级电教实验校。优雅的学习环境，先进的教学设施，为学生成才创造了良好条件。","workDateAfternoonEnd":"21:30","busroute":"844路、109路、108路、834路、820路、101路、936路、937路、821路、825路、816路、833路、507路、107路、102路","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"塘沽第一中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027173909378tangguyizhong2.jpg","stadiumfaceid":"bbb73b11-fc17-4848-a766-dc4321e26be8","workdatemorningstart":"","status":"1"},{"distance":"121.0km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"张老师","id":"8c094ba3-c321-4d1e-ad23-54854c909c65","workDateAfternoonStart":"18:00","periphery":"中国联通、惠民村镇银行。","lat":"38.869944","connectcode":"02225993012","createtime":"2019-10-27 15:05:01 638","address":"位于天津市滨海新区吉林街道","lng":"117.491824","imgid":"be901575-5785-4e99-b232-e3b2599c7fb4,3d8f4078-d727-4cae-b8ce-fc2e4bacbf02,258f0532-e432-4661-96e0-0b17a982fc90","workdatemorningend":"","stadiumdesc":"位于天津市滨海新区吉林街道","workDateAfternoonEnd":"21:30","busroute":"523路北环、523路南环、524路、545路内环、545路外环；","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"大港第一中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027145919962dagangdiyi.jpg","stadiumfaceid":"bb976f0d-9db4-478c-8d2b-7abaef7145fa","workdatemorningstart":"","status":"1"},{"distance":"124.4km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"99c8ef4c-f264-48a9-9da6-99ab050069f3","workDateAfternoonStart":"18:00","periphery":"超市、美食、泰达蓝盾加油站","lat":"38.88882","connectcode":"022-63278155","createtime":"2019-10-27 15:52:43 668","address":"天津市滨海新区中港路北","lng":"117.40258","imgid":"85c4a97c-5299-4db1-9c17-8dd4eac8ed74,d0df1c13-71e6-48d9-9bef-9f3c518bdc65,de41a855-167e-4071-8e69-7b9095b6e99d","workdatemorningend":"","stadiumdesc":"学校占地68亩，建筑面积6638平方米，具有现代化教学设备条件.","workDateAfternoonEnd":"21:30","busroute":"161路、920路、946路、178路、548路区间线、549路、945路","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"大港第五小学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027154547960dagangdiwu.jpg","stadiumfaceid":"b9bb9271-d97e-48a4-a222-86e85751d169","workdatemorningstart":"","status":"1"},{"distance":"124.6km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"8258045a-b446-4cb8-b44c-a9e815d5be30","workDateAfternoonStart":"18:00","periphery":"大港湿地公园、华润万家.","lat":"38.865317","connectcode":"022-63388913","createtime":"2019-10-27 16:37:43 593","address":"天津市大港区喜荣街","lng":"117.450465","imgid":"a5fb60c2-d59a-47b9-af19-12af50ac398b,6b95794c-0d95-4ec8-ac53-bb6bbb25e497,02d1d5a4-e859-49ce-b484-0b59cc699f11","workdatemorningend":"","stadiumdesc":"大港七中坐落在天津市大港区喜荣街，(曾用命:天津石油化工公司第一子弟中学，也称石化一中)现拥有(截至2006年六月)高中年级，初中年级教学班级共40个，拥有雄厚的师资力量。","workDateAfternoonEnd":"21:30","busroute":"523路北环、523路南环、161路、659路","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"大港第七中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027163324860dagangdiqizhongxue1.jpg","stadiumfaceid":"3b3f396b-b5f7-4fb7-812b-8daf38d06059","status":"1"},{"distance":"125.2km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"7b65b3a7-b64f-4bd5-94db-c47288664fba","workDateAfternoonStart":"18:00","periphery":"大港国税局","lat":"38.849831","connectcode":"02263386615","createtime":"2019-10-27 16:16:10 156","address":"天津市大港区世纪大道191号","lng":"117.445095","imgid":"525efbfa-ace6-42fc-a1e6-734491deadc8,3dafb174-24ca-455d-b925-6654faaa320f,32bbd0be-0fb8-4dc5-b430-a7c3468ecb04","workdatemorningend":"","stadiumdesc":"大港实验中学2006年建校，于2006年底被评为区级重点高中。学校的地址即原市重点大港一中的校址，占地131亩，总建筑面积28000平方米。","workDateAfternoonEnd":"21:30","busroute":"161路、178路、547路、548路区间线、549路、659路、688路、920路、945路、946路。","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"大港实验中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027161337279dagangshiyanzhongxeu.jpg","stadiumfaceid":"dac2e050-dd8a-40d4-9a14-edb3223b8463","workdatemorningstart":"","status":"1"},{"distance":"131.1km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"2bd61240-d881-4e4f-9482-a6ade8d8ccbc","workDateAfternoonStart":"18:00","periphery":"大港二号院小学，油田一中。","lat":"38.744626","connectcode":"022-25926413","createtime":"2019-10-27 15:23:55 349","address":"天津市大港油田二号院","lng":"117.506344","imgid":"0ae19ecc-da6c-4374-9b43-c0d3f5011b52,5158b590-3d28-436f-8af0-2a0fd834513d,a3aaafd6-d096-4f50-9e69-e21fdc1e7119","workdatemorningend":"","stadiumdesc":"学校占地七万多平方米，建筑面积1.8万平米，绿化面积1.7万平米。学校有宽敞明亮的教学楼和实验楼，配备标准塑胶运动场、篮球运动场及封闭式教学体育馆。","workDateAfternoonEnd":"21:30","busroute":"541路、542路。","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"大港油田实验中学","vacationDateAfternoonStart":"18:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027152220655dagangyoutian3.jpg","stadiumfaceid":"6155ce44-1839-46be-9856-6c6c53a6c948","workdatemorningstart":"","status":"1"},{"distance":"131.2km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"963db587-6b7c-4a2e-890b-70f4906d07b8","workDateAfternoonStart":"18:00","periphery":"中国移动、晨光文具、海滨街广场居委会","lat":"38.748604","connectcode":"022-25912473","createtime":"2019-10-27 16:12:24 669","address":" 天津市滨海新区海滨希望路大港油田第四中学","lng":"117.503028","imgid":"d0cd31dc-df2d-4845-91af-e8dadfcdedf6,16d10a60-6e27-45a5-9f09-3611c6a6b2d1,17c4ffa9-2da6-40d2-b74c-79069481e9a0","workdatemorningend":"","stadiumdesc":"建筑面积13800平方米，学校拥有设施先进的教学楼、电教实验楼、艺术教学楼、报告厅、体育馆、田径及球类场地等。学校教学设备先进，教学环境优雅，办学特色鲜明，师资力量雄厚，教育成果丰硕，是全国绿色学校和全国现代教育技术实验学校。","workDateAfternoonEnd":"21:30","busroute":"541路、542路、949路内环、949路外环.","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"天津市大港区油田第四中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027160754437daagngyotiansizhong1.jpg","stadiumfaceid":"b6c6365f-6944-4aa7-9a63-9d5e6ad1cd42","workdatemorningstart":"","status":"1"},{"distance":"133.9km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"8214e483-9f10-47b6-8b6b-b5980bb55b66","workDateAfternoonStart":"18:00","periphery":"大港南城中心公园","lat":"38.727711","connectcode":"022-63963075","createtime":"2019-10-27 16:32:25 636","address":"天津市滨海新区油田港盛道","lng":"117.485057","imgid":"396a9691-1dfb-46e1-a317-b9de31b9faed,afde8b27-71d2-4b4e-a180-e430d4d825d6,589ef572-e20f-4084-a937-374278371107","workdatemorningend":"","stadiumdesc":"学校地处油田新建的同盛小区，环境优美，文化气息浓厚。占地面积4.77万平方米，建筑面积7800平方米。有教职工51人，教学班19个，学生644人。","workDateAfternoonEnd":"21:30","busroute":"542路、949路、688路、943路、543路、541路。","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"大港同盛学校","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027162755208dagangtongshengxuexiao.jpg","stadiumfaceid":"ff7c59b0-fbe9-488c-ba81-babd85bc7a15","workdatemorningstart":"","status":"1"},{"distance":"142.2km","vacationDateMorningStart":"","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"8d999d0b-2e76-47a3-8688-7a6115a729bc","workDateAfternoonStart":"18:00","periphery":"超市、饺子馆","lat":"38.727186","connectcode":"022-63199651","createtime":"2019-10-27 16:00:50 489","address":"天津市滨海新区海滨街远景一村东","lng":"117.451698","imgid":"efaae118-ac93-43e1-a5fc-19218883ac1d,e3ded903-dcf6-4bf5-8612-87cf0c849f3b,96ec7d04-5c11-487e-916c-bd31347cff4e","workdatemorningend":"","stadiumdesc":"远景中学是由三个自然村组成的九年一贯制学校，前身是远景中学和远景联校，学校占地面积，现有教职工91人，在校学生八百余人，各类办公、教学、实验、微机、文体等设施器材配备齐全。","workDateAfternoonEnd":"21:30","busroute":"927路、943路","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"大港远景中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027155911959dagangyuanjing2.jpg","stadiumfaceid":"410384ac-ea66-449e-be99-903991d49437","workdatemorningstart":"","status":"1"},{"distance":"149.1km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"58ff853d-f710-4178-9406-90d7defdb924","workDateAfternoonStart":"18:00","periphery":"太平镇配出所","lat":"38.649921","connectcode":"022-63148107","createtime":"2019-10-27 15:43:32 229","address":"位于天津市大港区太平镇北环路","lng":"117.3556","imgid":"7019bd4e-7ab6-4c1d-a715-6212e57be1bb,d60a0e3a-d734-4efe-819c-f3617a193ad6,2262e90b-09c5-43d7-a694-45a0ad5128ab","workdatemorningend":"","stadiumdesc":"学校坐落于太平镇北环路上，南北，东西长度各为200米，占地40000 m2。教学楼2栋，配有大型多功能教室、食堂、计算机房、各学科实验室等功能室，校园内绿树成荫，草木青青，环境优雅。","workDateAfternoonEnd":"21:30","busroute":"923路、925路、924路、926路。","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"大港太平村第二中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027154110542dagangtaiupingcun1.png","stadiumfaceid":"1a4d3df3-d084-4bee-a6d1-e90190d5a33b","workdatemorningstart":"","status":"1"},{"distance":"151.9km","vacationDateMorningStart":"06:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","score":0,"managername":"wxy","id":"74bc86e9-6c6c-4a67-9496-ce1ebe15f5a2","workDateAfternoonStart":"18:00","periphery":"世纪华联超市、小王庄派出所、渡口医院","lat":"38.75388","connectcode":"022-63129035","createtime":"2019-10-27 16:42:42 502","address":"天津市大港区小王庄镇205国道西侧","lng":"117.165383","imgid":"29ff47da-9e66-419e-b779-c0324ec5b05b,0ec7a319-2437-429c-a619-de617a1ef930,04f5885b-c2be-4043-b16e-65dfd96ecb9c","workdatemorningend":"","stadiumdesc":"","workDateAfternoonEnd":"21:30","busroute":"548路、945路、184路、946路","vacationDateAfternoonEnd":"21:30","vacationDateMorningEnd":"12:00","name":"大港小王庄中学","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191027163853734dagangxiaowangzhuangzhongxue.jpg","stadiumfaceid":"d1bfbad2-2964-45d8-bf5e-80b917a50096","workdatemorningstart":"","status":"1"}]
     * code : 200
     * size : 20
     */

    private String msg;
    private String code;
    private int size;
    private List<ResBean> res;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<ResBean> getRes() {
        return res;
    }

    public void setRes(List<ResBean> res) {
        this.res = res;
    }

    public static class ResBean {
        /**
         * distance : 60.8km
         * vacationDateMorningStart : 06:00
         * typecode : 1
         * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9e
         * score : 0
         * managername : wxy
         * id : 5947c30d-d4ec-4525-8110-6f8ce7e29e37
         * workDateAfternoonStart : 18:00
         * periphery : 汉沽三幼
         * lat : 39.25983
         * connectcode : 022-67193903
         * createtime : 2019-10-27 17:14:31 963
         * address : 天津市滨海新区友谊街与建设南路交口东100米路南。
         * lng : 117.827251
         * imgid : 56f7b1e2-9960-4c51-b343-10f9c17dc928,c11e33f0-b493-4584-b01d-76f996eed62c,2347617a-0d45-44e3-a5cf-a7a35706c5f2,31ee4813-3ea4-43e2-ad85-082dd42d41fc
         * workdatemorningend :
         * stadiumdesc : 学校领导一班人，积极探索特色办学的思路和途径，注重校外资源的借鉴与校内资源的开发。在低年级开设了珠脑心算实验课题;语文、英语学科实施了小学生能力开发课题实验，两项课题均得到市教科院的关注，并建立我区首例实验基地，此课题具有广泛性和前沿性，构建了符合、尊重儿童个性发展规律，倡导并实施:"主动参与，探索发现，交流合作"的学习方式，使学生从小学会交往，学会学习，学会思考，学会创新，成为智商情商和谐发展的个性主体。
         * workDateAfternoonEnd : 21:30
         * busroute : 400路、404路。
         * vacationDateAfternoonEnd : 21:30
         * vacationDateMorningEnd : 12:00
         * name : 汉沽东海小学
         * vacationDateAfternoonStart : 12:00
         * isdelete : 0
         * stadiumfacepath : http://103.233.6.43:8009/XDfileserver/20191027170357189hangudonghaixiaoxue.jpg
         * stadiumfaceid : 94cb7e4c-cdaf-48a6-85e1-7889d8a69970
         * workdatemorningstart :
         * status : 1
         */

        private String distance;
        private String vacationDateMorningStart;
        private String typecode;
        private String uid;
        private float score;
        private String managername;
        private String id;
        private String workDateAfternoonStart;
        private String periphery;
        private String lat;
        private String connectcode;
        private String createtime;
        private String address;
        private String lng;
        private String imgid;
        private String workdatemorningend;
        private String stadiumdesc;
        private String workDateAfternoonEnd;
        private String busroute;
        private String vacationDateAfternoonEnd;
        private String vacationDateMorningEnd;
        private String name;
        private String vacationDateAfternoonStart;
        private String isdelete;
        private String stadiumfacepath;
        private String stadiumfaceid;
        private String workdatemorningstart;
        private String status;

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getVacationDateMorningStart() {
            return vacationDateMorningStart;
        }

        public void setVacationDateMorningStart(String vacationDateMorningStart) {
            this.vacationDateMorningStart = vacationDateMorningStart;
        }

        public String getTypecode() {
            return typecode;
        }

        public void setTypecode(String typecode) {
            this.typecode = typecode;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public float getScore() {
            return score;
        }

        public void setScore(float score) {
            this.score = score;
        }

        public String getManagername() {
            return managername;
        }

        public void setManagername(String managername) {
            this.managername = managername;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getWorkDateAfternoonStart() {
            return workDateAfternoonStart;
        }

        public void setWorkDateAfternoonStart(String workDateAfternoonStart) {
            this.workDateAfternoonStart = workDateAfternoonStart;
        }

        public String getPeriphery() {
            return periphery;
        }

        public void setPeriphery(String periphery) {
            this.periphery = periphery;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getConnectcode() {
            return connectcode;
        }

        public void setConnectcode(String connectcode) {
            this.connectcode = connectcode;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getImgid() {
            return imgid;
        }

        public void setImgid(String imgid) {
            this.imgid = imgid;
        }

        public String getWorkdatemorningend() {
            return workdatemorningend;
        }

        public void setWorkdatemorningend(String workdatemorningend) {
            this.workdatemorningend = workdatemorningend;
        }

        public String getStadiumdesc() {
            return stadiumdesc;
        }

        public void setStadiumdesc(String stadiumdesc) {
            this.stadiumdesc = stadiumdesc;
        }

        public String getWorkDateAfternoonEnd() {
            return workDateAfternoonEnd;
        }

        public void setWorkDateAfternoonEnd(String workDateAfternoonEnd) {
            this.workDateAfternoonEnd = workDateAfternoonEnd;
        }

        public String getBusroute() {
            return busroute;
        }

        public void setBusroute(String busroute) {
            this.busroute = busroute;
        }

        public String getVacationDateAfternoonEnd() {
            return vacationDateAfternoonEnd;
        }

        public void setVacationDateAfternoonEnd(String vacationDateAfternoonEnd) {
            this.vacationDateAfternoonEnd = vacationDateAfternoonEnd;
        }

        public String getVacationDateMorningEnd() {
            return vacationDateMorningEnd;
        }

        public void setVacationDateMorningEnd(String vacationDateMorningEnd) {
            this.vacationDateMorningEnd = vacationDateMorningEnd;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVacationDateAfternoonStart() {
            return vacationDateAfternoonStart;
        }

        public void setVacationDateAfternoonStart(String vacationDateAfternoonStart) {
            this.vacationDateAfternoonStart = vacationDateAfternoonStart;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getStadiumfacepath() {
            return stadiumfacepath;
        }

        public void setStadiumfacepath(String stadiumfacepath) {
            this.stadiumfacepath = stadiumfacepath;
        }

        public String getStadiumfaceid() {
            return stadiumfaceid;
        }

        public void setStadiumfaceid(String stadiumfaceid) {
            this.stadiumfaceid = stadiumfaceid;
        }

        public String getWorkdatemorningstart() {
            return workdatemorningstart;
        }

        public void setWorkdatemorningstart(String workdatemorningstart) {
            this.workdatemorningstart = workdatemorningstart;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
