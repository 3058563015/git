package com.soomax.pojo;

import java.util.List;

public class FinderPojo {


    /**
     * msg : 操作成功
     * res : [{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9e","descs":"2019年滨海新区机关干部乒乓球比赛落下帷幕","createtime":"2019-10-26 19:50:03","isrecommend":"1","ctype":"3","imgid":"5bb0e8cc-4026-4f15-bdf1-f74e9a9b43a6","imgpath":"http://103.233.6.43:8009/XDfileserver/201910261948167191-01.jpg","id":"1b846fbf-7167-454e-85e0-cc52f5bddc7f","isdelete":"0","title":"2019年滨海新区机关干部乒乓球比赛落下帷幕","content":"http://103.233.6.43:8009/XDfileserver/1b846fbf-7167-454e-85e0-cc52f5bddc7f.html","status":"1"}]
     * code : 200
     * size : 1
     */

    private String msg;
    private String code;
    private int size;
    private List<ResBean> res;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<ResBean> getRes() {
        return res;
    }

    public void setRes(List<ResBean> res) {
        this.res = res;
    }

    public static class ResBean {
        /**
         * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9e
         * descs : 2019年滨海新区机关干部乒乓球比赛落下帷幕
         * createtime : 2019-10-26 19:50:03
         * isrecommend : 1
         * ctype : 3
         * imgid : 5bb0e8cc-4026-4f15-bdf1-f74e9a9b43a6
         * imgpath : http://103.233.6.43:8009/XDfileserver/201910261948167191-01.jpg
         * id : 1b846fbf-7167-454e-85e0-cc52f5bddc7f
         * isdelete : 0
         * title : 2019年滨海新区机关干部乒乓球比赛落下帷幕
         * content : http://103.233.6.43:8009/XDfileserver/1b846fbf-7167-454e-85e0-cc52f5bddc7f.html
         * status : 1
         */

        private String uid;
        private String descs;
        private String createtime;
        private String isrecommend;
        private String ctype;
        private String imgid;
        private String imgpath;
        private String id;
        private String isdelete;
        private String title;
        private String content;
        private String status;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getDescs() {
            return descs;
        }

        public void setDescs(String descs) {
            this.descs = descs;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getIsrecommend() {
            return isrecommend;
        }

        public void setIsrecommend(String isrecommend) {
            this.isrecommend = isrecommend;
        }

        public String getCtype() {
            return ctype;
        }

        public void setCtype(String ctype) {
            this.ctype = ctype;
        }

        public String getImgid() {
            return imgid;
        }

        public void setImgid(String imgid) {
            this.imgid = imgid;
        }

        public String getImgpath() {
            return imgpath;
        }

        public void setImgpath(String imgpath) {
            this.imgpath = imgpath;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
