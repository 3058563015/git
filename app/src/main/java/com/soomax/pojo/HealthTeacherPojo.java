package com.soomax.pojo;

import java.util.List;

public class HealthTeacherPojo {


    /**
     * msg : 操作成功
     * res : [{"headimgid":"5bf6cf06-5904-4214-b3aa-fc7419fc5e97","createtime":"2019-10-27 19:16:41 506","address":"天津市滨海新区","lng":"117.687368","sportname":"舞蹈","imglist":"5cfeeb5b-f90b-41c5-ab4a-3b791cfaf7a0,7886639d-42b1-40ba-8fe4-f14a61c2295f,ebe84539-8bb0-426e-8dd0-2666c7908f25","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"12","descs":"从小对舞蹈感兴趣，擅长肚皮舞。教学风格独特，因材施教，深受学员喜欢。\n2017年，受邀参加华东首届东方舞艺术节；\n2018年，受邀参加丝路东方全球东方舞大赛；\n2018年，受邀参加第二届王维国际东方舞艺术节，荣获国际权威带队机构奖；团队包揽少儿团体组亚军，业余团体组亚军，个人明日之星，全国十佳等奖项；\n2019年，受邀参加繁美之星东方舞艺术大赛；\n2019年，受邀受邀参加华北第四届国际东方舞大赛，担任专业大赛评委；荣获权威带队机构奖；团队包揽少儿团体组季军，幼儿融合组季军，业余民俗组全国十强、金奖，半专业pop song组全国十强，半专业融合组冠军等奖项。\n","phone":"16622135566","name":"谭晓明","id":"b73c79d0-a583-4cf4-98bc-08cc4a3dd82e","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191027191420836txm.png","lat":"39.012813","status":"1"},{"headimgid":"058b0b3d-cf03-4b8a-8ef8-54b8a14a32c8","createtime":"2019-10-27 19:03:56 245","address":"天津市滨海新区","lng":"117.669148","sportname":"健身","imglist":"155bd753-7879-482f-9a7a-b8663c219ada,160c3a06-f839-4471-afe9-48ddde5cc928,24a6c822-cc2e-49fc-915f-dd88e2c27a48,125202b6-2cad-4e44-a804-8d578f7be31c,1a3d9e16-68a0-4cd0-beec-2b73a15e5b5d,4ade6642-f4c9-46ae-84d1-022b845e6d05","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"21","descs":"毕业于北京费恩莱斯国际健身学院\nIFBB国际运动营养师\nIFBB国际私人教练证书\nIBFA悬挂系统认证\nIFBB运动康复认证\n个人擅长:增肌，减脂，塑型，运动康复\n授课节数:5000+\n运动格言:没有干不成的事，就怕你不够努力，磕！\n","phone":"15122111361","name":"张加伟","id":"70e0f0ab-bf88-462d-aada-443846155d49","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191027190216312zhangjiawei.jpg","lat":"39.062801","status":"1"},{"headimgid":"a539954a-7265-4026-8285-c0e563757d39","createtime":"2019-10-27 18:32:28 549","address":"天津市滨海新区","lng":"117.66913","sportname":"健身","imglist":"155bd753-7879-482f-9a7a-b8663c219ada,160c3a06-f839-4471-afe9-48ddde5cc928,24a6c822-cc2e-49fc-915f-dd88e2c27a48","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"21","descs":"荣誉资质：毕业于费恩莱斯国际健身学院\nIFBB国际运动营养师\nIFBB国际私人教练\nIBFA抗阻力训练师\nIBFA产后恢复\n个人擅长：经典抗阻力训练    体态康复     运动损伤恢复    拉伸康复\n授课节数：3000+\n运动格言：多一分运动，少一分病痛\n","phone":"17601462557","name":"王英旭","id":"7fdf1cbd-7fa8-4eb0-b90e-6fe0250d7b98","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191027183006189wangyingxu.jpg","lat":"39.062843","status":"1"},{"headimgid":"d0b07386-281c-46f7-8982-d3d2e569b9fe","createtime":"2019-10-27 17:31:08 132","address":"天津市滨海新区泰丰路","lng":"117.728683","sportname":"击剑","imglist":"82f17763-5771-4915-8186-cbe659f7a74a,1657092f-6ec5-400e-af9c-ece3b7bb8262,53b1a73e-50b4-4d98-a7f6-828fae427a2b","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"24","descs":"中国击剑协会会员\n中国击剑协会教练员\n中国击剑协会裁判员\n","phone":"18722433688","name":"毕鑫","id":"85f03b8d-f246-4079-89d0-de8305f67596","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191027172930822jitou.png","lat":"39.063785","status":"1"},{"headimgid":"dd132090-bd8e-4337-81b8-32e11f96e537","createtime":"2019-10-27 17:28:56 807","address":"天津市滨海新区","lng":"117.687368","sportname":"舞蹈","imglist":"aaa8de57-27c9-471f-aa97-7d7dc6c0d5de,80fa40af-94da-43ae-8156-046a18c9eec5,a0cfba3b-be6f-4f21-8965-3dee66d79466","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"12","descs":"成人 少儿 东方舞金牌导师\n2012年与东方舞结缘 不断的学习提升 多次参加国内外大师集训\n擅长东方舞入门基础. 从浅入深的教学.让学员学习了解不同风格的东方舞！深受喜爱！\n东方舞是一种全身运动的舞蹈.通过骨盆.臀部.胸部和手臂旋转以及胯部动作，对人体有很好的减肥塑形.矫正不良姿态.提高心肺功能.调节内分泌和增强自信心等。\n","phone":"13011304321","name":"石岩","id":"a4b62920-73d0-4cbf-8f0c-4485232fd94f","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/2019102717212055927-2.jpg","lat":"39.012813","status":"1"},{"headimgid":"5a8eee57-edb3-4b01-b9da-3e74be07f811","createtime":"2019-10-27 16:35:19 310","address":"滨海新区五大街延长线万科海港城三期5号楼2401","lng":"117.748351","sportname":"瑜伽","imglist":"05a8f78f-6e7c-43b6-a6ab-1e5e15475e05,c19c62cf-47fb-49db-99b1-704450e9879b,5991b848-efcd-415d-8ea4-529c2876dec2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"4","descs":"瑜伽教学六年，系统学习哈他瑜伽，流瑜伽，阴瑜伽、理疗瑜伽，阿斯汤伽瑜伽等，取得全美联盟RYT200小时认证培训证书、取得 美国最权威老师jennifer孕产全美RPYT85小时认证培训证书。\n教学经验丰富，因材施教，擅长各种形体塑形与体态调整瑜伽，各类理疗瑜伽小班课程和孕产私教。\n","phone":"15522517164","name":"轩轩","id":"5be44aa7-0a3c-43be-94c0-b8759dbec283","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191027163343091xuanxuan.jpg","lat":"39.040361","status":"1"},{"headimgid":"1431a35c-673f-4076-8f94-08c385949079","createtime":"2019-10-27 16:32:53 073","address":"滨海新区五大街延长线万科海港城三期5号楼2401","lng":"117.748337","sportname":"瑜伽","imglist":"5cb6d130-e4d9-4944-88b7-5acf96486c53,ba2dd3a3-4bee-4f4e-ae39-ef7338dce647,34ee7264-6cac-4792-b91b-5fbaec6ed1af,1514488d-1c28-418c-a505-f750f82b6cbb","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"4","descs":"2012年接触并习练瑜伽，2014年系统学习哈他教培，2016至2018年一直跟随董娟老师学习流瑜伽及疗愈瑜伽。,\n2018年系统进修了董娟老师的200小时全是瑜教师培训，学习了各类基础体式的正位、辅助调整手法、体式解剖学原理及瑜伽体式的专注点等，瑜伽教学3年，教学风格细腻，可教授哈他瑜伽，流瑜伽，阴瑜伽、理疗瑜伽。\n","phone":"13034354577","name":"心怡","id":"4168b47d-aeb4-45a7-8dfe-02ae9d8d8b66","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/2019102716240090811.jpg","lat":"39.040329","status":"1"},{"headimgid":"7b7bfeae-786f-46c0-a094-459e2c23e935","createtime":"2019-10-27 16:22:40 084","address":"天津市滨海新区万科海港城三期5号楼2401","lng":"117.748337","sportname":"瑜伽","imglist":"5d2f8517-e80b-4fb6-b74d-b3707946c6fe,ecbfd08b-27bb-4b4c-ad35-8dce3ae2db3b,eaf476d5-f2eb-408a-9b38-d12689c3d32c","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"4","descs":"毕业于吉林体育大学体育教育专业，精熟运动解剖，运动康复，健身私教等，  2008年获得《中国健美协会国家一级健身指导员 证书》，并成为一名专职瑜伽教练至今，教学经验丰富，私教课程积累已达5000节。\n跟随王向东、赵扬、Sherri等多位名师学习，总结出自己的特色教学内容，现今主教哈他理疗瑜伽，精准正位，流瑜伽。\n","phone":"15522517164 ","name":"momo","id":"02440f45-2323-4eed-a916-8e3c8631d808","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191027161946961momo2.jpg","lat":"39.040427","status":"1"},{"headimgid":"f3395262-bcaa-4328-8fa9-66b26d289e2c","createtime":"2019-10-27 16:09:06 290","address":"天津市滨海新区","lng":"117.687386","sportname":"舞蹈","imglist":"918f716c-d7e2-479d-9456-a793c82908cb,f9460907-7dd7-4f5f-9f2e-565ae16bbd56,40ca8af7-ffa7-47c9-9ca3-cecd47e67ba1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"12","descs":"毕业于江西理工大学体育专业\n曾获得全国大众健美操冠军，啦啦操冠军\n擅长各类团操（有氧操、踏板操、蹦床、舞蹈等）\n","phone":"15522848288","name":"李亮","id":"31d31492-cfd5-45ff-bbc1-c1fc47bfa0e1","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/2019102716001208512.jpg","lat":"39.012792","status":"1"},{"headimgid":"265d8880-34ba-499e-b30e-70402a09518d","createtime":"2019-10-27 15:59:51 799","address":"天津市滨海新区","lng":"117.687365","sportname":"瑜伽","imglist":"fc3ce5da-7ba4-4b11-a713-e0018a7d8d8b,dfff09d7-cc8e-43c5-bd2d-28908a11ad55,9ee73add-922d-4f95-a257-ca73f6d95c70,31495c37-031b-4bf8-8e8e-467783cc9456,227b349f-861b-4a12-9ade-859a43fb89ac","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"4","descs":"2014年开始接触并学习瑜伽\n获RYT美国瑜伽联盟教师资格证书\n进修并学习杨格（正位瑜伽）、反重力瑜伽、流瑜伽、阴瑜伽、理疗课程、\n女性保养、孕产及产后修复，瘦身塑形及马甲线课程\n柔美、细腻的教学风格至今一直从事瑜伽，有丰富的课上教学经验，可以让习练者更加感受体验式与呼吸的完美结合。\n","phone":"13652178884","name":"Maggie","id":"e13218b5-f7e1-443b-b17c-098c45ec67b6","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/2019102715492480827-1.jpg","lat":"39.012826","status":"1"},{"headimgid":"29fb34fa-9474-4d51-839a-089833df8d75","createtime":"2019-10-27 15:34:42 853","address":"天津市滨海新区河北路4862号","lng":"117.66886","sportname":"乒乓球","imglist":"5a8fbe7d-b356-4d11-bb2e-be2d55fceb22,25aac514-f49a-4f9c-aa80-28c8a6253eb4,c37f3ed0-1252-4060-a2ad-fe86c1a32719","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9h","sportid":"7","descs":"国家高级教练员","phone":"15822585959","name":"董晓明","id":"1961ebbf-7aef-4aba-9491-cee74cb5c2ef","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191027153026839dong1.jpg","lat":"39.061872","status":"1"}]
     * code : 200
     * size : 11
     */

    private String msg;
    private String code;
    private int size;
    private List<ResBean> res;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<ResBean> getRes() {
        return res;
    }

    public void setRes(List<ResBean> res) {
        this.res = res;
    }

    public static class ResBean {
        /**
         * headimgid : 5bf6cf06-5904-4214-b3aa-fc7419fc5e97
         * createtime : 2019-10-27 19:16:41 506
         * address : 天津市滨海新区
         * lng : 117.687368
         * sportname : 舞蹈
         * imglist : 5cfeeb5b-f90b-41c5-ab4a-3b791cfaf7a0,7886639d-42b1-40ba-8fe4-f14a61c2295f,ebe84539-8bb0-426e-8dd0-2666c7908f25
         * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9h
         * sportid : 12
         * descs : 从小对舞蹈感兴趣，擅长肚皮舞。教学风格独特，因材施教，深受学员喜欢。
         2017年，受邀参加华东首届东方舞艺术节；
         2018年，受邀参加丝路东方全球东方舞大赛；
         2018年，受邀参加第二届王维国际东方舞艺术节，荣获国际权威带队机构奖；团队包揽少儿团体组亚军，业余团体组亚军，个人明日之星，全国十佳等奖项；
         2019年，受邀参加繁美之星东方舞艺术大赛；
         2019年，受邀受邀参加华北第四届国际东方舞大赛，担任专业大赛评委；荣获权威带队机构奖；团队包揽少儿团体组季军，幼儿融合组季军，业余民俗组全国十强、金奖，半专业pop song组全国十强，半专业融合组冠军等奖项。
         * phone : 16622135566
         * name : 谭晓明
         * id : b73c79d0-a583-4cf4-98bc-08cc4a3dd82e
         * isdelete : 0
         * headimgpath : http://103.233.6.43:8009/XDfileserver/20191027191420836txm.png
         * lat : 39.012813
         * status : 1
         */

        private String headimgid;
        private String createtime;
        private String address;
        private String lng;
        private String sportname;
        private String imglist;
        private String uid;
        private String sportid;
        private String descs;
        private String phone;
        private String name;
        private String id;
        private String isdelete;
        private String headimgpath;
        private String lat;
        private String status;

        public String getHeadimgid() {
            return headimgid;
        }

        public void setHeadimgid(String headimgid) {
            this.headimgid = headimgid;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getSportname() {
            return sportname;
        }

        public void setSportname(String sportname) {
            this.sportname = sportname;
        }

        public String getImglist() {
            return imglist;
        }

        public void setImglist(String imglist) {
            this.imglist = imglist;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getSportid() {
            return sportid;
        }

        public void setSportid(String sportid) {
            this.sportid = sportid;
        }

        public String getDescs() {
            return descs;
        }

        public void setDescs(String descs) {
            this.descs = descs;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getHeadimgpath() {
            return headimgpath;
        }

        public void setHeadimgpath(String headimgpath) {
            this.headimgpath = headimgpath;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
