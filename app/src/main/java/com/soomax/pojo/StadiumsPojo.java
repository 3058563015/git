package com.soomax.pojo;

import java.util.List;

public class StadiumsPojo {


    /**
     * msg : 操作成功
     * res : [{"distance":"87.5km","vacationDateMorningStart":"","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"managername":"管理员","id":"cc83e5fb-a798-4b39-b53e-25070fb1707b","workDateAfternoonStart":"","periphery":"周边设施","lat":"39.061795","connectcode":"13000000000","createtime":"2019-10-23 11:24:19 424","address":"天津市滨海新区第七大街辅路","lng":"117.705519","imgid":"4f23ddf0-8d9c-4901-8b59-4016cf8ddbe9,ffe7f794-0bac-4989-8f0e-ecffc4763b6c,006fb270-2df5-4138-bf6b-67b3ecac3511","workdatemorningend":"","stadiumdesc":"测试场馆","workDateAfternoonEnd":"","vacationDateAfternoonEnd":"","vacationDateMorningEnd":"","name":"测试场馆222","vacationDateAfternoonStart":"","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/2019102311225622353cdd1f7c1f21.jpg","stadiumfaceid":"ea0746d0-06bc-4a64-aec0-1d561551fce2","workdatemorningstart":"","status":"1"},{"distance":"89.3km","vacationDateMorningStart":"2019-07-26 08:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"881FCE5A-44B0-4733-E342-08D7116E15E0","workDateAfternoonStart":"2019-07-26 12:00:00.0000000","lat":"39.0547030","connectcode":"13000000000","createtime":"2019-07-26 10:36:51.7211125","address":"塘沽区工农村新北路港顺物流汽配城院内","lng":"117.6712720","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-26 12:00:00.0000000","workDateAfternoonEnd":"2019-07-26 23:00:00.0000000","vacationDateAfternoonEnd":"2019-07-26 23:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-26 12:00:00.0000000","name":"足球","vacationDateAfternoonStart":"2019-07-26 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190726/10/45/4/feeb0eb9e6cfbb3afa3053ce75298deb.jpg","stadiumfaceid":"57dd8b39-e32b-48db-9836-b4187b676ce5","workdatemorningstart":"2019-07-26 08:00:00.0000000","status":"1"},{"distance":"89.4km","vacationDateMorningStart":"2019-07-12 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"A9942756-C2CA-4392-44C1-08D70692DC2C","workDateAfternoonStart":"2019-07-12 12:00:00.0000000","lat":"39.0570000","connectcode":"13000000000","createtime":"2019-07-12 14:33:32.5163107","address":"天津市塘沽区金江路335号","lng":"117.6650000","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-12 12:00:00.0000000","workDateAfternoonEnd":"2019-07-12 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-12 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-12 12:00:00.0000000","name":"胜利足球馆","vacationDateAfternoonStart":"2019-07-12 22:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190712/13/28/4/05b672540f84b5e7457f54d4fca98381.jpg","stadiumfaceid":"61b09c00-b6e1-477d-bef1-caf2ae54ee49","workdatemorningstart":"2019-07-12 09:00:00.0000000","status":"1"},{"distance":"89.4km","vacationDateMorningStart":"2019-07-19 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"E8AA14FF-9CEB-4605-0944-08D70BEB9A57","workDateAfternoonStart":"2019-07-19 12:00:00.0000000","lat":"39.0558100","connectcode":"13000000000","createtime":"2019-07-19 10:28:09.0473368","address":"天津市滨海新区宝山道688号3层3307号","lng":"117.6671290","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-19 12:00:00.0000000","workDateAfternoonEnd":"2019-07-19 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-19 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-19 12:00:00.0000000","name":"金典羽毛球馆","vacationDateAfternoonStart":"2019-07-19 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/10/25/4/b06e32857b3c23ad6225cf8d01fdb211.jpg","stadiumfaceid":"a878a169-43d3-4dcd-a05e-ffe04b430c4d","workdatemorningstart":"2019-07-19 09:00:00.0000000","status":"1"},{"distance":"89.6km","vacationDateMorningStart":"2019-07-17 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"F44C0859-8EBA-475D-2460-08D70A62E9BC","workDateAfternoonStart":"2019-07-17 12:00:00.0000000","lat":"39.0569230","connectcode":"13000000000","createtime":"2019-07-17 11:19:58.0657857","address":"天津市滨海新区河北路4862号贻成集团院内","lng":"117.6617560","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-17 12:00:00.0000000","workDateAfternoonEnd":"2019-07-17 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-17 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-17 12:00:00.0000000","name":"乐萌游泳健身会馆","vacationDateAfternoonStart":"2019-07-17 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190717/11/16/4/8e674919bd6d33e85280e8700a5ce6d9.jpg","stadiumfaceid":"871228f5-c2c6-42c1-8080-fbabed3ae834","workdatemorningstart":"2019-07-17 09:00:00.0000000","status":"1"},{"distance":"89.9km","vacationDateMorningStart":"2019-07-15 10:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"916FF8E5-5EB9-4BE0-C6C2-08D708E947DE","workDateAfternoonStart":"2019-07-15 12:00:00.0000000","lat":"39.0425460","connectcode":"13000000000","createtime":"2019-07-15 14:03:09.2919861","address":"天津塘沽开发区第四大街鸿发工业园内","lng":"117.6976220","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-15 12:00:00.0000000","workDateAfternoonEnd":"2019-07-15 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-15 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-15 12:00:00.0000000","name":"TBA篮球馆","vacationDateAfternoonStart":"2019-07-15 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/16/05/4/ea973b877bc2d70fa13f0ee475375600.jpg","stadiumfaceid":"1b2589b2-df82-4ac3-960c-b8eedb68d4ad","workdatemorningstart":"2019-07-15 10:00:00.0000000","status":"1"},{"distance":"90.0km","vacationDateMorningStart":"2019-07-17 05:30:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":5,"id":"63D42F7D-BB75-46E8-A8CD-08D70A8E0C4A","workDateAfternoonStart":"2019-07-17 12:00:00.0000000","lat":"39.0421990","connectcode":"13000000000","createtime":"2019-07-17 16:22:27.6759677","address":"天津滨海开发区五大街与南海路交口南200米路西","lng":"117.7046150","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-17 12:00:00.0000000","workDateAfternoonEnd":"2019-07-17 23:00:00.0000000","vacationDateAfternoonEnd":"2019-07-17 23:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-17 12:00:00.0000000","name":"昊客健身俱乐部","vacationDateAfternoonStart":"2019-07-17 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190717/16/26/4/070634cb9c160de983f86569f327377b.jpg","stadiumfaceid":"2ffd10ed-7980-4a95-893c-ca4cb53194c5","workdatemorningstart":"2019-07-17 05:30:00.0000000","status":"1"},{"distance":"90.0km","vacationDateMorningStart":"2019-07-19 00:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"9A9DACBB-89D1-454F-0940-08D70BEB9A57","workDateAfternoonStart":"2019-07-19 12:00:00.0000000","lat":"39.0426210","connectcode":"13000000000","createtime":"2019-07-19 09:56:29.8837106","address":"天津滨海开发区南海路81号天翼羽毛球馆","lng":"117.7047400","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-19 12:00:00.0000000","workDateAfternoonEnd":"2019-07-19 23:59:59.0000000","vacationDateAfternoonEnd":"2019-07-19 23:59:59.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-19 12:00:00.0000000","name":"B &A 健身训场","vacationDateAfternoonStart":"2019-07-19 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/09/53/4/1ede5881ec2e0d6292f14d3c804bbf08.jpg","stadiumfaceid":"b194840c-3ef6-4666-9dfd-c17e3dda8a3c","workdatemorningstart":"2019-07-19 00:00:00.0000000","status":"1"},{"distance":"90.0km","vacationDateMorningStart":"2019-07-17 06:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"DE0B35C3-5EFD-428E-2461-08D70A62E9BC","workDateAfternoonStart":"2019-07-17 12:00:00.0000000","lat":"39.0387390","connectcode":"13000000000","createtime":"2019-07-17 11:59:11.2778502","address":"天津市滨海新区第五大街80号泰丰会馆(距泰丰家园北门公交站步行290米)","lng":"117.7085810","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-17 12:00:00.0000000","workDateAfternoonEnd":"2019-07-17 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-17 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-17 12:00:00.0000000","name":"泰丰27℃游泳俱乐部","vacationDateAfternoonStart":"2019-07-17 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190717/11/35/4/1ba593cfed6ea77de4adec19eeebdec8.jpg","stadiumfaceid":"5af2a3c7-5727-4e20-a02e-7bd5308daa7a","workdatemorningstart":"2019-07-17 06:00:00.0000000","status":"1"},{"distance":"90.0km","vacationDateMorningStart":"2019-07-17 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"EED9A8F8-B26F-4406-A8CC-08D70A8E0C4A","workDateAfternoonStart":"2019-07-17 12:00:00.0000000","lat":"39.0426210","connectcode":"13000000000","createtime":"2019-07-17 16:18:09.0827784","address":"天津滨海新区第五大街与南海路交口","lng":"117.7047400","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-17 12:00:00.0000000","workDateAfternoonEnd":"2019-07-17 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-17 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-17 12:00:00.0000000","name":"天翼羽毛球馆","vacationDateAfternoonStart":"2019-07-17 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/16/06/4/728f4e43667aef4fbe138c90c0c61129.png","stadiumfaceid":"7f0333f5-6f84-474d-9d71-336496e69e92","workdatemorningstart":"2019-07-17 09:00:00.0000000","status":"1"},{"distance":"90.8km","vacationDateMorningStart":"2019-07-19 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"709DE838-34D5-4383-093F-08D70BEB9A57","workDateAfternoonStart":"2019-07-19 12:00:00.0000000","lat":"39.0346590","connectcode":"13000000000","createtime":"2019-07-19 09:51:23.0863762","address":"天津滨海开发区第三大街泰达中心酒店D座6楼","lng":"117.6910210","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-19 12:00:00.0000000","workDateAfternoonEnd":"2019-07-19 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-19 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-19 12:00:00.0000000","name":"潮庭健身","vacationDateAfternoonStart":"2019-07-19 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/16/10/4/04ff78d921d6b9312c77afeac3eea118.jpg","stadiumfaceid":"d65317f3-d5d9-4b21-b213-92497a41bfd1","workdatemorningstart":"2019-07-19 09:00:00.0000000","status":"1"},{"distance":"91.0km","vacationDateMorningStart":"2019-07-19 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"65EC5546-2CED-4E74-E630-08D70C1676D3","workDateAfternoonStart":"0001-01-01 00:00:00.0000000","lat":"39.0369510","connectcode":"13000000000","createtime":"2019-07-19 16:14:11.8750557","address":"天津滨海新区鹍鹏街9号康隆苑正门","lng":"117.6948160","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-07-19 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-19 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-19 12:00:00.0000000","name":" DO   yoga 瑜伽舞蹈会馆","vacationDateAfternoonStart":"2019-07-19 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/16/11/4/8fe3871e937d3a7bd9cacef3e900e28f.jpg","stadiumfaceid":"8221fe3c-aa26-46e7-88c6-38ab2cd8e50b","workdatemorningstart":"2019-07-19 09:00:00.0000000","status":"1"},{"distance":"91.3km","vacationDateMorningStart":"2019-07-19 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"CB9CCC3A-DF67-480A-E62F-08D70C1676D3","workDateAfternoonStart":"0001-01-01 00:00:00.0000000","lat":"39.0411680","connectcode":"13000000000","createtime":"2019-07-19 15:46:49.7670630","address":"天津市滨海新区塘沽福州道贻成福地广场西区负一层","lng":"117.6688160","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-07-19 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-19 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-19 12:00:00.0000000","name":"祥云乒乓球馆","vacationDateAfternoonStart":"2019-07-19 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/15/43/4/98eb1e8bc6c6c22eaf0720aba8bb327d.png","stadiumfaceid":"4f688fd0-6244-4ffb-8ef0-772fe52058aa","workdatemorningstart":"2019-07-19 09:00:00.0000000","status":"1"},{"distance":"91.6km","vacationDateMorningStart":"2019-07-26 08:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"EEA24442-7FCA-4473-E341-08D7116E15E0","workDateAfternoonStart":"0001-01-01 00:00:00.0000000","lat":"39.0295920","connectcode":"13000000000","createtime":"2019-07-26 10:08:00.8998928","address":"天津市滨海新区奥运路11号泰达时尚健身休闲广场5号馆(近迪卡侬)","lng":"117.7221760","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-07-26 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-26 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-26 12:00:00.0000000","name":"泰达水世界","vacationDateAfternoonStart":"2019-07-26 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190726/10/04/4/9378791f22abcf45355afc31c07f3a9f.jpg","stadiumfaceid":"1a230167-ac38-4771-a2db-94a08525447c","workdatemorningstart":"2019-07-26 08:00:00.0000000","status":"1"},{"distance":"91.6km","vacationDateMorningStart":"2019-07-17 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"F9D0F440-B804-43C1-245F-08D70A62E9BC","workDateAfternoonStart":"1900-01-01 00:00:00.0000000","lat":"39.0316030","connectcode":"13000000000","createtime":"2019-07-17 11:00:24.0743116","address":"天津市滨海新区奥运路 迪卡侬运动超市楼上","lng":"117.7204620","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"1900-01-01 00:00:00.0000000","workDateAfternoonEnd":"1900-01-01 00:00:00.0000000","vacationDateAfternoonEnd":"2019-07-17 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-17 12:00:00.0000000","name":"伯明顿羽毛球馆","vacationDateAfternoonStart":"2019-07-17 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/16/02/4/dfa49275810a965042643031916a41b3.jpg","stadiumfaceid":"f61a1ffc-a306-4703-bf3c-a7209d8b681b","workdatemorningstart":"1900-01-01 00:00:00.0000000","status":"1"},{"distance":"91.9km","vacationDateMorningStart":"2019-07-19 10:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"837DD18A-AC08-4E91-0943-08D70BEB9A57","workDateAfternoonStart":"2019-07-19 12:00:00.0000000","lat":"39.0247970","connectcode":"13000000000","createtime":"2019-07-19 10:12:38.5872524","address":"天津市滨海新区二大街MSD1984-G2座-3楼","lng":"117.7107560","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-19 12:00:00.0000000","workDateAfternoonEnd":"2019-07-19 20:30:00.0000000","vacationDateAfternoonEnd":"2019-07-19 20:30:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-19 12:00:00.0000000","name":"舞润瑜伽生活馆","vacationDateAfternoonStart":"2019-07-19 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/10/05/4/cae97e3045d2b7cf8eaa97c2baa812d1.jpg","stadiumfaceid":"8a2ed82d-2151-4097-93b9-6bd0c4c65bf4","workdatemorningstart":"2019-07-19 10:00:00.0000000","status":"1"},{"distance":"94.0km","vacationDateMorningStart":"","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"managername":"管理员","id":"ad8521cf-bcca-492f-9604-e92baac91073","workDateAfternoonStart":"","periphery":"周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施周边设施","lat":"39.014014","connectcode":"13000000000","createtime":"2019-10-22 18:16:01 054","address":"天津市滨海新区三号路2593号","lng":"117.71179","imgid":"411ef45f-212e-48b2-8903-fbc16677fe07,caa22464-e868-4b44-9c59-44f09c68ae2f,d8aa804a-0669-4595-82c9-13a7d30903ab","workdatemorningend":"","stadiumdesc":"场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情场地详情","workDateAfternoonEnd":"","vacationDateAfternoonEnd":"","vacationDateMorningEnd":"","name":"测试场馆1111","vacationDateAfternoonStart":"","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/2019102218153306359bbc4288d107.jpg","stadiumfaceid":"a81209df-1d5c-4c5b-bea2-606c015872fe","workdatemorningstart":"","status":"1"},{"distance":"94.1km","vacationDateMorningStart":"2019-07-26 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"06ABEF7A-60BF-474F-E344-08D7116E15E0","workDateAfternoonStart":"0001-01-01 00:00:00.0000000","lat":"39.0218900","connectcode":"13000000000","createtime":"2019-07-26 11:25:35.9159453","address":"天津市滨海新区营口道1041号附近","lng":"117.6506970","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-07-26 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-26 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-26 12:00:00.0000000","name":"天津市塘沽区排球馆","vacationDateAfternoonStart":"2019-07-26 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190726/10/46/4/5fe2cef778a9761d03d520ec5762cd5b.jpg","stadiumfaceid":"e4de50e0-ac2f-428e-9839-f42a28db1c78","workdatemorningstart":"2019-07-26 09:00:00.0000000","status":"1"},{"distance":"95.0km","vacationDateMorningStart":"2019-07-17 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"12631841-129D-4202-A8CB-08D70A8E0C4A","workDateAfternoonStart":"2019-07-17 12:00:00.0000000","lat":"39.0174490","connectcode":"13000000000","createtime":"2019-07-17 16:09:10.4104282","address":"天津市滨海新区解放路507号","lng":"117.6682890","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-17 12:00:00.0000000","workDateAfternoonEnd":"2019-07-17 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-17 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-17 12:00:00.0000000","name":"天津金菲舞蹈培训","vacationDateAfternoonStart":"2019-07-17 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/16/09/4/f988fccaa117a903b686239dd981a969.jpg","stadiumfaceid":"93c834e4-32f0-41e4-a07a-3141bf58267a","workdatemorningstart":"2019-07-17 09:00:00.0000000","status":"1"},{"distance":"124.6km","vacationDateMorningStart":"2019-07-26 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"id":"2CCCD9D0-7F27-4A3D-6737-08D7118A84C3","workDateAfternoonStart":"0001-01-01 00:00:00.0000000","lat":"39.1101440","connectcode":"13000000000","createtime":"2019-07-26 14:09:01.6744017","address":"天津南开区天津大学、天津体育学院、网球中心","lng":"117.1703830","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-07-26 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-26 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-26 12:00:00.0000000","name":"天津星期八网球俱乐部","vacationDateAfternoonStart":"2019-07-26 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190726/13/57/4/ece3c26242acc3be1748d34072d2643d.jpg","stadiumfaceid":"75f6419e-27da-4cd9-a0eb-c68b8975c5f2","workdatemorningstart":"2019-07-26 09:00:00.0000000","status":"1"}]
     * code : 200
     * size : 20
     */

    private String msg;
    private String code;
    private int size;
    private List<ResBean> res;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<ResBean> getRes() {
        return res;
    }

    public void setRes(List<ResBean> res) {
        this.res = res;
    }

    public static class ResBean {
        /**
         * distance : 87.5km
         * vacationDateMorningStart :
         * typecode : 2
         * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9F
         * score : 0
         * managername : 管理员
         * id : cc83e5fb-a798-4b39-b53e-25070fb1707b
         * workDateAfternoonStart :
         * periphery : 周边设施
         * lat : 39.061795
         * connectcode : 13000000000
         * createtime : 2019-10-23 11:24:19 424
         * address : 天津市滨海新区第七大街辅路
         * lng : 117.705519
         * imgid : 4f23ddf0-8d9c-4901-8b59-4016cf8ddbe9,ffe7f794-0bac-4989-8f0e-ecffc4763b6c,006fb270-2df5-4138-bf6b-67b3ecac3511
         * workdatemorningend :
         * stadiumdesc : 测试场馆
         * workDateAfternoonEnd :
         * vacationDateAfternoonEnd :
         * vacationDateMorningEnd :
         * name : 测试场馆222
         * vacationDateAfternoonStart :
         * isdelete : 0
         * stadiumfacepath : http://103.233.6.43:8009/XDfileserver/2019102311225622353cdd1f7c1f21.jpg
         * stadiumfaceid : ea0746d0-06bc-4a64-aec0-1d561551fce2
         * workdatemorningstart :
         * status : 1
         * descs : 学校/场馆简介
         */

        private String distance;
        private String vacationDateMorningStart;
        private String typecode;
        private String uid;
        private int score;
        private String managername;
        private String id;
        private String workDateAfternoonStart;
        private String periphery;
        private String lat;
        private String connectcode;
        private String createtime;
        private String address;
        private String lng;
        private String imgid;
        private String workdatemorningend;
        private String stadiumdesc;
        private String workDateAfternoonEnd;
        private String vacationDateAfternoonEnd;
        private String vacationDateMorningEnd;
        private String name;
        private String vacationDateAfternoonStart;
        private String isdelete;
        private String stadiumfacepath;
        private String stadiumfaceid;
        private String workdatemorningstart;
        private String status;
        private String descs;

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getVacationDateMorningStart() {
            return vacationDateMorningStart;
        }

        public void setVacationDateMorningStart(String vacationDateMorningStart) {
            this.vacationDateMorningStart = vacationDateMorningStart;
        }

        public String getTypecode() {
            return typecode;
        }

        public void setTypecode(String typecode) {
            this.typecode = typecode;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public String getManagername() {
            return managername;
        }

        public void setManagername(String managername) {
            this.managername = managername;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getWorkDateAfternoonStart() {
            return workDateAfternoonStart;
        }

        public void setWorkDateAfternoonStart(String workDateAfternoonStart) {
            this.workDateAfternoonStart = workDateAfternoonStart;
        }

        public String getPeriphery() {
            return periphery;
        }

        public void setPeriphery(String periphery) {
            this.periphery = periphery;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getConnectcode() {
            return connectcode;
        }

        public void setConnectcode(String connectcode) {
            this.connectcode = connectcode;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getImgid() {
            return imgid;
        }

        public void setImgid(String imgid) {
            this.imgid = imgid;
        }

        public String getWorkdatemorningend() {
            return workdatemorningend;
        }

        public void setWorkdatemorningend(String workdatemorningend) {
            this.workdatemorningend = workdatemorningend;
        }

        public String getStadiumdesc() {
            return stadiumdesc;
        }

        public void setStadiumdesc(String stadiumdesc) {
            this.stadiumdesc = stadiumdesc;
        }

        public String getWorkDateAfternoonEnd() {
            return workDateAfternoonEnd;
        }

        public void setWorkDateAfternoonEnd(String workDateAfternoonEnd) {
            this.workDateAfternoonEnd = workDateAfternoonEnd;
        }

        public String getVacationDateAfternoonEnd() {
            return vacationDateAfternoonEnd;
        }

        public void setVacationDateAfternoonEnd(String vacationDateAfternoonEnd) {
            this.vacationDateAfternoonEnd = vacationDateAfternoonEnd;
        }

        public String getVacationDateMorningEnd() {
            return vacationDateMorningEnd;
        }

        public void setVacationDateMorningEnd(String vacationDateMorningEnd) {
            this.vacationDateMorningEnd = vacationDateMorningEnd;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVacationDateAfternoonStart() {
            return vacationDateAfternoonStart;
        }

        public void setVacationDateAfternoonStart(String vacationDateAfternoonStart) {
            this.vacationDateAfternoonStart = vacationDateAfternoonStart;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getStadiumfacepath() {
            return stadiumfacepath;
        }

        public void setStadiumfacepath(String stadiumfacepath) {
            this.stadiumfacepath = stadiumfacepath;
        }

        public String getStadiumfaceid() {
            return stadiumfaceid;
        }

        public void setStadiumfaceid(String stadiumfaceid) {
            this.stadiumfaceid = stadiumfaceid;
        }

        public String getWorkdatemorningstart() {
            return workdatemorningstart;
        }

        public void setWorkdatemorningstart(String workdatemorningstart) {
            this.workdatemorningstart = workdatemorningstart;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDescs() {
            return descs;
        }

        public void setDescs(String descs) {
            this.descs = descs;
        }
    }
}
