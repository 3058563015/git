package com.soomax.pojo;

import java.util.List;

public class HomeInfoPojo {


    /**
     * msg : 操作成功
     * res : {"recommendlist":[{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","descs":"这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻","createtime":"2019-10-22 17:34:05 308","isrecommend":"1","ctype":"3","imgid":"d8a9b669-677b-4e1e-962a-016452fbdaf1","imgpath":"http://103.233.6.43:8009/XDfileserver/20191022173205896Img314235964.jpg","id":"4c27b5bb-3e93-4609-9e94-b86f90722104","isdelete":"0","title":"这是一条测试新闻","content":"http://sports.sina.com.cn/g/laliga/2019-10-22/doc-iicezzrr3962888.shtml","status":"1"},{"descs":"这是一条新闻测试数据","createtime":"2019-10-08 12:00:00","isrecommend":"1","ctype":"1","imgid":"1ce237da-3bae-4f95-a154-7d6c8b0b632d","imgpath":"http://103.233.6.43:8009/XDfileserver/2019101518120036253cdd1f7c1f21.jpg","id":"C743412F-03A2-4DBC-59A2-08D6D46A61EA","isdelete":"0","title":"测试新闻数据2","content":"http://sports.sina.com.cn/g/laliga/2019-10-21/doc-iicezzrr3636016.shtml","status":"1"},{"descs":"这是一条新闻测试数据","createtime":"2019-10-09 12:00:00","isrecommend":"1","ctype":"2","imgid":"915897a4-50b3-445c-84b2-12a6b4984872","imgpath":"http://103.233.6.43:8009/XDfileserver/20191015181222923904373dc6d992bcec8bf1d6e564bcac3de060340.jpg","id":"C743412F-03A2-4DBC-59A2-08D6D46A61Eb","isdelete":"0","title":"测试新闻数据","content":"http://sports.sina.com.cn/g/laliga/2019-10-21/doc-iicezuev3635571.shtml","status":"1"}],"noticenum":0,"sportteacherlist":[{"headimgid":"8fff470f-32d3-445a-a2a9-8437f1555add","sportid":"1","name":"测试1","id":"1","sort":"2","isdelete":"0","sportname":"篮球","headimgpath":"http://103.233.6.43:8009/XDfileserver/2019101518144560820141020224133_Ur54c.jpeg","status":"1"},{"headimgid":"805a465c-4501-418c-9841-2aaa4500d623","sportid":"2","name":"测试2","id":"2","sort":"1","isdelete":"0","sportname":"足球","headimgpath":"http://103.233.6.43:8009/XDfileserver/2019101518150170920170205222154_WLdJS.jpeg","status":"1"}],"schoollist":[{"distance":".0km","vacationDateMorningStart":"2019-07-11 05:00:00.0000000","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","popularity":0,"id":"029598C1-B2CD-4775-3331-08D6ED629A4B","workDateAfternoonStart":"2019-07-11 18:00:00.0000000","lat":"38.8470000","connectcode":"13000000000","createtime":"2019-06-10 13:15:07.1788362","address":"天津市滨海新区世纪大道191号","lng":"117.4440000","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-06-13 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-11 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-11 12:00:00.0000000","name":"大港实验中学","vacationDateAfternoonStart":"2019-07-11 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190606/13/37/4/10.png","stadiumfaceid":"a60d95f7-8830-4b05-a0f7-a19ed5163234","workdatemorningstart":"0001-01-01 00:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"2019-07-11 05:00:00.0000000","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","popularity":0,"id":"03367557-9033-4E09-688B-08D6EE175616","workDateAfternoonStart":"2019-07-11 18:00:00.0000000","lat":"39.0720000","connectcode":"13000000000","createtime":"2019-06-11 11:06:31.5315496","address":"塘沽海洋高新区厦门路与云山道交口东北侧","lng":"117.6580000","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-06-13 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-11 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-11 12:00:00.0000000","name":"塘沽云山道学校","vacationDateAfternoonStart":"2019-07-11 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190611/11/03/4/25.png","stadiumfaceid":"6a3f34b2-d53d-4872-888a-1e88299ca8ac","workdatemorningstart":"0001-01-01 00:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"2019-07-11 05:00:00.0000000","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","popularity":1,"id":"13AA9B29-DCBA-41F1-688E-08D6EE175616","workDateAfternoonStart":"2019-07-11 18:00:00.0000000","lat":"39.0530000","connectcode":"13000000000","createtime":"2019-06-11 11:20:06.1231885","address":"天津市滨海新区杭州道街道贻成奥林花园西北角","lng":"117.6590000","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-06-13 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-11 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-11 12:00:00.0000000","name":"塘沽浙江路小学（福州道校区）","vacationDateAfternoonStart":"2019-07-11 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190611/11/19/4/999.jpg","stadiumfaceid":"db74b5ba-0241-4f9d-830a-f49ea03bf061","workdatemorningstart":"0001-01-01 00:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"00:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":10,"popularity":4,"managername":"王","id":"1619e793-01e0-4e1a-9b0f-9a69e3d695a8","workDateAfternoonStart":"12:10","periphery":"无","lat":"39.011659","connectcode":"13000000000","createtime":"2019-10-16 16:49:14 930","address":"天津市滨海新区","lng":"117.711322","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"00:30","stadiumdesc":"就这样吧","workDateAfternoonEnd":"12:40","vacationDateAfternoonEnd":"12:20","vacationDateMorningEnd":"00:40","name":"测试学校1","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191016164802021Img314235964.jpg","stadiumfaceid":"cadb3e50-97dd-4a72-b7ec-a0747d75f289","workdatemorningstart":"00:10","status":"1"}],"stadiumlist":[{"distance":".0km","vacationDateMorningStart":"2019-07-26 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"popularity":0,"id":"06ABEF7A-60BF-474F-E344-08D7116E15E0","workDateAfternoonStart":"0001-01-01 00:00:00.0000000","lat":"39.0218900","connectcode":"13000000000","createtime":"2019-07-26 11:25:35.9159453","address":"天津市滨海新区营口道1041号附近","lng":"117.6506970","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-07-26 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-26 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-26 12:00:00.0000000","name":"天津市塘沽区排球馆","vacationDateAfternoonStart":"2019-07-26 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190726/10/46/4/5fe2cef778a9761d03d520ec5762cd5b.jpg","stadiumfaceid":"e4de50e0-ac2f-428e-9839-f42a28db1c78","workdatemorningstart":"2019-07-26 09:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"2019-07-17 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"popularity":0,"id":"12631841-129D-4202-A8CB-08D70A8E0C4A","workDateAfternoonStart":"2019-07-17 12:00:00.0000000","lat":"39.0174490","connectcode":"13000000000","createtime":"2019-07-17 16:09:10.4104282","address":"天津市滨海新区解放路507号","lng":"117.6682890","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-17 12:00:00.0000000","workDateAfternoonEnd":"2019-07-17 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-17 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-17 12:00:00.0000000","name":"天津金菲舞蹈培训","vacationDateAfternoonStart":"2019-07-17 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/16/09/4/f988fccaa117a903b686239dd981a969.jpg","stadiumfaceid":"93c834e4-32f0-41e4-a07a-3141bf58267a","workdatemorningstart":"2019-07-17 09:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"2019-07-26 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"popularity":0,"id":"2CCCD9D0-7F27-4A3D-6737-08D7118A84C3","workDateAfternoonStart":"0001-01-01 00:00:00.0000000","lat":"39.1101440","connectcode":"13000000000","createtime":"2019-07-26 14:09:01.6744017","address":"天津南开区天津大学、天津体育学院、网球中心","lng":"117.1703830","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-07-26 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-26 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-26 12:00:00.0000000","name":"天津星期八网球俱乐部","vacationDateAfternoonStart":"2019-07-26 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190726/13/57/4/ece3c26242acc3be1748d34072d2643d.jpg","stadiumfaceid":"75f6419e-27da-4cd9-a0eb-c68b8975c5f2","workdatemorningstart":"2019-07-26 09:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"2019-07-17 05:30:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"popularity":0,"id":"63D42F7D-BB75-46E8-A8CD-08D70A8E0C4A","workDateAfternoonStart":"2019-07-17 12:00:00.0000000","lat":"39.0421990","connectcode":"13000000000","createtime":"2019-07-17 16:22:27.6759677","address":"天津滨海开发区五大街与南海路交口南200米路西","lng":"117.7046150","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-17 12:00:00.0000000","workDateAfternoonEnd":"2019-07-17 23:00:00.0000000","vacationDateAfternoonEnd":"2019-07-17 23:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-17 12:00:00.0000000","name":"昊客健身俱乐部","vacationDateAfternoonStart":"2019-07-17 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190717/16/26/4/070634cb9c160de983f86569f327377b.jpg","stadiumfaceid":"2ffd10ed-7980-4a95-893c-ca4cb53194c5","workdatemorningstart":"2019-07-17 05:30:00.0000000","status":"1"}],"bannerlist":[{"imgid":"87655864-ec51-4a16-a8e2-0730de4113b6","ctype":"1","imgpath":"http://103.233.6.43:8009/XDfileserver/201910161125518552131243qeq.png","contentid":"","id":"1","playtime":"3","isdelete":"0","sort":"3","status":"1"},{"imgid":"dc8357f1-c628-4355-ae67-cfab64b31083","ctype":"1","imgpath":"http://103.233.6.43:8009/XDfileserver/2019101609144702820191015173206300_20191015172715.png","id":"2","playtime":"3","isdelete":"0","sort":"2","status":"1"},{"imgid":"1648914e-3b77-475c-ba7f-2e0be6729b95","ctype":"1","imgpath":"http://103.233.6.43:8009/XDfileserver/2019101609151057820191015173243923_20191015172741.png","id":"3","playtime":"3","isdelete":"0","sort":"1","status":"1"}]}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * recommendlist : [{"uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","descs":"这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻","createtime":"2019-10-22 17:34:05 308","isrecommend":"1","ctype":"3","imgid":"d8a9b669-677b-4e1e-962a-016452fbdaf1","imgpath":"http://103.233.6.43:8009/XDfileserver/20191022173205896Img314235964.jpg","id":"4c27b5bb-3e93-4609-9e94-b86f90722104","isdelete":"0","title":"这是一条测试新闻","content":"http://sports.sina.com.cn/g/laliga/2019-10-22/doc-iicezzrr3962888.shtml","status":"1"},{"descs":"这是一条新闻测试数据","createtime":"2019-10-08 12:00:00","isrecommend":"1","ctype":"1","imgid":"1ce237da-3bae-4f95-a154-7d6c8b0b632d","imgpath":"http://103.233.6.43:8009/XDfileserver/2019101518120036253cdd1f7c1f21.jpg","id":"C743412F-03A2-4DBC-59A2-08D6D46A61EA","isdelete":"0","title":"测试新闻数据2","content":"http://sports.sina.com.cn/g/laliga/2019-10-21/doc-iicezzrr3636016.shtml","status":"1"},{"descs":"这是一条新闻测试数据","createtime":"2019-10-09 12:00:00","isrecommend":"1","ctype":"2","imgid":"915897a4-50b3-445c-84b2-12a6b4984872","imgpath":"http://103.233.6.43:8009/XDfileserver/20191015181222923904373dc6d992bcec8bf1d6e564bcac3de060340.jpg","id":"C743412F-03A2-4DBC-59A2-08D6D46A61Eb","isdelete":"0","title":"测试新闻数据","content":"http://sports.sina.com.cn/g/laliga/2019-10-21/doc-iicezuev3635571.shtml","status":"1"}]
         * noticenum : 0
         * sportteacherlist : [{"headimgid":"8fff470f-32d3-445a-a2a9-8437f1555add","sportid":"1","name":"测试1","id":"1","sort":"2","isdelete":"0","sportname":"篮球","headimgpath":"http://103.233.6.43:8009/XDfileserver/2019101518144560820141020224133_Ur54c.jpeg","status":"1"},{"headimgid":"805a465c-4501-418c-9841-2aaa4500d623","sportid":"2","name":"测试2","id":"2","sort":"1","isdelete":"0","sportname":"足球","headimgpath":"http://103.233.6.43:8009/XDfileserver/2019101518150170920170205222154_WLdJS.jpeg","status":"1"}]
         * schoollist : [{"distance":".0km","vacationDateMorningStart":"2019-07-11 05:00:00.0000000","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","popularity":0,"id":"029598C1-B2CD-4775-3331-08D6ED629A4B","workDateAfternoonStart":"2019-07-11 18:00:00.0000000","lat":"38.8470000","connectcode":"13000000000","createtime":"2019-06-10 13:15:07.1788362","address":"天津市滨海新区世纪大道191号","lng":"117.4440000","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-06-13 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-11 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-11 12:00:00.0000000","name":"大港实验中学","vacationDateAfternoonStart":"2019-07-11 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190606/13/37/4/10.png","stadiumfaceid":"a60d95f7-8830-4b05-a0f7-a19ed5163234","workdatemorningstart":"0001-01-01 00:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"2019-07-11 05:00:00.0000000","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","popularity":0,"id":"03367557-9033-4E09-688B-08D6EE175616","workDateAfternoonStart":"2019-07-11 18:00:00.0000000","lat":"39.0720000","connectcode":"13000000000","createtime":"2019-06-11 11:06:31.5315496","address":"塘沽海洋高新区厦门路与云山道交口东北侧","lng":"117.6580000","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-06-13 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-11 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-11 12:00:00.0000000","name":"塘沽云山道学校","vacationDateAfternoonStart":"2019-07-11 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190611/11/03/4/25.png","stadiumfaceid":"6a3f34b2-d53d-4872-888a-1e88299ca8ac","workdatemorningstart":"0001-01-01 00:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"2019-07-11 05:00:00.0000000","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","popularity":1,"id":"13AA9B29-DCBA-41F1-688E-08D6EE175616","workDateAfternoonStart":"2019-07-11 18:00:00.0000000","lat":"39.0530000","connectcode":"13000000000","createtime":"2019-06-11 11:20:06.1231885","address":"天津市滨海新区杭州道街道贻成奥林花园西北角","lng":"117.6590000","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-06-13 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-11 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-11 12:00:00.0000000","name":"塘沽浙江路小学（福州道校区）","vacationDateAfternoonStart":"2019-07-11 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190611/11/19/4/999.jpg","stadiumfaceid":"db74b5ba-0241-4f9d-830a-f49ea03bf061","workdatemorningstart":"0001-01-01 00:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"00:00","typecode":"1","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":10,"popularity":4,"managername":"王","id":"1619e793-01e0-4e1a-9b0f-9a69e3d695a8","workDateAfternoonStart":"12:10","periphery":"无","lat":"39.011659","connectcode":"13000000000","createtime":"2019-10-16 16:49:14 930","address":"天津市滨海新区","lng":"117.711322","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"00:30","stadiumdesc":"就这样吧","workDateAfternoonEnd":"12:40","vacationDateAfternoonEnd":"12:20","vacationDateMorningEnd":"00:40","name":"测试学校1","vacationDateAfternoonStart":"12:00","isdelete":"0","stadiumfacepath":"http://103.233.6.43:8009/XDfileserver/20191016164802021Img314235964.jpg","stadiumfaceid":"cadb3e50-97dd-4a72-b7ec-a0747d75f289","workdatemorningstart":"00:10","status":"1"}]
         * stadiumlist : [{"distance":".0km","vacationDateMorningStart":"2019-07-26 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"popularity":0,"id":"06ABEF7A-60BF-474F-E344-08D7116E15E0","workDateAfternoonStart":"0001-01-01 00:00:00.0000000","lat":"39.0218900","connectcode":"13000000000","createtime":"2019-07-26 11:25:35.9159453","address":"天津市滨海新区营口道1041号附近","lng":"117.6506970","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-07-26 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-26 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-26 12:00:00.0000000","name":"天津市塘沽区排球馆","vacationDateAfternoonStart":"2019-07-26 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190726/10/46/4/5fe2cef778a9761d03d520ec5762cd5b.jpg","stadiumfaceid":"e4de50e0-ac2f-428e-9839-f42a28db1c78","workdatemorningstart":"2019-07-26 09:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"2019-07-17 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"popularity":0,"id":"12631841-129D-4202-A8CB-08D70A8E0C4A","workDateAfternoonStart":"2019-07-17 12:00:00.0000000","lat":"39.0174490","connectcode":"13000000000","createtime":"2019-07-17 16:09:10.4104282","address":"天津市滨海新区解放路507号","lng":"117.6682890","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-17 12:00:00.0000000","workDateAfternoonEnd":"2019-07-17 21:00:00.0000000","vacationDateAfternoonEnd":"2019-07-17 21:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-17 12:00:00.0000000","name":"天津金菲舞蹈培训","vacationDateAfternoonStart":"2019-07-17 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190719/16/09/4/f988fccaa117a903b686239dd981a969.jpg","stadiumfaceid":"93c834e4-32f0-41e4-a07a-3141bf58267a","workdatemorningstart":"2019-07-17 09:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"2019-07-26 09:00:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"popularity":0,"id":"2CCCD9D0-7F27-4A3D-6737-08D7118A84C3","workDateAfternoonStart":"0001-01-01 00:00:00.0000000","lat":"39.1101440","connectcode":"13000000000","createtime":"2019-07-26 14:09:01.6744017","address":"天津南开区天津大学、天津体育学院、网球中心","lng":"117.1703830","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"0001-01-01 00:00:00.0000000","workDateAfternoonEnd":"2019-07-26 22:00:00.0000000","vacationDateAfternoonEnd":"2019-07-26 22:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-26 12:00:00.0000000","name":"天津星期八网球俱乐部","vacationDateAfternoonStart":"2019-07-26 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190726/13/57/4/ece3c26242acc3be1748d34072d2643d.jpg","stadiumfaceid":"75f6419e-27da-4cd9-a0eb-c68b8975c5f2","workdatemorningstart":"2019-07-26 09:00:00.0000000","status":"1"},{"distance":".0km","vacationDateMorningStart":"2019-07-17 05:30:00.0000000","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9F","score":0,"popularity":0,"id":"63D42F7D-BB75-46E8-A8CD-08D70A8E0C4A","workDateAfternoonStart":"2019-07-17 12:00:00.0000000","lat":"39.0421990","connectcode":"13000000000","createtime":"2019-07-17 16:22:27.6759677","address":"天津滨海开发区五大街与南海路交口南200米路西","lng":"117.7046150","imgid":"2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b","workdatemorningend":"2019-07-17 12:00:00.0000000","workDateAfternoonEnd":"2019-07-17 23:00:00.0000000","vacationDateAfternoonEnd":"2019-07-17 23:00:00.0000000","descs":"学校/场馆简介","vacationDateMorningEnd":"2019-07-17 12:00:00.0000000","name":"昊客健身俱乐部","vacationDateAfternoonStart":"2019-07-17 12:00:00.0000000","isdelete":"0","stadiumfacepath":"http://static.bhxdty.com/group1/avator/20190717/16/26/4/070634cb9c160de983f86569f327377b.jpg","stadiumfaceid":"2ffd10ed-7980-4a95-893c-ca4cb53194c5","workdatemorningstart":"2019-07-17 05:30:00.0000000","status":"1"}]
         * bannerlist : [{"imgid":"87655864-ec51-4a16-a8e2-0730de4113b6","ctype":"1","imgpath":"http://103.233.6.43:8009/XDfileserver/201910161125518552131243qeq.png","contentid":"","id":"1","playtime":"3","isdelete":"0","sort":"3","status":"1"},{"imgid":"dc8357f1-c628-4355-ae67-cfab64b31083","ctype":"1","imgpath":"http://103.233.6.43:8009/XDfileserver/2019101609144702820191015173206300_20191015172715.png","id":"2","playtime":"3","isdelete":"0","sort":"2","status":"1"},{"imgid":"1648914e-3b77-475c-ba7f-2e0be6729b95","ctype":"1","imgpath":"http://103.233.6.43:8009/XDfileserver/2019101609151057820191015173243923_20191015172741.png","id":"3","playtime":"3","isdelete":"0","sort":"1","status":"1"}]
         */

        private int noticenum;
        private List<RecommendlistBean> recommendlist;
        private List<SportteacherlistBean> sportteacherlist;
        private List<SchoollistBean> schoollist;
        private List<StadiumlistBean> stadiumlist;
        private List<BannerlistBean> bannerlist;

        public int getNoticenum() {
            return noticenum;
        }

        public void setNoticenum(int noticenum) {
            this.noticenum = noticenum;
        }

        public List<RecommendlistBean> getRecommendlist() {
            return recommendlist;
        }

        public void setRecommendlist(List<RecommendlistBean> recommendlist) {
            this.recommendlist = recommendlist;
        }

        public List<SportteacherlistBean> getSportteacherlist() {
            return sportteacherlist;
        }

        public void setSportteacherlist(List<SportteacherlistBean> sportteacherlist) {
            this.sportteacherlist = sportteacherlist;
        }

        public List<SchoollistBean> getSchoollist() {
            return schoollist;
        }

        public void setSchoollist(List<SchoollistBean> schoollist) {
            this.schoollist = schoollist;
        }

        public List<StadiumlistBean> getStadiumlist() {
            return stadiumlist;
        }

        public void setStadiumlist(List<StadiumlistBean> stadiumlist) {
            this.stadiumlist = stadiumlist;
        }

        public List<BannerlistBean> getBannerlist() {
            return bannerlist;
        }

        public void setBannerlist(List<BannerlistBean> bannerlist) {
            this.bannerlist = bannerlist;
        }

        public static class RecommendlistBean {
            /**
             * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9F
             * descs : 这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻这是一条测试新闻
             * createtime : 2019-10-22 17:34:05 308
             * isrecommend : 1
             * ctype : 3
             * imgid : d8a9b669-677b-4e1e-962a-016452fbdaf1
             * imgpath : http://103.233.6.43:8009/XDfileserver/20191022173205896Img314235964.jpg
             * id : 4c27b5bb-3e93-4609-9e94-b86f90722104
             * isdelete : 0
             * title : 这是一条测试新闻
             * content : http://sports.sina.com.cn/g/laliga/2019-10-22/doc-iicezzrr3962888.shtml
             * status : 1
             */

            private String uid;
            private String descs;
            private String createtime;
            private String isrecommend;
            private String ctype;
            private String imgid;
            private String imgpath;
            private String id;
            private String isdelete;
            private String title;
            private String content;
            private String status;

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getDescs() {
                return descs;
            }

            public void setDescs(String descs) {
                this.descs = descs;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getIsrecommend() {
                return isrecommend;
            }

            public void setIsrecommend(String isrecommend) {
                this.isrecommend = isrecommend;
            }

            public String getCtype() {
                return ctype;
            }

            public void setCtype(String ctype) {
                this.ctype = ctype;
            }

            public String getImgid() {
                return imgid;
            }

            public void setImgid(String imgid) {
                this.imgid = imgid;
            }

            public String getImgpath() {
                return imgpath;
            }

            public void setImgpath(String imgpath) {
                this.imgpath = imgpath;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

        public static class SportteacherlistBean {
            /**
             * headimgid : 8fff470f-32d3-445a-a2a9-8437f1555add
             * sportid : 1
             * name : 测试1
             * id : 1
             * sort : 2
             * isdelete : 0
             * sportname : 篮球
             * headimgpath : http://103.233.6.43:8009/XDfileserver/2019101518144560820141020224133_Ur54c.jpeg
             * status : 1
             */

            private String headimgid;
            private String sportid;
            private String name;
            private String id;
            private String sort;
            private String isdelete;
            private String sportname;
            private String headimgpath;
            private String status;

            public String getHeadimgid() {
                return headimgid;
            }

            public void setHeadimgid(String headimgid) {
                this.headimgid = headimgid;
            }

            public String getSportid() {
                return sportid;
            }

            public void setSportid(String sportid) {
                this.sportid = sportid;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getSort() {
                return sort;
            }

            public void setSort(String sort) {
                this.sort = sort;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getSportname() {
                return sportname;
            }

            public void setSportname(String sportname) {
                this.sportname = sportname;
            }

            public String getHeadimgpath() {
                return headimgpath;
            }

            public void setHeadimgpath(String headimgpath) {
                this.headimgpath = headimgpath;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

        public static class SchoollistBean {
            /**
             * distance : .0km
             * vacationDateMorningStart : 2019-07-11 05:00:00.0000000
             * typecode : 1
             * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9F
             * popularity : 0
             * id : 029598C1-B2CD-4775-3331-08D6ED629A4B
             * workDateAfternoonStart : 2019-07-11 18:00:00.0000000
             * lat : 38.8470000
             * connectcode : 13000000000
             * createtime : 2019-06-10 13:15:07.1788362
             * address : 天津市滨海新区世纪大道191号
             * lng : 117.4440000
             * imgid : 2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b
             * workdatemorningend : 0001-01-01 00:00:00.0000000
             * workDateAfternoonEnd : 2019-06-13 21:00:00.0000000
             * vacationDateAfternoonEnd : 2019-07-11 21:00:00.0000000
             * descs : 学校/场馆简介
             * vacationDateMorningEnd : 2019-07-11 12:00:00.0000000
             * name : 大港实验中学
             * vacationDateAfternoonStart : 2019-07-11 12:00:00.0000000
             * isdelete : 0
             * stadiumfacepath : http://static.bhxdty.com/group1/avator/20190606/13/37/4/10.png
             * stadiumfaceid : a60d95f7-8830-4b05-a0f7-a19ed5163234
             * workdatemorningstart : 0001-01-01 00:00:00.0000000
             * status : 1
             * score : 10
             * managername : 王
             * periphery : 无
             * stadiumdesc : 就这样吧
             */

            private String distance;
            private String vacationDateMorningStart;
            private String typecode;
            private String uid;
            private int popularity;
            private String id;
            private String workDateAfternoonStart;
            private String lat;
            private String connectcode;
            private String createtime;
            private String address;
            private String lng;
            private String imgid;
            private String workdatemorningend;
            private String workDateAfternoonEnd;
            private String vacationDateAfternoonEnd;
            private String descs;
            private String vacationDateMorningEnd;
            private String name;
            private String vacationDateAfternoonStart;
            private String isdelete;
            private String stadiumfacepath;
            private String stadiumfaceid;
            private String workdatemorningstart;
            private String status;
            private int score;
            private String managername;
            private String periphery;
            private String stadiumdesc;

            public String getDistance() {
                return distance;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public String getVacationDateMorningStart() {
                return vacationDateMorningStart;
            }

            public void setVacationDateMorningStart(String vacationDateMorningStart) {
                this.vacationDateMorningStart = vacationDateMorningStart;
            }

            public String getTypecode() {
                return typecode;
            }

            public void setTypecode(String typecode) {
                this.typecode = typecode;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public int getPopularity() {
                return popularity;
            }

            public void setPopularity(int popularity) {
                this.popularity = popularity;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getWorkDateAfternoonStart() {
                return workDateAfternoonStart;
            }

            public void setWorkDateAfternoonStart(String workDateAfternoonStart) {
                this.workDateAfternoonStart = workDateAfternoonStart;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getConnectcode() {
                return connectcode;
            }

            public void setConnectcode(String connectcode) {
                this.connectcode = connectcode;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }

            public String getImgid() {
                return imgid;
            }

            public void setImgid(String imgid) {
                this.imgid = imgid;
            }

            public String getWorkdatemorningend() {
                return workdatemorningend;
            }

            public void setWorkdatemorningend(String workdatemorningend) {
                this.workdatemorningend = workdatemorningend;
            }

            public String getWorkDateAfternoonEnd() {
                return workDateAfternoonEnd;
            }

            public void setWorkDateAfternoonEnd(String workDateAfternoonEnd) {
                this.workDateAfternoonEnd = workDateAfternoonEnd;
            }

            public String getVacationDateAfternoonEnd() {
                return vacationDateAfternoonEnd;
            }

            public void setVacationDateAfternoonEnd(String vacationDateAfternoonEnd) {
                this.vacationDateAfternoonEnd = vacationDateAfternoonEnd;
            }

            public String getDescs() {
                return descs;
            }

            public void setDescs(String descs) {
                this.descs = descs;
            }

            public String getVacationDateMorningEnd() {
                return vacationDateMorningEnd;
            }

            public void setVacationDateMorningEnd(String vacationDateMorningEnd) {
                this.vacationDateMorningEnd = vacationDateMorningEnd;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getVacationDateAfternoonStart() {
                return vacationDateAfternoonStart;
            }

            public void setVacationDateAfternoonStart(String vacationDateAfternoonStart) {
                this.vacationDateAfternoonStart = vacationDateAfternoonStart;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getStadiumfacepath() {
                return stadiumfacepath;
            }

            public void setStadiumfacepath(String stadiumfacepath) {
                this.stadiumfacepath = stadiumfacepath;
            }

            public String getStadiumfaceid() {
                return stadiumfaceid;
            }

            public void setStadiumfaceid(String stadiumfaceid) {
                this.stadiumfaceid = stadiumfaceid;
            }

            public String getWorkdatemorningstart() {
                return workdatemorningstart;
            }

            public void setWorkdatemorningstart(String workdatemorningstart) {
                this.workdatemorningstart = workdatemorningstart;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public int getScore() {
                return score;
            }

            public void setScore(int score) {
                this.score = score;
            }

            public String getManagername() {
                return managername;
            }

            public void setManagername(String managername) {
                this.managername = managername;
            }

            public String getPeriphery() {
                return periphery;
            }

            public void setPeriphery(String periphery) {
                this.periphery = periphery;
            }

            public String getStadiumdesc() {
                return stadiumdesc;
            }

            public void setStadiumdesc(String stadiumdesc) {
                this.stadiumdesc = stadiumdesc;
            }
        }

        public static class StadiumlistBean {
            /**
             * distance : .0km
             * vacationDateMorningStart : 2019-07-26 09:00:00.0000000
             * typecode : 2
             * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9F
             * score : 0
             * popularity : 0
             * id : 06ABEF7A-60BF-474F-E344-08D7116E15E0
             * workDateAfternoonStart : 0001-01-01 00:00:00.0000000
             * lat : 39.0218900
             * connectcode : 13000000000
             * createtime : 2019-07-26 11:25:35.9159453
             * address : 天津市滨海新区营口道1041号附近
             * lng : 117.6506970
             * imgid : 2eccc38e-0c2d-4443-93d1-b5b6eaceb027,fdd4313e-c0e1-406d-b408-98572b120e08,f7e03a9c-4036-41a8-a7cf-98f1cfaa6a7b
             * workdatemorningend : 0001-01-01 00:00:00.0000000
             * workDateAfternoonEnd : 2019-07-26 22:00:00.0000000
             * vacationDateAfternoonEnd : 2019-07-26 22:00:00.0000000
             * descs : 学校/场馆简介
             * vacationDateMorningEnd : 2019-07-26 12:00:00.0000000
             * name : 天津市塘沽区排球馆
             * vacationDateAfternoonStart : 2019-07-26 12:00:00.0000000
             * isdelete : 0
             * stadiumfacepath : http://static.bhxdty.com/group1/avator/20190726/10/46/4/5fe2cef778a9761d03d520ec5762cd5b.jpg
             * stadiumfaceid : e4de50e0-ac2f-428e-9839-f42a28db1c78
             * workdatemorningstart : 2019-07-26 09:00:00.0000000
             * status : 1
             */

            private String distance;
            private String vacationDateMorningStart;
            private String typecode;
            private String uid;
            private int score;
            private int popularity;
            private String id;
            private String workDateAfternoonStart;
            private String lat;
            private String connectcode;
            private String createtime;
            private String address;
            private String lng;
            private String imgid;
            private String workdatemorningend;
            private String workDateAfternoonEnd;
            private String vacationDateAfternoonEnd;
            private String descs;
            private String vacationDateMorningEnd;
            private String name;
            private String vacationDateAfternoonStart;
            private String isdelete;
            private String stadiumfacepath;
            private String stadiumfaceid;
            private String workdatemorningstart;
            private String status;

            public String getDistance() {
                return distance;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public String getVacationDateMorningStart() {
                return vacationDateMorningStart;
            }

            public void setVacationDateMorningStart(String vacationDateMorningStart) {
                this.vacationDateMorningStart = vacationDateMorningStart;
            }

            public String getTypecode() {
                return typecode;
            }

            public void setTypecode(String typecode) {
                this.typecode = typecode;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public int getScore() {
                return score;
            }

            public void setScore(int score) {
                this.score = score;
            }

            public int getPopularity() {
                return popularity;
            }

            public void setPopularity(int popularity) {
                this.popularity = popularity;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getWorkDateAfternoonStart() {
                return workDateAfternoonStart;
            }

            public void setWorkDateAfternoonStart(String workDateAfternoonStart) {
                this.workDateAfternoonStart = workDateAfternoonStart;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getConnectcode() {
                return connectcode;
            }

            public void setConnectcode(String connectcode) {
                this.connectcode = connectcode;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }

            public String getImgid() {
                return imgid;
            }

            public void setImgid(String imgid) {
                this.imgid = imgid;
            }

            public String getWorkdatemorningend() {
                return workdatemorningend;
            }

            public void setWorkdatemorningend(String workdatemorningend) {
                this.workdatemorningend = workdatemorningend;
            }

            public String getWorkDateAfternoonEnd() {
                return workDateAfternoonEnd;
            }

            public void setWorkDateAfternoonEnd(String workDateAfternoonEnd) {
                this.workDateAfternoonEnd = workDateAfternoonEnd;
            }

            public String getVacationDateAfternoonEnd() {
                return vacationDateAfternoonEnd;
            }

            public void setVacationDateAfternoonEnd(String vacationDateAfternoonEnd) {
                this.vacationDateAfternoonEnd = vacationDateAfternoonEnd;
            }

            public String getDescs() {
                return descs;
            }

            public void setDescs(String descs) {
                this.descs = descs;
            }

            public String getVacationDateMorningEnd() {
                return vacationDateMorningEnd;
            }

            public void setVacationDateMorningEnd(String vacationDateMorningEnd) {
                this.vacationDateMorningEnd = vacationDateMorningEnd;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getVacationDateAfternoonStart() {
                return vacationDateAfternoonStart;
            }

            public void setVacationDateAfternoonStart(String vacationDateAfternoonStart) {
                this.vacationDateAfternoonStart = vacationDateAfternoonStart;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getStadiumfacepath() {
                return stadiumfacepath;
            }

            public void setStadiumfacepath(String stadiumfacepath) {
                this.stadiumfacepath = stadiumfacepath;
            }

            public String getStadiumfaceid() {
                return stadiumfaceid;
            }

            public void setStadiumfaceid(String stadiumfaceid) {
                this.stadiumfaceid = stadiumfaceid;
            }

            public String getWorkdatemorningstart() {
                return workdatemorningstart;
            }

            public void setWorkdatemorningstart(String workdatemorningstart) {
                this.workdatemorningstart = workdatemorningstart;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

        public static class BannerlistBean {
            /**
             * imgid : 87655864-ec51-4a16-a8e2-0730de4113b6
             * ctype : 1
             * imgpath : http://103.233.6.43:8009/XDfileserver/201910161125518552131243qeq.png
             * contentid :
             * id : 1
             * playtime : 3
             * isdelete : 0
             * sort : 3
             * status : 1
             */

            private String imgid;
            private String ctype;
            private String imgpath;
            private String contentid;
            private String id;
            private String playtime;
            private String isdelete;
            private String sort;
            private String status;

            public String getImgid() {
                return imgid;
            }

            public void setImgid(String imgid) {
                this.imgid = imgid;
            }

            public String getCtype() {
                return ctype;
            }

            public void setCtype(String ctype) {
                this.ctype = ctype;
            }

            public String getImgpath() {
                return imgpath;
            }

            public void setImgpath(String imgpath) {
                this.imgpath = imgpath;
            }

            public String getContentid() {
                return contentid;
            }

            public void setContentid(String contentid) {
                this.contentid = contentid;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPlaytime() {
                return playtime;
            }

            public void setPlaytime(String playtime) {
                this.playtime = playtime;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getSort() {
                return sort;
            }

            public void setSort(String sort) {
                this.sort = sort;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }
}
