package com.soomax.pojo;

import java.util.List;

public class PersonxmPojo {


    /**
     * msg : 操作成功
     * res : {"region":[{"isopen":"0","lng":"117.221108","name":"和平区","pid":"1","id":1,"isdelete":"0","lat":"39.122998","status":"1"},{"isopen":"0","lng":"117.25827","name":"河东区","pid":"1","id":2,"isdelete":"0","lat":"39.135607","status":"1"},{"isopen":"0","lng":"117.229704","name":"河西区","pid":"1","id":3,"isdelete":"0","lat":"39.116053","status":"1"},{"isopen":"0","lng":"117.15767","name":"南开区","pid":"1","id":4,"isdelete":"0","lat":"39.142316","status":"1"},{"isopen":"0","lng":"117.203591","name":"河北区","pid":"1","id":5,"isdelete":"0","lat":"39.153708","status":"1"},{"isopen":"0","lng":"117.157733","name":"红桥区","pid":"1","id":6,"isdelete":"0","lat":"39.172895","status":"1"},{"isopen":"0","lng":"117.320245","name":"东丽区","pid":"1","id":7,"isdelete":"0","lat":"39.092836","status":"1"},{"isopen":"0","lng":"117.015199","name":"西青区","pid":"1","id":8,"isdelete":"0","lat":"39.148195","status":"1"},{"isopen":"0","lng":"117.363031","name":"津南区","pid":"1","id":9,"isdelete":"0","lat":"38.943505","status":"1"},{"isopen":"0","lng":"117.124944","name":"北辰区","pid":"1","id":10,"isdelete":"0","lat":"39.241101","status":"1"},{"isopen":"0","lng":"117.051067","name":"武清区","pid":"1","id":11,"isdelete":"0","lat":"39.389983","status":"1"},{"isopen":"0","lng":"117.316418","name":"宝坻区","pid":"1","id":12,"isdelete":"0","lat":"39.723028","status":"1"},{"isopen":"1","lng":"117.719731","name":"滨海新区","pid":"1","id":13,"isdelete":"0","lat":"39.011042","status":"1"},{"isopen":"0","lng":"117.833151","name":"宁河区","pid":"1","id":14,"isdelete":"0","lat":"39.336091","status":"1"},{"isopen":"0","lng":"116.982051","name":"静海区","pid":"1","id":15,"isdelete":"0","lat":"38.953876","status":"1"},{"isopen":"0","lng":"117.414971","name":"蓟州区","pid":"1","id":16,"isdelete":"0","lat":"40.051591","status":"1"}],"hobby":[{"name":"篮球","id":"1","isdelete":"0","status":"1"},{"name":"足球","id":"2","isdelete":"0","status":"1"},{"name":"游泳","id":"3","isdelete":"0","status":"1"},{"name":"瑜伽","id":"4","isdelete":"0","status":"1"},{"name":"网球","id":"5","isdelete":"0","status":"1"},{"name":"排球","id":"6","isdelete":"0","status":"1"},{"name":"乒乓球","id":"7","isdelete":"0","status":"1"},{"name":"羽毛球","id":"8","isdelete":"0","status":"1"},{"name":"其他","id":"9","isdelete":"0","status":"1"}]}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        private List<RegionBean> region;
        private List<HobbyBean> hobby;

        public List<RegionBean> getRegion() {
            return region;
        }

        public void setRegion(List<RegionBean> region) {
            this.region = region;
        }

        public List<HobbyBean> getHobby() {
            return hobby;
        }

        public void setHobby(List<HobbyBean> hobby) {
            this.hobby = hobby;
        }

        public static class RegionBean {
            /**
             * isopen : 0
             * lng : 117.221108
             * name : 和平区
             * pid : 1
             * id : 1
             * isdelete : 0
             * lat : 39.122998
             * status : 1
             */

            private String isopen;
            private String lng;
            private String name;
            private String pid;
            private int id;
            private String isdelete;
            private String lat;
            private String status;

            public String getIsopen() {
                return isopen;
            }

            public void setIsopen(String isopen) {
                this.isopen = isopen;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPid() {
                return pid;
            }

            public void setPid(String pid) {
                this.pid = pid;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

        public static class HobbyBean {
            /**
             * name : 篮球
             * id : 1
             * isdelete : 0
             * status : 1
             */

            private String name;
            private String id;
            private String isdelete;
            private String status;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }
}
