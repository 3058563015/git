package com.soomax.pojo;

public class NoticeNumPojo {


    /**
     * msg : 操作成功
     * res : {"noticenum":14}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * noticenum : 14
         */

        private int noticenum;

        public int getNoticenum() {
            return noticenum;
        }

        public void setNoticenum(int noticenum) {
            this.noticenum = noticenum;
        }
    }
}
