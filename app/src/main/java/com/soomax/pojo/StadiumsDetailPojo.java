package com.soomax.pojo;

import java.util.List;

public class StadiumsDetailPojo {


    /**
     * msg : 操作成功
     * res : {"stadiuminfo":{"vacationDateMorningStart":"","sportname":"游泳","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9g","score":5,"commentlist":[{"headimgid":"4e92b9a9-cfff-4d2d-9de8-234b7c5bd4c3","createtime":"2019-10-30 13:42:40 353","viewtime":"2019-10-30","contentid":"02477ded-e967-427a-9284-93392b08d616","content":"vvvb","islikecount":1,"uid":"e0990977-8ed2-41de-b73f-bc86ecc108ac","score":"5","commentcount":0,"ctype":"1","islike":0,"nickname":"念旧","childcommentlist":[],"id":"083cf966-67b6-4e23-9bd3-b5a4a211bd24","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191030114400602255296555.png"}],"managername":"管理员","id":"02477ded-e967-427a-9284-93392b08d616","workDateAfternoonStart":"","periphery":"暂无","lat":"39.045484","connectcode":"022-65171888","createtime":"2019-10-27 18:04:14 302","address":"天津市滨海新区泰丰路110号","lng":"117.718232","imgid":"9e32ed8a-43bf-43bc-8f39-bbb3074007c7,ffc6bc38-4adb-40ef-ad8a-db2befa449c1,49d3b350-8c5a-4dc0-9571-bad4ff58fe6d","workdatemorningend":"","stadiumdesc":"提示： 必须戴泳帽，有免费停车位 人均：56元\n俱乐部面向全民场地开放；指导、培训青少年和全民游泳健身；组织游泳体育竞赛；承办游泳体育赛事","imgpath":"http://103.233.6.43:8009/XDfileserver/201910271801387873.png,http://103.233.6.43:8009/XDfileserver/201910271801341871.png,http://103.233.6.43:8009/XDfileserver/201910271801364121.png","workDateAfternoonEnd":"","vacationDateAfternoonEnd":"","groundlist":[{"sportname":"游泳","groundname":"泰丰27℃游泳俱乐部","id":"e0acdebf-368a-4984-b54c-7cfbe26566fa"}],"vacationDateMorningEnd":"","name":"泰丰27℃游泳俱乐部","vacationDateAfternoonStart":"","isdelete":"0","stadiumfaceid":"87c50d7b-f1d0-4520-aac8-a6c6454ae750","workdatemorningstart":"","commentlistsize":1,"status":"1"}}
     * code : 200
     */

    private String msg;
    private ResBean res;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class ResBean {
        /**
         * stadiuminfo : {"vacationDateMorningStart":"","sportname":"游泳","typecode":"2","uid":"C122FE17-31F3-897A-D09C-39ED0CB47F9g","score":5,"commentlist":[{"headimgid":"4e92b9a9-cfff-4d2d-9de8-234b7c5bd4c3","createtime":"2019-10-30 13:42:40 353","viewtime":"2019-10-30","contentid":"02477ded-e967-427a-9284-93392b08d616","content":"vvvb","islikecount":1,"uid":"e0990977-8ed2-41de-b73f-bc86ecc108ac","score":"5","commentcount":0,"ctype":"1","islike":0,"nickname":"念旧","childcommentlist":[],"id":"083cf966-67b6-4e23-9bd3-b5a4a211bd24","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191030114400602255296555.png"}],"managername":"管理员","id":"02477ded-e967-427a-9284-93392b08d616","workDateAfternoonStart":"","periphery":"暂无","lat":"39.045484","connectcode":"022-65171888","createtime":"2019-10-27 18:04:14 302","address":"天津市滨海新区泰丰路110号","lng":"117.718232","imgid":"9e32ed8a-43bf-43bc-8f39-bbb3074007c7,ffc6bc38-4adb-40ef-ad8a-db2befa449c1,49d3b350-8c5a-4dc0-9571-bad4ff58fe6d","workdatemorningend":"","stadiumdesc":"提示： 必须戴泳帽，有免费停车位 人均：56元\n俱乐部面向全民场地开放；指导、培训青少年和全民游泳健身；组织游泳体育竞赛；承办游泳体育赛事","imgpath":"http://103.233.6.43:8009/XDfileserver/201910271801387873.png,http://103.233.6.43:8009/XDfileserver/201910271801341871.png,http://103.233.6.43:8009/XDfileserver/201910271801364121.png","workDateAfternoonEnd":"","vacationDateAfternoonEnd":"","groundlist":[{"sportname":"游泳","groundname":"泰丰27℃游泳俱乐部","id":"e0acdebf-368a-4984-b54c-7cfbe26566fa"}],"vacationDateMorningEnd":"","name":"泰丰27℃游泳俱乐部","vacationDateAfternoonStart":"","isdelete":"0","stadiumfaceid":"87c50d7b-f1d0-4520-aac8-a6c6454ae750","workdatemorningstart":"","commentlistsize":1,"status":"1"}
         */

        private StadiuminfoBean stadiuminfo;

        public StadiuminfoBean getStadiuminfo() {
            return stadiuminfo;
        }

        public void setStadiuminfo(StadiuminfoBean stadiuminfo) {
            this.stadiuminfo = stadiuminfo;
        }

        public static class StadiuminfoBean {
            /**
             * vacationDateMorningStart :
             * sportname : 游泳
             * typecode : 2
             * uid : C122FE17-31F3-897A-D09C-39ED0CB47F9g
             * score : 5
             * commentlist : [{"headimgid":"4e92b9a9-cfff-4d2d-9de8-234b7c5bd4c3","createtime":"2019-10-30 13:42:40 353","viewtime":"2019-10-30","contentid":"02477ded-e967-427a-9284-93392b08d616","content":"vvvb","islikecount":1,"uid":"e0990977-8ed2-41de-b73f-bc86ecc108ac","score":"5","commentcount":0,"ctype":"1","islike":0,"nickname":"念旧","childcommentlist":[],"id":"083cf966-67b6-4e23-9bd3-b5a4a211bd24","isdelete":"0","headimgpath":"http://103.233.6.43:8009/XDfileserver/20191030114400602255296555.png"}]
             * managername : 管理员
             * id : 02477ded-e967-427a-9284-93392b08d616
             * workDateAfternoonStart :
             * periphery : 暂无
             * lat : 39.045484
             * connectcode : 022-65171888
             * createtime : 2019-10-27 18:04:14 302
             * address : 天津市滨海新区泰丰路110号
             * lng : 117.718232
             * imgid : 9e32ed8a-43bf-43bc-8f39-bbb3074007c7,ffc6bc38-4adb-40ef-ad8a-db2befa449c1,49d3b350-8c5a-4dc0-9571-bad4ff58fe6d
             * workdatemorningend :
             * stadiumdesc : 提示： 必须戴泳帽，有免费停车位 人均：56元
             俱乐部面向全民场地开放；指导、培训青少年和全民游泳健身；组织游泳体育竞赛；承办游泳体育赛事
             * imgpath : http://103.233.6.43:8009/XDfileserver/201910271801387873.png,http://103.233.6.43:8009/XDfileserver/201910271801341871.png,http://103.233.6.43:8009/XDfileserver/201910271801364121.png
             * workDateAfternoonEnd :
             * vacationDateAfternoonEnd :
             * groundlist : [{"sportname":"游泳","groundname":"泰丰27℃游泳俱乐部","id":"e0acdebf-368a-4984-b54c-7cfbe26566fa"}]
             * vacationDateMorningEnd :
             * name : 泰丰27℃游泳俱乐部
             * vacationDateAfternoonStart :
             * isdelete : 0
             * stadiumfaceid : 87c50d7b-f1d0-4520-aac8-a6c6454ae750
             * workdatemorningstart :
             * commentlistsize : 1
             * status : 1
             */

            private String vacationDateMorningStart;
            private String sportname;
            private String typecode;
            private String uid;
            private int score;
            private String managername;
            private String id;
            private String workDateAfternoonStart;
            private String periphery;
            private String lat;
            private String connectcode;
            private String createtime;
            private String address;
            private String lng;
            private String imgid;
            private String workdatemorningend;
            private String stadiumdesc;
            private String imgpath;
            private String workDateAfternoonEnd;
            private String vacationDateAfternoonEnd;
            private String vacationDateMorningEnd;
            private String name;
            private String vacationDateAfternoonStart;
            private String isdelete;
            private String stadiumfaceid;
            private String workdatemorningstart;
            private int commentlistsize;
            private String status;
            private List<CommentlistBean> commentlist;
            private List<GroundlistBean> groundlist;

            public String getVacationDateMorningStart() {
                return vacationDateMorningStart;
            }

            public void setVacationDateMorningStart(String vacationDateMorningStart) {
                this.vacationDateMorningStart = vacationDateMorningStart;
            }

            public String getSportname() {
                return sportname;
            }

            public void setSportname(String sportname) {
                this.sportname = sportname;
            }

            public String getTypecode() {
                return typecode;
            }

            public void setTypecode(String typecode) {
                this.typecode = typecode;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public int getScore() {
                return score;
            }

            public void setScore(int score) {
                this.score = score;
            }

            public String getManagername() {
                return managername;
            }

            public void setManagername(String managername) {
                this.managername = managername;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getWorkDateAfternoonStart() {
                return workDateAfternoonStart;
            }

            public void setWorkDateAfternoonStart(String workDateAfternoonStart) {
                this.workDateAfternoonStart = workDateAfternoonStart;
            }

            public String getPeriphery() {
                return periphery;
            }

            public void setPeriphery(String periphery) {
                this.periphery = periphery;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getConnectcode() {
                return connectcode;
            }

            public void setConnectcode(String connectcode) {
                this.connectcode = connectcode;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }

            public String getImgid() {
                return imgid;
            }

            public void setImgid(String imgid) {
                this.imgid = imgid;
            }

            public String getWorkdatemorningend() {
                return workdatemorningend;
            }

            public void setWorkdatemorningend(String workdatemorningend) {
                this.workdatemorningend = workdatemorningend;
            }

            public String getStadiumdesc() {
                return stadiumdesc;
            }

            public void setStadiumdesc(String stadiumdesc) {
                this.stadiumdesc = stadiumdesc;
            }

            public String getImgpath() {
                return imgpath;
            }

            public void setImgpath(String imgpath) {
                this.imgpath = imgpath;
            }

            public String getWorkDateAfternoonEnd() {
                return workDateAfternoonEnd;
            }

            public void setWorkDateAfternoonEnd(String workDateAfternoonEnd) {
                this.workDateAfternoonEnd = workDateAfternoonEnd;
            }

            public String getVacationDateAfternoonEnd() {
                return vacationDateAfternoonEnd;
            }

            public void setVacationDateAfternoonEnd(String vacationDateAfternoonEnd) {
                this.vacationDateAfternoonEnd = vacationDateAfternoonEnd;
            }

            public String getVacationDateMorningEnd() {
                return vacationDateMorningEnd;
            }

            public void setVacationDateMorningEnd(String vacationDateMorningEnd) {
                this.vacationDateMorningEnd = vacationDateMorningEnd;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getVacationDateAfternoonStart() {
                return vacationDateAfternoonStart;
            }

            public void setVacationDateAfternoonStart(String vacationDateAfternoonStart) {
                this.vacationDateAfternoonStart = vacationDateAfternoonStart;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getStadiumfaceid() {
                return stadiumfaceid;
            }

            public void setStadiumfaceid(String stadiumfaceid) {
                this.stadiumfaceid = stadiumfaceid;
            }

            public String getWorkdatemorningstart() {
                return workdatemorningstart;
            }

            public void setWorkdatemorningstart(String workdatemorningstart) {
                this.workdatemorningstart = workdatemorningstart;
            }

            public int getCommentlistsize() {
                return commentlistsize;
            }

            public void setCommentlistsize(int commentlistsize) {
                this.commentlistsize = commentlistsize;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public List<CommentlistBean> getCommentlist() {
                return commentlist;
            }

            public void setCommentlist(List<CommentlistBean> commentlist) {
                this.commentlist = commentlist;
            }

            public List<GroundlistBean> getGroundlist() {
                return groundlist;
            }

            public void setGroundlist(List<GroundlistBean> groundlist) {
                this.groundlist = groundlist;
            }

            public static class CommentlistBean {
                /**
                 * headimgid : 4e92b9a9-cfff-4d2d-9de8-234b7c5bd4c3
                 * createtime : 2019-10-30 13:42:40 353
                 * viewtime : 2019-10-30
                 * contentid : 02477ded-e967-427a-9284-93392b08d616
                 * content : vvvb
                 * islikecount : 1
                 * uid : e0990977-8ed2-41de-b73f-bc86ecc108ac
                 * score : 5
                 * commentcount : 0
                 * ctype : 1
                 * islike : 0
                 * nickname : 念旧
                 * childcommentlist : []
                 * id : 083cf966-67b6-4e23-9bd3-b5a4a211bd24
                 * isdelete : 0
                 * headimgpath : http://103.233.6.43:8009/XDfileserver/20191030114400602255296555.png
                 */

                private String headimgid;
                private String createtime;
                private String viewtime;
                private String contentid;
                private String content;
                private int islikecount;
                private String uid;
                private String score;
                private int commentcount;
                private String ctype;
                private int islike;
                private String nickname;
                private String id;
                private String isdelete;
                private String headimgpath;
                private List<?> childcommentlist;

                public String getHeadimgid() {
                    return headimgid;
                }

                public void setHeadimgid(String headimgid) {
                    this.headimgid = headimgid;
                }

                public String getCreatetime() {
                    return createtime;
                }

                public void setCreatetime(String createtime) {
                    this.createtime = createtime;
                }

                public String getViewtime() {
                    return viewtime;
                }

                public void setViewtime(String viewtime) {
                    this.viewtime = viewtime;
                }

                public String getContentid() {
                    return contentid;
                }

                public void setContentid(String contentid) {
                    this.contentid = contentid;
                }

                public String getContent() {
                    return content;
                }

                public void setContent(String content) {
                    this.content = content;
                }

                public int getIslikecount() {
                    return islikecount;
                }

                public void setIslikecount(int islikecount) {
                    this.islikecount = islikecount;
                }

                public String getUid() {
                    return uid;
                }

                public void setUid(String uid) {
                    this.uid = uid;
                }

                public String getScore() {
                    return score;
                }

                public void setScore(String score) {
                    this.score = score;
                }

                public int getCommentcount() {
                    return commentcount;
                }

                public void setCommentcount(int commentcount) {
                    this.commentcount = commentcount;
                }

                public String getCtype() {
                    return ctype;
                }

                public void setCtype(String ctype) {
                    this.ctype = ctype;
                }

                public int getIslike() {
                    return islike;
                }

                public void setIslike(int islike) {
                    this.islike = islike;
                }

                public String getNickname() {
                    return nickname;
                }

                public void setNickname(String nickname) {
                    this.nickname = nickname;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getIsdelete() {
                    return isdelete;
                }

                public void setIsdelete(String isdelete) {
                    this.isdelete = isdelete;
                }

                public String getHeadimgpath() {
                    return headimgpath;
                }

                public void setHeadimgpath(String headimgpath) {
                    this.headimgpath = headimgpath;
                }

                public List<?> getChildcommentlist() {
                    return childcommentlist;
                }

                public void setChildcommentlist(List<?> childcommentlist) {
                    this.childcommentlist = childcommentlist;
                }
            }

            public static class GroundlistBean {
                /**
                 * sportname : 游泳
                 * groundname : 泰丰27℃游泳俱乐部
                 * id : e0acdebf-368a-4984-b54c-7cfbe26566fa
                 */

                private String sportname;
                private String groundname;
                private String id;

                public String getSportname() {
                    return sportname;
                }

                public void setSportname(String sportname) {
                    this.sportname = sportname;
                }

                public String getGroundname() {
                    return groundname;
                }

                public void setGroundname(String groundname) {
                    this.groundname = groundname;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }
            }
        }
    }
}
