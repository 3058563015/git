package com.simascaffold.eventbus;

public class EventBusBean<T> {
    private int code;
    private T data;


    public final static int Parking_Number = 0;
    public final static int Community_Yes_list = 1;//用于已开通小区的定位后 回调getlist
    public final static int Search_location_change=2;//当想查询的地址发生改变时

    public EventBusBean(T data) {
        this.data = data;
    }

    public EventBusBean(int code) {
        this.code = code;
    }

    public EventBusBean(int code, T data) {
        this.code = code;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
