package com.simascaffold.eventbus;

import org.greenrobot.eventbus.EventBus;

public class EventBusUtil {


    public static void register(Object subscriber) {
        EventBus.getDefault().register(subscriber);
    }

    public static void unregister(Object subscriber) {
        EventBus.getDefault().unregister(subscriber);
    }


    public static void sendEvent(EventBusBean event) {
        EventBus.getDefault().post(event);
    }

    public static void sendStickyEvent(EventBusBean event) {
        EventBus.getDefault().postSticky(event);
    }

}