package com.simascaffold.okgofz;

import com.google.gson.Gson;



public class MyException extends IllegalStateException {

    private YtResponse errorBean;

    public MyException(String s) {
        super(s);
        errorBean = new Gson().fromJson(s, YtResponse.class);
    }

    public YtResponse getErrorBean() {
        return errorBean;
    }
}
