
package com.simascaffold.okgofz;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.lzy.okgo.callback.AbsCallback;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import okhttp3.Response;
import okhttp3.ResponseBody;


public abstract class YtCallback<T> extends AbsCallback<T> {

    private Type type;
    private Class<T> clazz;

    public YtCallback() {
    }

    public YtCallback(Type type) {
        this.type = type;
    }

    public YtCallback(Class<T> clazz) {
        this.clazz = clazz;
    }

    /**
     * 该方法是子线程处理，不能做ui相关的工作
     * 主要作用是解析网络返回的 response 对象,生产onSuccess回调中需要的数据对象
     * 这里的解析工作不同的业务逻辑基本都不一样,所以需要自己实现,以下给出的时模板代码,实际使用根据需要修改
     */
    @Override
    public T convertResponse(Response response) throws Throwable {
        //详细自定义的原理和文档，看这里： https://github.com/jeasonlzy/okhttp-OkGo/wiki/JsonCallback

//      1.解析response 中传递泛型类型
        Type genType = getClass().getGenericSuperclass();
        Type type = ((ParameterizedType) genType).getActualTypeArguments()[0];
        if (!(type instanceof ParameterizedType)) throw new IllegalStateException("没有填写泛型参数");


        Type rawType = ((ParameterizedType) type).getRawType();//父bean
        Type typeArgument = ((ParameterizedType) type).getActualTypeArguments()[0];//子bean


        //2.判断返回是否为空
        ResponseBody body = response.body();
        if (body == null) return null;


        //3.开始反序列化
        Gson gson = new Gson();
        JsonReader jsonReader = new JsonReader(body.charStream());

        //如果不是嵌套的类型 那么直接通过泛型解析出来
        if (rawType != YtResponse.class) {
            T data = gson.fromJson(jsonReader, type);
            response.close();
            return data;
        } else {
            YtResponse ytResponse = gson.fromJson(jsonReader, type);
            response.close();
            String code = ytResponse.httpCode;
//            通过这样封装 最后只有 code 吗是200的 才会返回数据,其他的后台抛出错误信息
            if (code.equals("200")) {
                // noinspection unchecked
                return (T) ytResponse;
            } else {
                throw new IllegalStateException("待定义");
            }
        }
    }

    @Override
    public void onError(com.lzy.okgo.model.Response<T> response) {
        super.onError(response);
        int code = response.code();
        if (code == 404) {
//            LogUtils.debug("404 当前链接不存在");
        }
        Throwable exception = response.getException();

        if (response.getException() instanceof SocketTimeoutException) {
            Log.d("YtCallback", "请求超时");
        } else if (response.getException() instanceof SocketException) {
            Log.d("YtCallback", "服务器异常");
        } else if (response.getException() instanceof MyException) { //个人自定义 异常 根据后台 约定值判断异常雷系

            if (((MyException) response.getException()).getErrorBean().httpCode.equals("")) {

            }
        }
    }

}
