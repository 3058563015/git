package com.simascaffold.update;

public class YTBBean {


    /**
     * data : {"id":"993772895210692609","createdBy":"848518968341807104","createdTime":"1525768896000",
     * "modifiedBy":"848518968341807104","modifiedTime":"1525768903000","deleted":false,
     * "applicationId":"960329681679273985","version":"1.0.2","versionCode":"3","description":"测试版本",
     * "downloadUrl":"http://www.aiseminar.cn","size":"13","forceUpdate":false}
     * httpCode : 200
     * msg : OK
     * timestamp : 1525770273338
     */

    private DataBean data;
    private String httpCode;
    private String msg;
    private String timestamp;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(String httpCode) {
        this.httpCode = httpCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public static class DataBean {
        /**
         * id : 993772895210692609
         * createdBy : 848518968341807104
         * createdTime : 1525768896000
         * modifiedBy : 848518968341807104
         * modifiedTime : 1525768903000
         * deleted : false
         * applicationId : 960329681679273985
         * version : 1.0.2
         * versionCode : 3
         * description : 测试版本
         * downloadUrl : http://www.aiseminar.cn
         * size : 13
         * forceUpdate : false
         */

        private String id;
        private String createdBy;
        private String createdTime;
        private String modifiedBy;
        private String modifiedTime;
        private boolean deleted;
        private String applicationId;
        private String version;
        private String versionCode;
        private String description;
        private String downloadUrl;
        private String size;
        private boolean forceUpdate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        public String getModifiedBy() {
            return modifiedBy;
        }

        public void setModifiedBy(String modifiedBy) {
            this.modifiedBy = modifiedBy;
        }

        public String getModifiedTime() {
            return modifiedTime;
        }

        public void setModifiedTime(String modifiedTime) {
            this.modifiedTime = modifiedTime;
        }

        public boolean isDeleted() {
            return deleted;
        }

        public void setDeleted(boolean deleted) {
            this.deleted = deleted;
        }

        public String getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(String applicationId) {
            this.applicationId = applicationId;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(String versionCode) {
            this.versionCode = versionCode;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDownloadUrl() {
            return downloadUrl;
        }

        public void setDownloadUrl(String downloadUrl) {
            this.downloadUrl = downloadUrl;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public boolean isForceUpdate() {
            return forceUpdate;
        }

        public void setForceUpdate(boolean forceUpdate) {
            this.forceUpdate = forceUpdate;
        }
    }
}
