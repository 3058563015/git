package com.simascaffold.update;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;
import com.orhanobut.logger.Logger;
import com.simascaffold.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class UpdateManager {

    private static final int DOWN = 1;// 用于区分正在下载
    private static final int DOWN_FINISH = 0;// 用于区分下载完成
    private static final int CHECK_UPDATE = 2;// 用于区分下载完成
    private HashMap<String, String> hashMap;// 存储更新版本的xml信息
    private String fileSavePath;// 下载新apk的储存位置
    private int progress;// 获取新apk的下载数据量,更新下载滚动条
    private boolean cancelUpdate = false;// 是否取消下载
    private ProgressBar progressBar;
    private Dialog downLoadDialog;

    private Context context;
    XBYbean xbYbean;
    GoLinster goLinster;
    String versionURL;

    public UpdateManager(UpdateManager updateManager) {
        if (updateManager == null) return;
        this.versionURL = updateManager.versionURL;
        this.context = updateManager.context;
        this.goLinster = updateManager.goLinster;
    }

    public UpdateManager() {
    }

    public static final class UpdateManagerBuilder {
        UpdateManager updateManager;

        public UpdateManagerBuilder() {
            updateManager = new UpdateManager();
        }

        public UpdateManagerBuilder setUrl(String url) {
            updateManager.versionURL = url;
            return UpdateManagerBuilder.this;
        }

        /*
         * 传入上下文
         **/
        public UpdateManagerBuilder setContext(Context context) {
            updateManager.context = context;
            return UpdateManagerBuilder.this;
        }

        /*
         * 传入回调函数
         * */
        public UpdateManagerBuilder setLinster(GoLinster goLinster) {
            updateManager.goLinster = goLinster;
            return UpdateManagerBuilder.this;
        }

        public UpdateManager build() {
            return new UpdateManager(updateManager);
        }


    }

    private void getVersion() {


    }

    private void go() {
        goLinster.go();
    }


    private Handler handler = new Handler() {

        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch ((Integer) msg.obj) {
                case CHECK_UPDATE:
                    // 显示提示对话框
                    showUpdateVersionDialog();
                    break;
                case DOWN:
                    progressBar.setProgress(progress);
                    break;
                case DOWN_FINISH:
                    Toast.makeText(context, "文件下载完成,正在安装更新", Toast.LENGTH_SHORT).show();
                    installAPK();
                    break;

                default:
                    break;
            }
        }

    };


    /**
     * 检测是否可更新
     *
     * @return
     */
    public void checkUpdate() {

        OkGo.<String>get(versionURL)
                .tag(this)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        xbYbean = JSON.parseObject(response.body(), XBYbean.class);
                        if (xbYbean.getErrcode() == 0) {
                            if (isUpdate()) {
                                Message message = new Message();
                                message.obj = CHECK_UPDATE;
                                handler.sendMessage(message);
                            } else {
                                goLinster.go();

                            }
                        } else {
                            goLinster.go();
                        }
                    }

                    @Override
                    public void onError(Response<String> response) {
                        super.onError(response);
                    }
                });

    }

    /**
     * 更新提示框
     */
    private void showUpdateVersionDialog() {
        // // 构造对话框
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("发现新版本");
        builder.setMessage(xbYbean.getData().getPACKAGE() + "\n" + xbYbean.getData().getDESCRIPTION());
        // 更新
        builder.setPositiveButton("更新", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // // 显示下载对话框
                showDownloadDialog();
            }
        });


        // 稍后更新
        int i_lasted = Integer.parseInt(xbYbean.getData().getPACKAGE().split("_")[1].split("\\.")[1]);
        int i_current = Integer.parseInt(getVersion(context).split("\\.")[1]);

        if (i_current == i_lasted) {
            builder.setNegativeButton("下次更新", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    goLinster.go();
                }
            });
        }

        Dialog noticeDialog = builder.create();
        noticeDialog.show();
        downLoadDialog = builder.create();
        // 添加按钮之外的焦点控制
        downLoadDialog.setCanceledOnTouchOutside(false);
        downLoadDialog.setCancelable(false);
        downLoadDialog.show();
    }

    /**
     * 下载的提示框
     */
    protected void showDownloadDialog() {
        {
            // 构造软件下载对话框
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setTitle("正在更新");
            // 给下载对话框增加进度条
            final LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.softupdate_progress, null);
            progressBar = (ProgressBar) v.findViewById(R.id.update_progress);
            builder.setView(v);
            // 取消更新
            builder.setNegativeButton("取消", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    // 设置取消状态
                    cancelUpdate = true;

                }
            });
            downLoadDialog = builder.create();
            // 添加按钮之外的焦点控制
            downLoadDialog.setCanceledOnTouchOutside(false);
            downLoadDialog.setCancelable(false);
            downLoadDialog.show();
            // 下载文件
            downloadApk();
        }

    }

    /**
     * 下载apk,不能占用主线程.所以另开的线程
     */
    private void downloadApk() {
        new downloadApkThread().start();

    }

    /**
     * 判断是否可更新
     */
    private boolean isUpdate() {
        int versionCode = getVersionCode(context);
        try {
            int serverCode = Integer.valueOf(xbYbean.getData().getVERSION());
//            Logger.e("服务器app最新版本是:" + serverCode);
            if (serverCode > versionCode) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Logger.e("baocuol ");
        }
        return false;
    }

    /**
     * 获取当前版本和服务器版本.如果服务器版本高于本地安装的版本.就更新
     */
    private int getVersionCode(Context context2) {
        int versionCode = 0;
        try {
            // 获取软件版本号，对应AndroidManifest.xml下android:versionCode
            versionCode = context.getPackageManager().getPackageInfo("com.bepo", 0).versionCode;
            // System.out.println("当前版本是：========================= " +
            // versionCode);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;

    }

    private String getVersion(Context context2) {
        String version = "";
        try {
            version = context.getPackageManager().getPackageInfo("com.bepo", 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;

    }


    /**
     * 安装apk文件
     */
    private void installAPK() {
        File apkfile = new File(fileSavePath, xbYbean.getData().getPACKAGE() + ".apk");
        if (!apkfile.exists()) {
            return;
        }
        // 通过Intent安装APK文件
        Intent i = new Intent(Intent.ACTION_VIEW);
        System.out.println("filepath=" + apkfile.toString() + "  " + apkfile.getPath());
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setDataAndType(Uri.parse("file://" + apkfile.toString()), "application/vnd.android.package-archive");
        context.startActivity(i);
        // apkfile.delete();
        android.os.Process.killProcess(android.os.Process.myPid());// 如果不加上这句的话在apk安装完成之后点击单开会崩溃
    }


    /**
     * 下载apk的方法
     */
    public class downloadApkThread extends Thread {
        @Override
        public void run() {
            super.run();
            try {
                // 判断SD卡是否存在，并且是否具有读写权限
                if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                    // 获得存储卡的路径
                    String sdpath = Environment.getExternalStorageDirectory() + "/";
                    fileSavePath = sdpath + "download";
                    URL url = new URL(xbYbean.getData().getPATH());
                    // 创建连接
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(5 * 1000);// 设置超时时间
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Charser", "GBK,utf-8;q=0.7,*;q=0.3");
                    // 获取文件大小
                    int length = conn.getContentLength();
                    // 创建输入流
                    InputStream is = conn.getInputStream();

                    File file = new File(fileSavePath);
                    // 判断文件目录是否存在
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    File apkFile = new File(fileSavePath, xbYbean.getData().getPACKAGE() + ".apk");
                    FileOutputStream fos = new FileOutputStream(apkFile);
                    int count = 0;
                    // 缓存
                    byte buf[] = new byte[1024];
                    // 写入到文件中
                    do {
                        int numread = is.read(buf);
                        count += numread;
                        // 计算进度条位置
                        progress = (int) (((float) count / length) * 100);
                        // 更新进度
                        Message message = new Message();
                        message.obj = DOWN;
                        handler.sendMessage(message);
                        if (numread <= 0) {
                            // 下载完成
                            // 取消下载对话框显示
                            installAPK();
                            downLoadDialog.dismiss();
                            Message message2 = new Message();
                            message2.obj = DOWN_FINISH;
                            handler.sendMessage(message2);
                            break;
                        }
                        // 写入文件
                        fos.write(buf, 0, numread);
                    } while (!cancelUpdate);// 点击取消就停止下载.
                    fos.close();
                    is.close();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
