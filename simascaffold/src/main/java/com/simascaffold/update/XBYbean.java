package com.simascaffold.update;

import java.util.List;

/**
 * Created by kefanbufan on 2017/6/5.
 */

public class XBYbean {


    /**
     * errcode : 0
     * errmsg : 操作成功！
     * data : {"CODE":20,"CREATED_DATE":"2017-06-05 14:09:35","CREATED_BY":1,"MODIFY_DATE":"2017-06-05 14:09:35",
     * "MODIFY_BY":3,"FLAG_DEL":0,"SORT":0,"REMARK":"","APPID":"c2a3ce0b-8ef8-453c-bde3-02ada95a3b63","NAME":"云停宝",
     * "VERSION":"42","PICTURE":34,"DESCRIPTION":"版本初始化2","SIZE":"10.90M","PATH":"http://xby.sharecar
     * .cn/xby/upload/2017/6/5/52b0958e-6d36-4e8e-9c04-dc745c8342db.apk","PACKAGE":"云停宝_2.11.0_42__0605",
     * "PICTURE_LIST":[{"CODE":34,"PATH":"2017/6/5/bc649ddb-ca25-465e-96ea-04128f13379f.png"}]}
     */

    private int errcode;
    private String errmsg;
    private DataBean data;

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * CODE : 20
         * CREATED_DATE : 2017-06-05 14:09:35
         * CREATED_BY : 1
         * MODIFY_DATE : 2017-06-05 14:09:35
         * MODIFY_BY : 3
         * FLAG_DEL : 0
         * SORT : 0
         * REMARK :
         * APPID : c2a3ce0b-8ef8-453c-bde3-02ada95a3b63
         * NAME : 云停宝
         * VERSION : 42
         * PICTURE : 34
         * DESCRIPTION : 版本初始化2
         * SIZE : 10.90M
         * PATH : http://xby.sharecar.cn/xby/upload/2017/6/5/52b0958e-6d36-4e8e-9c04-dc745c8342db.apk
         * PACKAGE : 云停宝_2.11.0_42__0605
         * PICTURE_LIST : [{"CODE":34,"PATH":"2017/6/5/bc649ddb-ca25-465e-96ea-04128f13379f.png"}]
         */

        private int CODE;
        private String CREATED_DATE;
        private int CREATED_BY;
        private String MODIFY_DATE;
        private int MODIFY_BY;
        private int FLAG_DEL;
        private int SORT;
        private String REMARK;
        private String APPID;
        private String NAME;
        private String VERSION;
        private int PICTURE;
        private String DESCRIPTION;
        private String SIZE;
        private String PATH;
        private String PACKAGE;
        private List<PICTURELISTBean> PICTURE_LIST;

        public int getCODE() {
            return CODE;
        }

        public void setCODE(int CODE) {
            this.CODE = CODE;
        }

        public String getCREATED_DATE() {
            return CREATED_DATE;
        }

        public void setCREATED_DATE(String CREATED_DATE) {
            this.CREATED_DATE = CREATED_DATE;
        }

        public int getCREATED_BY() {
            return CREATED_BY;
        }

        public void setCREATED_BY(int CREATED_BY) {
            this.CREATED_BY = CREATED_BY;
        }

        public String getMODIFY_DATE() {
            return MODIFY_DATE;
        }

        public void setMODIFY_DATE(String MODIFY_DATE) {
            this.MODIFY_DATE = MODIFY_DATE;
        }

        public int getMODIFY_BY() {
            return MODIFY_BY;
        }

        public void setMODIFY_BY(int MODIFY_BY) {
            this.MODIFY_BY = MODIFY_BY;
        }

        public int getFLAG_DEL() {
            return FLAG_DEL;
        }

        public void setFLAG_DEL(int FLAG_DEL) {
            this.FLAG_DEL = FLAG_DEL;
        }

        public int getSORT() {
            return SORT;
        }

        public void setSORT(int SORT) {
            this.SORT = SORT;
        }

        public String getREMARK() {
            return REMARK;
        }

        public void setREMARK(String REMARK) {
            this.REMARK = REMARK;
        }

        public String getAPPID() {
            return APPID;
        }

        public void setAPPID(String APPID) {
            this.APPID = APPID;
        }

        public String getNAME() {
            return NAME;
        }

        public void setNAME(String NAME) {
            this.NAME = NAME;
        }

        public String getVERSION() {
            return VERSION;
        }

        public void setVERSION(String VERSION) {
            this.VERSION = VERSION;
        }

        public int getPICTURE() {
            return PICTURE;
        }

        public void setPICTURE(int PICTURE) {
            this.PICTURE = PICTURE;
        }

        public String getDESCRIPTION() {
            return DESCRIPTION;
        }

        public void setDESCRIPTION(String DESCRIPTION) {
            this.DESCRIPTION = DESCRIPTION;
        }

        public String getSIZE() {
            return SIZE;
        }

        public void setSIZE(String SIZE) {
            this.SIZE = SIZE;
        }

        public String getPATH() {
            return PATH;
        }

        public void setPATH(String PATH) {
            this.PATH = PATH;
        }

        public String getPACKAGE() {
            return PACKAGE;
        }

        public void setPACKAGE(String PACKAGE) {
            this.PACKAGE = PACKAGE;
        }

        public List<PICTURELISTBean> getPICTURE_LIST() {
            return PICTURE_LIST;
        }

        public void setPICTURE_LIST(List<PICTURELISTBean> PICTURE_LIST) {
            this.PICTURE_LIST = PICTURE_LIST;
        }

        public static class PICTURELISTBean {
            /**
             * CODE : 34
             * PATH : 2017/6/5/bc649ddb-ca25-465e-96ea-04128f13379f.png
             */

            private int CODE;
            private String PATH;

            public int getCODE() {
                return CODE;
            }

            public void setCODE(int CODE) {
                this.CODE = CODE;
            }

            public String getPATH() {
                return PATH;
            }

            public void setPATH(String PATH) {
                this.PATH = PATH;
            }
        }
    }
}
