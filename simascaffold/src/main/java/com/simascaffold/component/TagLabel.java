package com.simascaffold.component;

import android.content.Context;
import android.util.AttributeSet;

import com.kongzue.stacklabelview.StackLabel;

public class TagLabel extends StackLabel {
    public TagLabel(Context context) {
        super(context);
    }

    public TagLabel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TagLabel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }





}
