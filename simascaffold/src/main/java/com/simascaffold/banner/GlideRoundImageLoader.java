package com.simascaffold.banner;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.youth.banner.loader.ImageLoader;

/**
 * Created by hl on 2018/4/10.
 */

public class GlideRoundImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        /**
         注意：
         1.图片加载器由自己选择，这里不限制，只是提供几种使用方法
         2.返回的图片路径为Object类型，由于不能确定你到底使用的那种图片加载器，
         传输的到的是什么格式，那么这种就使用Object接收和返回，你只需要强转成你传输的类型就行，
         切记不要胡乱强转！
         */
        //        eg：

        //Glide 加载图片简单用法
        //Glide.with(context).load(path).into(imageView);
        ///< @hl的方式
        //imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);  ///< @attention banner自己会处理，不用添加；有可能导致图片自动缩放问题
        Glide.with(context)
                .load(path)
                //.placeholder(R.mipmap.pic_default)                ///< @attention banner自己会处理，不用添加；有可能导致图片自动缩放问题
                //.error(R.mipmap.pic_default)
                .apply(RequestOptions.bitmapTransform(new GlideRoundCropTransform(context,20)))
                .into(imageView);

        //Picasso 加载图片简单用法
        //Picasso.with(context).load(path).into(imageView);

        //用fresco加载图片简单用法，记得要写下面的createImageView方法
        //Uri uri = Uri.parse((String) path);
        //imageView.setImageURI(uri);
    }
}
