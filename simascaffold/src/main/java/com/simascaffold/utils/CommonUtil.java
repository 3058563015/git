package com.simascaffold.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.util.HashMap;

public class CommonUtil {


    public static HashMap<String, String> json2map(String str) {
        HashMap<String, String> args = (HashMap<String, String>) JSON.parseObject(str,
                new TypeReference<HashMap<String, String>>() {
                });
        return args;
    }


}
