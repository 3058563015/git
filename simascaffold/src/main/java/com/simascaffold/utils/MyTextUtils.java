package com.simascaffold.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.orhanobut.logger.Logger;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyTextUtils {
    private static final String TAG = "MyTextUtils";

    private static final int FIRST_MAX_LENGTH = 22;
    private static final int SENCOND_MAX_LENGTH = 18;
    private static final int NAME_PREV_MAX_LENGTH = 18;
    private static final int NAME_NEXT_MAX_LENGTH = 0;

    public static String getUTFstr(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str;

    }

    ;

    public static Boolean isZimu(String s) {
        String reg = "[a-zA-Z]";
        return s.matches(reg);
    }

    public static String random() {

        int n = 2;
        int max = 20;
        int min = 10;
        if (n == 1) {
            max = 9;
            min = 1;
        } else if (n == 2) {
            max = 99;
            min = 11;
        } else if (n == 3) {
            max = 999;
            min = 101;
        } else if (n == 4) {
            max = 9999;
            min = 1001;
        } else {
            return "";
        }
        Random random = new Random();
        int s = random.nextInt(max) % (max - min + 1) + min;
        return String.valueOf(s);

    }

    public static void showToast(final String toast, final Context context) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();
                Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
        }).start();
    }

    // a integer to xx:xx:xx
    public static String secToTime(int time) {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (time <= 0)
            return "00:00";
        else {
            minute = time / 60;
            if (minute < 60) {
                second = time % 60;
                timeStr = unitFormat(minute) + ":" + unitFormat(second);
            } else {
                hour = minute / 60;
                if (hour > 99)
                    return "99:59:59";
                minute = minute % 60;
                second = time - hour * 3600 - minute * 60;
                timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
            }
        }
        return timeStr;
    }

    public static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10)
            retStr = "0" + Integer.toString(i);
        else
            retStr = "" + i;
        return retStr;
    }


    public static String getMD5String(String str) {
        try {
            // 生成一个MD5加密计算摘要
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 计算md5函数
            md.update(str.getBytes());
            // digest()最后确定返回md5 hash值，返回值为8位字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
            // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
            //一个byte是八位二进制，也就是2位十六进制字符（2的8次方等于16的2次方）
            return new BigInteger(1, md.digest()).toString(16);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static String getSpanTime(DateTime d1, DateTime d2) {
        Interval interval = new Interval(d1.toInstant(), d2.toInstant());
        Period p = interval.toPeriod();
        return p.getMinutes() + "";
    }


    public static boolean isChinese(char c) {
        String regEx = "[\u4e00-\u9fa5]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(c + "");
        if (m.find())
            return true;
        return false;
    }

    public static String noSpace(String content) {
        return content.replaceAll("\\s*", "");
    }

    public static boolean isEmpty(String content) {
        if (content == null || "" .equals(content.trim())) {
            return true;
        }
        return false;
    }

    public static String isZeroToString(String content) {
        if (content == null || "" .equals(content) || "0" .equals(content) || "0.0" .equals(content)) {

            return "";
        }
        return content;

    }

    public static boolean isArrayEmpty(String[] content) {
        if (content == null || content.length == 0) {
            return true;
        }
        return false;
    }

    public static String getOnelineFormatName(String str) {
        if (isEmpty(str)) {
            return "";
        }

        return getFormatSecLine(str, 25, 11, 11);
    }

    public static String getTwolineFormatName(String str) {
        char[] chars = str.toCharArray();

        String firstLine = "";
        String secondLine = "";
        int firstLength = 0;
        for (int i = 0; i < chars.length; i++) {
            byte[] bytes = String.valueOf(chars[i]).getBytes();
            // LogHelper.w(TAG, "getFormatName, chars[" + i + "]" + chars[i] +
            // " ,length: " + bytes.length);

            // firstLength = firstLength + bytes.length;
            if (isChinese(chars[i])) {
                firstLength = firstLength + 2;
            } else {
                firstLength = firstLength + bytes.length;
            }

            if (firstLength <= FIRST_MAX_LENGTH) {
                firstLine = firstLine + chars[i];
            } else {
                secondLine = secondLine + chars[i];
            }
        }

        secondLine = getFormatSecLine(secondLine, SENCOND_MAX_LENGTH, NAME_PREV_MAX_LENGTH, NAME_NEXT_MAX_LENGTH);
        String name = firstLine + "\n" + secondLine;

        return name;
    }

    private static String getFormatSecLine(String secondLine, int maxLength, int prevLength, int lastLength) {
        String prevStr = secondLine;
        String lastStr = secondLine;
        char[] chars = secondLine.toCharArray();

        int sencondLength = 0;
        for (int i = 0; i < chars.length; i++) {
            byte[] bytes = String.valueOf(chars[i]).getBytes();

            if (isChinese(chars[i])) {
                sencondLength = sencondLength + 2;
            } else {
                sencondLength = sencondLength + bytes.length;
            }
        }

        if (sencondLength <= maxLength) {
            return secondLine;
        } else {
            return getPrevChars(prevStr, prevLength) + "..." + getLastChars(lastStr, lastLength);
        }
    }

    private static String getPrevChars(String secondLine, int prevCount) {
        char[] chars = secondLine.toCharArray();

        int preLength = 0;
        String prevStr = "";
        for (int i = 0; i < chars.length; i++) {
            byte[] bytes = String.valueOf(chars[i]).getBytes();
            // preLength = preLength + bytes.length;
            if (isChinese(chars[i])) {
                preLength = preLength + 2;
            } else {
                preLength = preLength + bytes.length;
            }

            if (preLength <= prevCount) {
                prevStr = prevStr + chars[i];
            } else {
                break;
            }
        }

        return prevStr;
    }

    private static String getLastChars(String secondLine, int lastCount) {
        char[] chars = secondLine.toCharArray();

        int lastLength = 0;
        String lastStr = "";
        for (int i = chars.length - 1; i >= 0; i--) {
            byte[] bytes = String.valueOf(chars[i]).getBytes();
            // lastLength = lastLength + bytes.length;
            if (isChinese(chars[i])) {
                lastLength = lastLength + 2;
            } else {
                lastLength = lastLength + bytes.length;
            }

            if (lastLength <= lastCount) {
                lastStr = chars[i] + lastStr;
            } else {
                break;
            }
        }

        return lastStr;
    }

    // 服务端以s为单位，android端以ms为单位
    public static String getDateString(long position) {
        Date date = new Date(position * 1000);
        // yyyy/MM/dd
        SimpleDateFormat fmPlayTime = new SimpleDateFormat("dd/MM/yyyy");
        fmPlayTime.setTimeZone(TimeZone.getTimeZone("GMT"));
        String dateStr = fmPlayTime.format(date);
        return dateStr;
    }

    public static String getTimeString(long position) {
        Date date = new Date(position);
        // yyyy/MM/dd
        SimpleDateFormat fmPlayTime = new SimpleDateFormat("dd/MM/yyyy");
        fmPlayTime.setTimeZone(TimeZone.getTimeZone("GMT"));
        String dateStr = fmPlayTime.format(date);
        return dateStr;
    }

    // 服务端以s为单位，android端以ms为单位
    public static String getTimeString(String position) {
        try {
            long time = Long.parseLong(position);
            Date date = new Date(time * 1000);
            // yyyy/MM/dd
            SimpleDateFormat fmPlayTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            fmPlayTime.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
            String dateStr = fmPlayTime.format(date);
            return dateStr;
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    // 服务端以s为单位，android端以ms为单位
    public static String getCurrentTimeString() {
        try {
            Date date = new Date();
            // yyyy/MM/dd
            SimpleDateFormat fmPlayTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            fmPlayTime.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
            String dateStr = fmPlayTime.format(date);
            return dateStr;
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    // 服务端以s为单位，android端以ms为单位
    public static String getDateStringShare(long position) {
        Date date = new Date(position * 1000);
        SimpleDateFormat fmPlayTime = new SimpleDateFormat("yyyyMMdd");
        fmPlayTime.setTimeZone(TimeZone.getTimeZone("GMT"));
        String dateStr = fmPlayTime.format(date);
        return dateStr;
    }

    /**
     * 123000 --> 123 --> 2:03
     *
     * @param position
     */
    public static String getStringTime(long position) {
        SimpleDateFormat fmPlayTime;
        if (position <= 0) {
            return "00:00";
        }

        long lCurrentPosition = position / 1000;
        long lHours = lCurrentPosition / 3600;

        if (lHours > 0)
            fmPlayTime = new SimpleDateFormat("HH:mm:ss");
        else
            fmPlayTime = new SimpleDateFormat("mm:ss");

        fmPlayTime.setTimeZone(TimeZone.getTimeZone("GMT"));
        return fmPlayTime.format(position);
    }

    public static String getStringTime(String position) {
        long pos = Long.parseLong(position);
        return getStringTime(pos);
    }

    /**
     * 根据手机的分辨率dp 的单位转成px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 16进制转换为字符串
     *
     * @param hexStr
     * @return
     */
    public static String parseHexStr2Byte(String hexStr) {
        try {
            if (hexStr.length() < 1)
                return null;
            byte[] result = new byte[hexStr.length() / 2];
            for (int i = 0; i < hexStr.length() / 2; i++) {
                int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
                int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
                result[i] = (byte) (high * 16 + low);
            }
            return new String(result, "utf-8");
        } catch (Exception e) {
            Log.e(TAG, "parseHexStr2Byte, ", e);
            return hexStr;
        }
    }

    public static ArrayList<String> toArrayList(String[] strings) {
        ArrayList<String> list = new ArrayList<String>();
        if (strings == null) {
            return list;
        }
        for (int i = 0; i < strings.length; i++) {
            list.add(strings[i]);
        }
        return list;
    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }


    public static boolean isPhone(String phone) {
        String regExp = "^[1][3,5,7,8][0-9]{9}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(phone);
        return m.find();

    }


    //hashmap
    public static String getKey(HashMap<String, String> map, String value) {
        String key = "";
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                key = entry.getKey();
            }
        }
        return key;
    }

    //map中的 key 转为 stirng[]
    public static String[] map2KeyArray(HashMap map) {
        Collection<String> values = map.values();
        Iterator<String> iterator = values.iterator();
        ArrayList<String> list = new ArrayList<>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return (list.toArray(new String[list.size()]));
    }


    public static float str2float(String str) {

        if (MyTextUtils.isEmpty(str)) {
            return 0f;
        }
        return Float.parseFloat(str);

    }

    public static String timeStamp2Date(String seconds, String format) {
        if (seconds == null || seconds.isEmpty() || seconds.equals("null")) {
            return "";
        }
        if (format == null || format.isEmpty()) format = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(Long.valueOf(seconds + "000")));
    }

    /**
     * 日期格式字符串转换成时间戳
     *
     * @param    字符串日期
     * @param format 如：yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String date2TimeStamp(String date_str, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return String.valueOf(sdf.parse(date_str).getTime() / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 取得当前时间戳（精确到秒）
     *
     * @return
     */
    public static String timeStamp() {
        long time = System.currentTimeMillis();
        String t = String.valueOf(time / 1000);
        return t;
    }

    public static boolean checkObjFieldIsFull(Object obj, ArrayList<String> filter) {


        try {
            for (Field f : obj.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                if (filter.contains(f.getName())) {
                    continue;
                }

                if (f.get(obj) == null) {
                    Logger.e("未赋值的字段是>>>" + f.getName());
                    return false;
                }
            }
        } catch (Exception e) {

        }


        return true;
    }

    public static String sHA1(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_SIGNATURES);
            byte[] cert = info.signatures[0].toByteArray();
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(cert);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < publicKey.length; i++) {
                String appendString = Integer.toHexString(0xFF & publicKey[i])
                        .toUpperCase(Locale.US);
                if (appendString.length() == 1)
                    hexString.append("0");
                hexString.append(appendString);
                hexString.append(":");
            }
            String result = hexString.toString();
            return result.substring(0, result.length() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}


