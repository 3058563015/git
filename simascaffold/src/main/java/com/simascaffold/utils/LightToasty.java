package com.simascaffold.utils;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

public class LightToasty {
    public static void normal(Context context, CharSequence text) {
        if (null == context) {
            return;
        }
        Toasty.normal(context, text).show();
    }

    public static void info(Context context, CharSequence text) {
        if (null == context) {
            return;
        }
        Toasty.info(context, text, Toast.LENGTH_SHORT, true).show();
    }

    public static void sucess(Context context, CharSequence text) {
        if (null == context) {
            return;
        }
        Toasty.success(context, text, Toast.LENGTH_SHORT, true).show();
    }

    public static void warning(Context context, CharSequence text) {
        if (null == context) {
            return;
        }
        Toasty.warning(context, text, Toast.LENGTH_LONG, true).show();
    }


}
