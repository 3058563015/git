package com.simascaffold.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.FragmentManager;

import com.baoyz.actionsheet.ActionSheet;

import java.util.ArrayList;

public class MapNavigationUtil {


    public static void goNav(final Context context, FragmentManager fragmentManager, final String lat,
                             final String lon) {
        String[] packageNames = new String[]{"com.baidu.BaiduMap", "com.autonavi.minimap"};
        ArrayList<String> sheetlist = new ArrayList<String>();

        for (String packageName : packageNames) {
            if (isAppInstalled(context, packageName)) {
                switch (packageName) {
                    case "com.baidu.BaiduMap":
                        sheetlist.add("使用百度地图导航");
                        break;
                    case "com.autonavi.minimap":
                        sheetlist.add("使用高德地图导航");
                        break;
                }
            }
        }

        if (sheetlist.size() == 0) {
            LightToasty.normal(context, "请安装百度地图或高德地图!");
        }

        String[] sheetArray = (String[]) sheetlist.toArray(new String[sheetlist.size()]);
        ActionSheet.createBuilder(context, fragmentManager)
                .setCancelButtonTitle("取消")
                .setOtherButtonTitles(sheetArray)
                .setCancelableOnTouchOutside(true)
                .setListener(new ActionSheet.ActionSheetListener() {

                    @Override
                    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

                    }

                    @Override
                    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
                        switch (index) {
                            case 0:
                                invokingBD(lat, lon, context);
                                break;
                            case 1:
                                invokingGD(context, "sharedo", "", lat, lon);
                                break;

                        }
                    }

                }).show();


    }


    //调用百度导航
    public static void invokingBD(String lat_destination, String lon_destination, Context context) {
        Intent intent = null;
        try {
            intent = new Intent("android.intent.action.VIEW", Uri.parse("baidumap://map/navi?coord_type=gcj02" +
                    "&location=" + lat_destination + "," + lon_destination));
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void invokingGD(Context context, String appName, String poiname, String latitude, String longitude) {


        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);

        //将功能Scheme以URI的方式传入data
        Uri uri = Uri.parse("androidamap://navi?sourceApplication=" + appName + "&poiname=" + poiname + "&lat=" + latitude + "&lon=" + longitude + "&style=2");
        intent.setData(uri);
        context.startActivity(intent);

    }


    public static boolean isAppInstalled(Context context, String packageName) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }

        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }

}
