package com.simascaffold.utils;

import android.os.CountDownTimer;


/**
 * 点击按钮后倒计时
 */
class CountTimer extends CountDownTimer {

    public CountTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    /**
     * 倒计时过程中调用
     *
     * @param millisUntilFinished
     */
    @Override
    public void onTick(long millisUntilFinished) {

    }

    /**
     * 倒计时完成后调用
     */
    @Override
    public void onFinish() {

    }
}